$(document).ready(function(){
    $('small').hide();
    $('input').focusout(function() {
        var caja = $(this);
        var form_group = caja.parent();
        var small = form_group.find('small');
        var label = form_group.find('label');
        /* Cuenta los posibles errores encontrados */
        var errores = 0;

        /* Mensajes por defecto */
        var _mensaje = {
            campo_obligatorio: 'This field is required',
            campo_numerico: 'This field is not numeric',
            campo_letra: 'This field does not contain letters',
            campo_alfanumerico: 'This field is not alphanumeric',
            campo_correo: 'This field is not an email',
            campo_longitud: 'This field must have a length of {0} characters',
            campo_min: 'This field must have at least {0} characters',
            campo_max: 'This field must have a maximum of {0} characters',
            campo_valido: 'This field is not valid',
            campo_ip: 'This field is not a valid IP',
            campo_url: 'This field is not a valid URL',
            campo_curp: 'This field is not a valid CURP',
            campo_rfcProfesional: 'This field is not RFC with valid homoclave',
            campo_rfcAlumno: 'This field is not a valid RFC',
            campo_telefono: 'This field is not a Telephone',
            campo_social_twitter: 'This field is not a valid Twitter URL',
            campo_social_facebook: 'This field is not a valid Facebook URL',
            campo_social_youtube: 'This field is not a valid YouTube URL',
            campo_numero_control: 'Este campo no es un Numero de Control',
            campo_usuario:'Nombre de usuario no valido'
        };

        /* No nos interesa validar controles con el estado readonly/disabled */
            if (!caja.prop('readonly') || !caja.prop('disabled'))
            {
                if ($(this).data('valid') != undefined) {
                    /* El tipo de validacion asignado a este control */
                    $.each($(this).data('valid').split('|'), function (i, v) {

                        form_group.removeClass(' has-danger'); /* Limpiamos el estado de error */

                        /* Validamos si es requerido */
                        if (v == 'requerido') {
                            if (caja.val().length == 0) {

                                /* Contamos que hay un error */
                                errores++;

                                /* Agregamos la clase de bootstrap de errores */
                                /* form_group.addClass('has-error'); */
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_obligatorio);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                small.show();

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.removeClass('form-control-danger').addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es numérico */
                        if (v == 'numero') {
                            if (!caja.val().match(/^([0-9])*[.]?[0-9]*$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_numerico);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es un numero de Control */
                        if (v == 'numeroControl') {
                            if (!caja.val().match(/^([M]|[D])?[0-9]{8}$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_numero_control);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si son letras */
                        if (v == 'letras') {
                            if (!caja.val().match(/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s\'\.\,]*(\s*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s\'\.\,]*)*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s\'\.]+$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_letra);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        if (v == 'alfanumerico') {
                            if (!caja.val().match(/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9\s\.\,\:\;]*(\s*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9\s\'\.\,\:\;]*)*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9\s\.\,\:\;]+$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_alfanumerico);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }
                         if (v == 'usuario') {
                           if (!caja.val().match(/^[a-zA-Z0-9]*$/) && caja.val().length > 0) {
                                errores++;
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();
                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_usuario);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }
                                return false; /* Rompe el bucle */
                           } else {
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                           }
                      }
                       
                       /* Validación de alfanumérico Global acepta cualquier tipo de caracter*/
                       if (v == 'alfanumericoGlobal') {                              
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();                          
                            
                        }

                        /* Validamos si es un email */
                        if (v == 'email') {
                            if (!caja.val().match(/^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,4}$/i) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');                                
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_correo);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es un CURP */
                        if (v == 'curp') {
                            if (!caja.val().match(/^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_curp);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es un RFC con homoclave */
                        if (v == 'rfcProfesional') {
                            if (!caja.val().match(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_rfcProfesional);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }
                        /* Validamos si es un RFC */
                        if (v == 'rfcAlumno') {
                            if (!caja.val().match(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_rfcAlumno);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es un telefono */
                        if (v == 'telefono') {
                            if (!caja.val().match(/^[0-9]{10}$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_telefono);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Longitud de caracteres a tener */
                        if (v.indexOf('longitud') > -1 && caja.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _longitud = v.split(':');
                            if (caja.val().length != _longitud[1]) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_longitud.replace('{0}', _longitud[1]));
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Cantidad minima de caracteres */
                        if (v.indexOf('min') > -1 && caja.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _min = v.split(':');
                            if (caja.val().length < _min[1]) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_min.replace('{0}', _min[1]));
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Cantidad maxima de caracteres */
                        if (v.indexOf('max') > -1 && caja.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _min = v.split(':');
                            if (caja.val().length > _min[1]) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_max.replace('{0}', _min[1]));
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validación mediante una funcion personalizada */
                        if (v.indexOf('funcion') > -1 && caja.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _funcion = v.split(':');

                            // Respuesta de la funcion
                            var _respuesta = false;

                            // Espera parámetros
                            if (_funcion.length >= 3) {
                                _respuesta = window[_funcion[1]].apply(this, _funcion[2].split(','));
                            } else {
                                _respuesta = window[_funcion[1]]();
                            }

                            /* Mostramos el mensaje */
                            if (!_respuesta || _respuesta == undefined) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_valido);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false;
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Válidamos una IP */
                        if (v == 'ip') {
                            if (!caja.val().match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_ip);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Válidamos una URL válida */
                        if (v == 'url') {
                            if (!caja.val().match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_url);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Comparamos con otro control */
                        if (v.indexOf('compara') > -1 && caja.val().length > 0) {
                            var _comparacion = true;
                            var _aComparar = v.split(':');

                            $(_aComparar[1], form).each(function () {
                                if (caja.val() != $(this).val()) {

                                    errores++;
                                    //form_group.addClass('has-error');
                                    form_group.removeClass(' has-success').addClass(' has-danger');
                                    caja.removeClass('form-control-success').addClass('form-control-danger');
                                    small.show();

                                    if (caja.data('validacion-mensaje') == undefined) {
                                        small.text(_mensaje.campo_valido);
                                    } else {
                                        small.text(caja.data('validacion-mensaje'));
                                    }
                                }
                                else{
                                    form_group.removeClass(' has-danger').addClass(' has-success');
                                    caja.addClass('form-control-success');
                                    small.hide();
                                }
                            })
                        }

                    })
                }                
            }
        
    });
});


jQuery.fn.validate = function ()
{

    /* Mensajes por defecto */
    var _mensaje = {
        campo_obligatorio: 'This field is required',
        campo_numerico: 'This field is not numeric',
        campo_letra: 'This field does not contain letters',
        campo_alfanumerico: 'This field is not alphanumeric',
        campo_correo: 'This field is not an email',
        campo_longitud: '',
        campo_min: 'This field must have at least {0} characters',
        campo_max: 'This field must have a maximum of {0} characters',
        campo_valido: 'This field is not valid',
        campo_ip: 'This field is not a valid IP',
        campo_url: 'This field is not a valid URL',
        campo_curp: 'This field is not a valid CURP',
        campo_rfcProfesional: 'This field is not RFC with valid homoclave',
        campo_rfcAlumno: 'This field is not a valid RFC',
        campo_telefono: 'This field is not a Telephone',
        campo_social_twitter: 'This field is not a valid Twitter URL',
        campo_social_facebook: 'This field is not a valid Facebook URL',
        campo_social_youtube: 'This field is not a valid YouTube URL',
        campo_numero_control: 'Este campo no es un Numero de Control'
    };

    var form = $(this);

    try {
        /* Cuenta los posibles errores encontrados */
        var errores = 0;

        /* Los controles encontrados por nuestra Clase de CSS */
        var controles = $('[data-valid]', form);

        /* Comenzamos a validar cada control */
        $.each(controles, function () {

            /* El control actual del arreglo */
            var obj = $(this);

            /* No nos interesa validar controles con el estado readonly/disabled */
            if (!obj.prop('readonly') || !obj.prop('disabled'))
            {
                if ($(this).data('valid') != undefined) {
                    /* El tipo de validacion asignado a este control */
                    $.each($(this).data('valid').split('|'), function (i, v) {

                        /* El contenedor del control */
                        var form_group = obj.closest('.form-group');
                        form_group.removeClass('has-danger'); /* Limpiamos el estado de error */

                        /* Capturamos el label donde queremos mostrar el mensaje */
                        var label = form_group.find('label');
                        var caja = form_group.find('input');
                        var small = form_group.find('small');

                        /* Validamos si es requerido */
                        if (v == 'requerido') {
                            if (obj.val().length == 0) {

                                /* Contamos que hay un error */
                                errores++;

                                /* Agregamos la clase de bootstrap de errores */
                                /* form_group.addClass('has-error'); */
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_obligatorio);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                small.show();

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es numérico */
                        if (v == 'numero') {
                            if (!obj.val().match(/^([0-9])*[.]?[0-9]*$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_numerico);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es un numero de Control */
                        if (v == 'numeroControl') {
                            if (!obj.val().match(/^([M]|[D])?[0-9]{8}$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_numero_control);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si son letras */
                        if (v == 'letras') {
                            if (!obj.val().match(/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s\'\.\,]*(\s*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s\'\.\,]*)*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s\'\.]+$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_letra);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        if (v == 'alfanumerico') {
                            if (!obj.val().match(/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9\s\.\,\:\;]*(\s*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9\s\'\.\,\:\;]*)*[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9\s\.\,\:\;]+$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_alfanumerico);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }
                        /* Validación de alfanumérico Global acepta cualquier tipo de caracter
                        * 
                        */
                       if (v == 'alfanumericoGlobal') { 
                                

                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();                            
                        }

                        /* Validamos si es un email */
                        if (v == 'email') {
                            if (!obj.val().match(/^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,4}$/i) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');                                
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_correo);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validamos si es un CURP */
                        if (v == 'curp') {
                            if (!obj.val().match(/^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_curp);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                         /* Validamos si es un RFC con homoclave */
                        if (v == 'rfcProfesional') {
                            if (!caja.val().match(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_rfcProfesional);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }
                        /* Validamos si es un RFC */
                        if (v == 'rfcAlumno') {
                            if (!caja.val().match(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))$/) && caja.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (caja.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_rfcAlumno);
                                } else {
                                    small.text(caja.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }


                        /* Validamos si es un telefono */
                        if (v == 'telefono') {
                            if (!obj.val().match(/^[0-9]{10}$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_telefono);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Longitud de caracteres a tener */
                        if (v.indexOf('longitud') > -1 && obj.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _longitud = v.split(':');
                            if (obj.val().length != _longitud[1]) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_longitud.replace('{0}', _longitud[1]));
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Cantidad minima de caracteres */
                        if (v.indexOf('min') > -1 && obj.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _min = v.split(':');
                            if (obj.val().length < _min[1]) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_min.replace('{0}', _min[1]));
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Cantidad maxima de caracteres */
                        if (v.indexOf('max') > -1 && obj.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _min = v.split(':');
                            if (obj.val().length > _min[1]) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_max.replace('{0}', _min[1]));
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Validación mediante una funcion personalizada */
                        if (v.indexOf('funcion') > -1 && obj.val().length > 0) {

                            // Necesitamos saber la longitud máxima
                            var _funcion = v.split(':');

                            // Respuesta de la funcion
                            var _respuesta = false;

                            // Espera parámetros
                            if (_funcion.length >= 3) {
                                _respuesta = window[_funcion[1]].apply(this, _funcion[2].split(','));
                            } else {
                                _respuesta = window[_funcion[1]]();
                            }

                            /* Mostramos el mensaje */
                            if (!_respuesta || _respuesta == undefined) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_valido);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false;
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Válidamos una IP */
                        if (v == 'ip') {
                            if (!obj.val().match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_ip);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Válidamos una URL válida */
                        if (v == 'url') {
                            if (!obj.val().match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/) && obj.val().length > 0) {

                                errores++;
                                //form_group.addClass('has-error');
                                form_group.removeClass(' has-success').addClass(' has-danger');
                                caja.removeClass('form-control-success').addClass('form-control-danger');
                                small.show();

                                /* Mostramos el mensaje */
                                if (obj.data('validacion-mensaje') == undefined) {
                                    small.text(_mensaje.campo_url);
                                } else {
                                    small.text(obj.data('validacion-mensaje'));
                                }

                                return false; /* Rompe el bucle */
                            }
                            else{
                                form_group.removeClass(' has-danger').addClass(' has-success');
                                caja.addClass('form-control-success');
                                small.hide();
                            }
                        }

                        /* Comparamos con otro control */
                        if (v.indexOf('compara') > -1 && obj.val().length > 0) {
                            var _comparacion = true;
                            var _aComparar = v.split(':');

                            $(_aComparar[1], form).each(function () {
                                if (obj.val() != $(this).val()) {

                                    errores++;
                                    //form_group.addClass('has-error');
                                    form_group.removeClass(' has-success').addClass(' has-danger');
                                    caja.removeClass('form-control-success').addClass('form-control-danger');
                                    small.show();

                                    if (obj.data('validacion-mensaje') == undefined) {
                                        small.text(_mensaje.campo_valido);
                                    } else {
                                        small.text(obj.data('validacion-mensaje'));
                                    }
                                }
                                else{
                                    form_group.removeClass(' has-danger').addClass(' has-success');
                                    caja.addClass('form-control-success');
                                    small.hide();
                                }
                            })
                        }

                    })
                }                
            }
        })

        /* Verificamos si ha sido validado */
        return (errores == 0);
    } catch (e) {
        console.error(e);
        return false;
    }
}