var app = angular.module('Mirocs', ['ui.router', 'datatables', 'swangular', 'ngAnimate']);

app.config(function($stateProvider, $urlRouterProvider, $provide) {

	//$urlRouterProvider.otherwise('/login');
	$urlRouterProvider.otherwise('/home');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('/login', {
		url: '/login',
		templateUrl: 'views/login.html',
		controller: "Login"
	})
	.state('/home', {
		url: '/home',
		templateUrl: 'views/main.html',
		controller: "Main"
	})
	.state('/home.historia', {
		url: '/history',
		templateUrl: 'views/historiaClinica.html',
		controller: "History"
	})
	.state('/home.contacto', {
		url: '/contacto',
		templateUrl: 'views/contacto.html',
		controller: "Contacto"
	})
	.state('/home.mensaje', {
		url: '/mensaje',
		templateUrl: 'views/mensaje.html',
		controller: "Mensaje"
	})
	.state('/home.historia.tab1', {
		url: '/tab1',
		templateUrl: 'views/history/datosGenerales.html',
		controller: "tab1"
	})
	.state('/home.historia.tab2', {
		url: '/tab2',
		templateUrl: 'views/history/QuejaPrincipal.html',
		controller: "tab2"
	})
	.state('/home.historia.tab3', {
		url: '/tab3',
		templateUrl: 'views/history/AntOcularesPx.html',
		controller: "tab3"
	})
	.state('/home.historia.tab4', {
		url: '/tab4',
		templateUrl: 'views/history/AntOcularesFam.html',
		controller: "tab4"
	})
	.state('/home.historia.tab5', {
		url: '/tab5',
		templateUrl: 'views/history/AntMedicosPx.html',
		controller: "tab5"
	})
	.state('/home.historia.tab6', {
		url: '/tab6',
		templateUrl: 'views/history/AntMedicosFam.html',
		controller: "tab6"
	})
	.state('/home.historia.tab7', {
		url: '/tab7',
		templateUrl: 'views/history/ObsGeneral.html',
		controller: "tab7"
	})
	.state('/home.historia.tab8', {
		url: '/tab8',
		templateUrl: 'views/history/ExamPx.html',
		controller: "tab8"
	})
	.state('/home.historia.tab9', {
		url: '/tab9',
		templateUrl: 'views/history/Laboratorio.html',
		controller: "tab9"
	})
	.state('/home.marca', {
		url: '/marca',
		templateUrl: 'views/inventario/marca.html',
		controller: "Marca"
	})
	.state('/home.Material', {
		url: '/Material',
		templateUrl: 'views/inventario/Material.html',
		controller: "Material"
	})
	.state('/home.Colores', {
		url: '/Colores',
		templateUrl: 'views/inventario/Colores.html',
		controller: "Colores"
	})
	.state('/home.Armazon', {
		url: '/Armazon',
		templateUrl: 'views/inventario/Armazon.html',
		controller: "Armazon"

	})
	.state('/home.TamForm', {
		url: '/TamForm',
		templateUrl: 'views/inventario/TamForm.html',
		controller: "TamForm"

	})
	.state('/home.TamTerm', {
		url: '/TamTerm',
		templateUrl: 'views/inventario/TamTerm.html',
		controller: "TamTerm"

	})
	.state('/home.TamPuente', {
		url: '/TamPuente',
		templateUrl: 'views/inventario/TamPuente.html',
		controller: "TamPuente"

	})
	.state('/home.ColFrente', {
		url: '/ColFrente',
		templateUrl: 'views/inventario/ColFrente.html',
		controller: "ColFrente"

	})
	.state('/home.ColTerm', {
		url: '/ColTerm',
		templateUrl: 'views/inventario/ColTerm.html',
		controller: "ColTerm"

	})
	.state('/home.Producto', {
		url: '/Producto',
		templateUrl: 'views/inventario/Producto.html',
		controller: "Producto"
	});

	$provide.decorator('$state', function($delegate, $stateParams) {
		$delegate.forceReload = function() {
			return $delegate.go($delegate.current, $stateParams, {
				reload: true,
				inherit: false,
				notify: true
			});
		};
		return $delegate;
	});
});

app.service('ServiceMirosc', function ($http, $q) {
   this.loginSave = function(idUser,token, idRoll){
	sessionStorage.setItem("user", idUser);
	sessionStorage.setItem("token", token);
	sessionStorage.setItem("idRoll", idRoll);
};

this.cacheHPX = function(idDato){
	sessionStorage.setItem("idDato", idDato);
};

this.cacheHvista = function(vista, paciente){
	sessionStorage.setItem("vista", vista);
	sessionStorage.setItem("paciente", paciente);
};
	//
	this.logout = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('localhost/archivo_optica/test/back/apiMirosc/api/user/logout',info,configuracion)
		.success(function(respuesta) {
			sessionStorage.clear();
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
				//alert('¡Existe un error con el servidor! Intente mas tarde');
				//serviceNotify.notificiacionPanel('There is an error in the server, contact the Administrator',"danger")
			});
		return promise;
	};

	this.sessionLoad = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('localhost/archivo_optica/test/back/apiMirosc/api/session/load',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
				//alert('¡Existe un error con el servidor! Intente mas tarde');
				//serviceNotify.notificiacionPanel('There is an error in the server, contact the Administrator',"danger")
			});
		return promise;
	};

	this.login = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('localhost/archivo_optica/test/back/apiMirosc/api/user/login',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
				//alert('¡Existe un error con el servidor! Intente mas tarde');
				//serviceNotify.notificiacionPanel('There is an error in the server, contact the Administrator',"danger")
			});
		return promise;
	};
	this.session = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('localhost/archivo_optica/test/back/apiMirosc/api/session/user/'+id)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.destroySession = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('localhost/archivo_optica/test/back/apiMirosc/api/session/destroy',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
				//alert('¡Existe un error con el servidor! Intente mas tarde');
				//serviceNotify.notificiacionPanel('There is an error in the server, contact the Administrator',"danger")
			});
		return promise;
	};
	//Contactos
	this.contactos = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/contactos')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchContacto = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/searchCliente',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};  
	//enviados
	this.addEnviado = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/enviados/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	//mensajes
	this.mensajes = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/mensajes')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchMensaje = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/searchMensaje',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.searchMensajeURL = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/searchMensajeURL',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addMensaje = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/mensajes/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyMensaje = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/mensajes/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteMensaje = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/whats/mensajes/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	//Armazon
	this.Armazon = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/armazon')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchArmazon = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchArmazon',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addArmazon = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Armazon/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyArmazon = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Armazon/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteArmazon = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Armazon/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};

	this.ColFrente = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/colFrente')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchColFrente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchColFrente',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addColFrente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColFrente/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyColFrente = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColFrente/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteColFrente = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColFrente/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};

	this.Material = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/material')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchMaterial = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchMaterial',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addMaterial = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Material/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyMaterial = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Material/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteMaterial = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Material/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};

	this.ColFrente = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/colFrente')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchColFrente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchColFrente',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};  
	this.addColFrente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColFrente/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyColFrente = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColFrente/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteColFrente = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColFrente/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};

	this.Colores = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/colores')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchColores = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchColores',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addColores = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Colores/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyColores = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Colores/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteColores = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Colores/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.ColoTerm = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/colTerm')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchColTerm = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchColTerm',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addColTerm = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColTerm/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyColTerm = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColTerm/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteColTerm = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/ColTerm/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.Producto = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/producto')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchProducto = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchProducto',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addProducto = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Producto/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyProducto = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Producto/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteProducto = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/Producto/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.TamForm = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_forma')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchTamForm = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchtamanio_forma',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addTamForm = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_forma/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyTamForm = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_forma/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteTamForm = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_forma/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.TamPuente = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_puente')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchTamPuente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchtamanio_puente',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};    
	this.addTamPuente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_puente/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyTamPuente = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_puente/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteTamPuente = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_puente/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.TamTerm = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_terminal')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchTamTerm = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchtamanio_terminal',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addTamTerm = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_terminal/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.modifyTamTerm = function (id,info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_terminal/update/'+id,info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.deleteTamTerm = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.put('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/tamanio_terminal/delete/'+id,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	//marcas
	this.marcas = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/marcas')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.searchMarca = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/inventario/searchMarca',info)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	//Historia Clinica
	this.historias = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/pacientes')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.historiaPaciente = function (id) {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/Hpaciente/'+id)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.addGeneral = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/general/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addGeneralProspecto = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/general/newProspecto',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addQueja = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/queja/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addOcuPX = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/antocupx/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addOcuFam = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc//api/historia/antocufam/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addMedPX = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/antmedpx/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addMedFam = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/antmedfam/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addObsGen = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/obsgen/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addExamenPX = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/examenpx/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	this.addRXPaciente = function (info) {
		var defered = $q.defer();
		var promise = defered.promise;
		//Se Contruye el Json de la cabecera
		var configuracion = {
			headers : {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'                    
			}
		}; 
		$http.post('http://localhost/archivo_optica/test/back/apiMirosc/api/historia/rxpaciente/new',info,configuracion)
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta);
		});
		return promise;
	};
	//valores
	this.catvalores = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/cat/valores')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.cateje = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/cat/eje')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.catcilindro = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/cat/cilindro')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
	this.catesferico = function () {
		var defered = $q.defer();
		var promise = defered.promise;
		$http.get('http://localhost/archivo_optica/test/back/apiMirosc/api/cat/esferico')
		.success(function(respuesta) {
			defered.resolve(respuesta);
		})
		.error(function(respuesta) {
			defered.reject(respuesta)
		});

		return promise;
	};
});

app.service("serviceExtra", function(){
	//extras
	this.closeModal = function(){
		if ($('.modal-backdrop').is(':visible')) {
			$('body').removeClass('modal-open'); 
			$('.modal-backdrop').remove(); 
		};
		$("#modalOperacion").modal("hide");
	}
});

app.controller('Login', function ($scope, swangular, ServiceMirosc, serviceExtra) {
	$scope.enviar = function(){
		if($('#login').validate()){
			var info = {                
				email: $scope.userlogin,
				password: $scope.pwlogin
			}
			//console.log(info)
			ServiceMirosc.login(info).then(function(respuesta) {                        
				//console.log(respuesta)
				if (respuesta.status == 'OK') {
					var idUser = respuesta.data[0].idUser;
					var idRoll = respuesta.data[0].idRoll;
					var token = respuesta.data[0].token;

					ServiceMirosc.loginSave(idUser, token, idRoll);

				};
				if (respuesta.status == 'ERROR') {
					if (respuesta.messages == 'there is another session on another device, close the corresponding session'){
						//$('#session').modal('show');
						$scope.cargarSessions(respuesta.idUser);
					}
					//serviceNotify.notificiacionPanel(respuesta.messages,'danger');
					//$('#alertLogin').show('slow');
					//$scope.mnesjaLogin = respuesta.messages;
				};
			});
		}
	}

	$scope.cargarSessions = function(idUser){
		ServiceMirosc.session(idUser).then(function(respuestaSession){
			$scope.misSession = respuestaSession;
			for(var i = 0; i < Object.keys(respuestaSession).length; i++){
				$scope.eliminarSession(respuestaSession[i].idUser, respuestaSession[i].idPlataform)
			}
			if(respuestaSession.length == 0){
				$scope.enviar();
			}
		});
	}

	$scope.eliminarSession = function(idUser, idPlataform){
		var infoSession = {                
			idUser: idUser,
			idPlataform: idPlataform
		}
		////console.log(infoSession);
		ServiceMirosc.destroySession(infoSession).then(function(respuesta) {
			if (respuesta.status == 'OK') {
				serviceNotify.notificiacionPanel(respuesta.messages,'success');  
				$scope.cargarSessions(idUser);
			}
			else{
				serviceNotify.notificiacionPanel(respuesta.messages,'danger');
			}
		});
	}
});

app.controller('Main', function ($scope, ServiceMirosc, serviceExtra) {
	$scope.home = function(){
		location.href="#/home/history";
	}
	//$(".preloader").fadeOut();
	//window.scrollTo(0,0);
	/*ServiceMirosc.sessionLoad(infoToken).then(function(respuesta){
		if (!respuesta.status) {
			sessionStorage.clear();
			location.href="#!/login";
			serviceNotify.notificiacionPanel('Your session has expired, start again','danger');
		}
	}); */   
});

app.controller('History', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.tabla = true;
	$scope.formulario = false;

	loadHistoria();

	function loadHistoria(){
		ServiceMirosc.historias().then(function(respuesta) {
			//console.log(respuesta)
			$scope.MisHistorias = respuesta;
		});
	}

	$scope.selectHistoria = function(idPaciente){ 
		$scope.tablaPaciente = true;
		//console.log(idPaciente)
		ServiceMirosc.historiaPaciente(idPaciente).then(function(respuesta) {
			//console.log(respuesta)
			$scope.miPaciente = respuesta;
			$scope.nomCo = respuesta[0].nomC  
		}); 
	};

	$scope.historia = function(idGeneral){
		$scope.viewAnswer = true;
		//$scope.url = "http://localhost/archivo_optica/test/back/apiMirosc/historia/pdf/"+idGeneral+"#page=1&zoom=90";
		$scope.url = "http://localhost/archivo_optica/test/back/apiMirosc/historia/vfpdf/"+idGeneral+"#page=1&zoom=90";
	}

	$scope.addHistory = function(){
		$scope.tabla = false;
		$scope.formulario = true;
		location.href="#/home/history/tab1";
	}

	$scope.newHistory = function(idPaciente){
		$scope.tabla = false;
		$scope.formulario = true;
		location.href="#/home/history/tab1";
		ServiceMirosc.cacheHvista(2, idPaciente);
	}

	
});

app.controller('tab1', function ($scope, ServiceMirosc, serviceExtra) {
	//Datos Generales
	window.scrollTo(0,0);
	var vista = sessionStorage.vista
	var paciente = 0
	$scope.titulo = "Datos Generales";
	$scope.datos = true;
	$scope.genre = false;
	$scope.guardar = true;
	
	if(vista == 2){
		$scope.datos = false;
		$scope.genre = true;		
	}

	if(sessionStorage.paciente > 0){
		paciente = sessionStorage.paciente
	}

	$scope.save1 = function(){
		var info = {
			nombre: $scope.nombre,
			paterno: $scope.paterno,
			materno: $scope.materno,
			casa: $scope.casa,
			celular: $scope.celular
		}
		console.log(info)
		ServiceMirosc.addGeneralProspecto(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				ServiceMirosc.cacheHPX(respuesta.dataD);
				location.href="#/home";
			}
		});
	}

	$scope.save2 = function(){
		var info = {
			cod_paciente: paciente, 
			nombre: $scope.nombre,
			paterno: $scope.paterno,
			materno: $scope.materno,
			casa: $scope.casa,
			celular: $scope.celular,
			nacimiento: $scope.nacimiento,
			//edad: calcularEdad($scope.nacimiento),
			pasatiempo: $scope.pasatiempo,
			calle: $scope.calle,
			nuExt: $scope.nuExt,
			nuInt: $scope.nuInt,
			colonia:$scope.colonia,
			cp: $scope.cp,
			municipio: $scope.municipio,
			estado: $scope.estado,
			email: $scope.email,
			ocupacion: $scope.ocupacion,
			facRies: $scope.facRies
		}
		console.log(info)
		ServiceMirosc.addGeneral(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				ServiceMirosc.cacheHPX(respuesta.dataD);
				location.href="#/home/history/tab2";
			}
		});
	}

	$scope.personal = function(){
		if($scope.visible){
			$scope.genre = true;
			$scope.guardar = false;
		}else{
			$scope.genre = false;
			$scope.guardar = true;
		}
	}

	function calcularEdad(fecha) {
		var hoy = new Date();
		var cumpleanos = new Date(fecha);
		var edad = hoy.getFullYear() - cumpleanos.getFullYear();
		var m = hoy.getMonth() - cumpleanos.getMonth();

		if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
			edad--;
		}

		return edad;
	}
	
});

app.controller('tab2', function ($scope, ServiceMirosc, serviceExtra) {
	//queja
	window.scrollTo(0,0);
	var idDato = sessionStorage.idDato

	$scope.save = function(){
		var info = {
			cod_dato: idDato,
			causa: $scope.causaVis,
			problema: $scope.problema,
			cuando: $scope.cuando,
			bol_vlejana: $scope.visionLej,
			bol_vcercana: $scope.cercana,
			bol_vdoble: $scope.visionDob,
			bol_ardor: $scope.ardor,
			bol_comezon: $scope.comez,
			bol_secrecion: $scope.secre,
			bol_fatiga: $scope.fatiOc,
			bol_epiforia: $scope.epiforia,
			bol_ojoseco: $scope.ojosec,
			bol_ojorojo: $scope.ojoenro,
			bol_moscas: $scope.moscas,
			bol_flashes: $scope.flash,
			bol_auras: $scope.auras,
			bol_fotofobia: $scope.fotofobia,
			otro: $scope.otro
		}
		console.log(info)
		ServiceMirosc.addQueja(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				location.href="#/home/history/tab3";   
			}
		});
	}
});

app.controller('tab3', function ($scope, ServiceMirosc, serviceExtra) {
	//ocu px
	window.scrollTo(0,0);
	$scope.titulo = "Antecedentes Oculares PX"
	$scope.tx1 = false
	$scope.tx2 = false
	$scope.tx3 = false
	var idDato = sessionStorage.idDato

	$scope.golpe = function(){
		if($scope.bol_golpes){
			$scope.tx1 = true;
		}else{
			$scope.tx1 = false;
		}
	}

	$scope.infeccion = function(){
		if($scope.bol_infecciones){
			$scope.tx2 = true;
		}else{
			$scope.tx2= false;
		}
	}

	$scope.desviacion = function(){
		if($scope.bol_desviacion_o){
			$scope.tx3 = true;
		}else{
			$scope.tx3 = false;
		}
	}

	$scope.save = function(){
		var info = {
			cod_dato: idDato,
			bol_golpes: $scope.bol_golpes,
			txt_ubicacion1: $scope.txt_ubicacion1,
			txt_tx1: $scope.txt_tx1,

			bol_infecciones: $scope.bol_infecciones,
			txt_ubicacion2: $scope.txt_ubicacion2,
			txt_tx2: $scope.txt_tx2,

			bol_desviacion_o: $scope.bol_desviacion_o,
			txt_tx3: $scope.txt_tx3
		}
		console.log(info)
		ServiceMirosc.addOcuPX(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				location.href="#/home/history/tab4";    
			}
		});
	}
});

app.controller('tab4', function ($scope, ServiceMirosc, serviceExtra) {
	//ocu fam
	window.scrollTo(0,0);
	var idDato = sessionStorage.idDato
	$scope.titulo = "Antecedentes Oculares Familiares"
	$scope.bol_cataratas = false
	$scope.bol_glaucoma = false
	$scope.bol_cirugias = false
	$scope.bol_estrabismo = false
	$scope.bol_ametropias = false
	$scope.bol_queratocono = false
	$scope.bol_ceguera = false

	$scope.cataratas = function(){
		if($scope.bol_cataratas){
			$scope.catarata = true;
		}else{
			$scope.catarata = false;
		}
	}

	$scope.glaucomaf = function(){
		if($scope.bol_glaucoma){
			$scope.Glaucoma = true;
		}else{
			$scope.Glaucoma = false;
		}
	}

	$scope.ceguera = function(){
		if($scope.bol_ceguera){
			$scope.Ceguera = true;
		}else{
			$scope.Ceguera = false;
		}
	}

	$scope.cirugias = function(){
		if($scope.bol_cirugias){
			$scope.cirugia = true;
		}else{
			$scope.cirugia = false;
		}
	}

	$scope.estrabismo = function(){
		if($scope.bol_estrabismo){
			$scope.Estrabismo = true;
		}else{
			$scope.Estrabismo = false;
		}
	}

	$scope.ametropias = function(){
		if($scope.bol_ametropias){
			$scope.ametropia = true;
		}else{
			$scope.ametropia = false;
		}
	}

	$scope.queratocono = function(){
		if($scope.bol_queratocono){
			$scope.Queratocono = true;
		}else{
			$scope.Queratocono = false;
		}
	}

	$scope.save = function(){
		var info = {
			cod_dato: idDato,
			bol_cataratas: $scope.bol_cataratas,
			bol_glaucoma: $scope.bol_glaucoma,
			bol_cirugias: $scope.bol_cirugias,
			bol_estrabismo: $scope.bol_estrabismo,
			bol_ametropias: $scope.bol_ametropias,
			bol_queratocono: $scope.bol_queratocono,
			bol_ceguera: $scope.bol_ceguera,
			txt1: $scope.txt_tx1,
			txt2: $scope.txt_tx2,
			txt3: $scope.txt_tx3,
			txt4: $scope.txt_tx4,
			txt5: $scope.txt_tx5,
			txt6: $scope.txt_tx6,
			txt7: $scope.txt_tx7
		}
		console.log(info)
		ServiceMirosc.addOcuFam(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				location.href="#/home/history/tab5";    
			}
		});
	}
});

app.controller('tab5', function ($scope, ServiceMirosc, serviceExtra) {
	//med px
	window.scrollTo(0,0);
	$scope.titulo = "Antecedentes Medicos PX"
	var idDato = sessionStorage.idDato
	$scope.bol_cirugias = false
	$scope.bol_alergias = false

	$scope.servicios = [
	{nombre: "IMSS"},
	{nombre: "ISSSTE"},
	{nombre: "SEG POP"},
	{nombre: "MILITAR"}
	]

	$scope.cirugias = function(){
		if($scope.bol_cirugias){
			$scope.cirugia = true;
		}else{
			$scope.cirugia = false;
		}
	}
	
	$scope.alergias = function(){
		if($scope.bol_alergias){
			$scope.alergia = true;
		}else{
			$scope.alergia = false;
		}
	}

	$scope.save = function(){
		var info = {
			cod_dato: idDato,
			des_fecultexammed: $scope.des_fecultexammed,
			bol_salud: $scope.bol_salud,
			bol_diabetes: $scope.bol_diabetes,
			bol_preionarter: $scope.bol_preionarter,
			bol_glaucoma: $scope.bol_glaucoma,
			bol_cirugias: $scope.bol_cirugias,
			txt_tx1: $scope.txt_tx1,
			bol_traumatismo: $scope.bol_traumatismo,
			bol_dolorcabeza: $scope.bol_dolorcabeza,
			bol_alergias: $scope.bol_alergias,
			txt_tx2: $scope.txt_tx2,
			//bol_sermedicos: $scope.bol_sermedicos,
			txt_cuales: $scope.selectedServicio
		}
		console.log(info)
		ServiceMirosc.addMedPX(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				location.href="#/home/history/tab6";        
			}
		});
	}
	
});

app.controller('tab6', function ($scope, ServiceMirosc, serviceExtra) {
	//med fam
	window.scrollTo(0,0);
	var idDato = sessionStorage.idDato
	$scope.titulo = "Antecedentes medicos familiares"
	$scope.bol_diabetes = false
	$scope.bol_preionarter = false
	$scope.bol_glaucoma = false
	$scope.bol_cirugias = false
	$scope.bol_traumatismo = false
	$scope.bol_dolorcabeza = false
	$scope.bol_alergias = false

	$scope.diabetes = function(){
		if($scope.bol_diabetes){
			$scope.txt1 = true;
		}else{
			$scope.txt1 = false;
		}
	}

	$scope.presion = function(){
		if($scope.bol_preionarter){
			$scope.txt2 = true;
		}else{
			$scope.txt2 = false;
		}
	}

	$scope.glucoma = function(){
		if($scope.bol_glaucoma                	){
			$scope.txt3 = true;
		}else{
			$scope.txt3 = false;
		}
	}

	$scope.cirugia = function(){
		if($scope.bol_cirugias){
			$scope.txt4 = true;
		}else{
			$scope.txt4 = false;
		}
	}

	$scope.trumatismo = function(){
		if($scope.bol_traumatismo){
			$scope.txt5 = true;
		}else{
			$scope.txt5 = false;
		}
	}

	$scope.dolor = function(){
		if($scope.bol_dolorcabeza){
			$scope.txt6 = true;
		}else{
			$scope.txt6 = false;
		}
	}

	$scope.alergia = function(){
		if($scope.bol_alergias){
			$scope.txt7 = true;
		}else{
			$scope.txt7 = false;
		}
	}

	$scope.save = function(){
		var info = {
			cod_dato: idDato,
			bol_diabetes: $scope.bol_diabetes,
			bol_preionarter: $scope.bol_preionarter,
			bol_glaucoma: $scope.bol_glaucoma,
			bol_cirugias: $scope.bol_cirugias,
			bol_traumatismo: $scope.bol_traumatismo,
			bol_dolorcabeza: $scope.bol_dolorcabeza,
			bol_alergias: $scope.bol_alergias,
			txt1: $scope.txt_tx1,
			txt2: $scope.txt_tx2,
			txt3: $scope.txt_tx3,
			txt4: $scope.txt_tx4,
			txt5: $scope.txt_tx5,
			txt6: $scope.txt_tx6,
			txt7: $scope.txt_tx7
			
		}
		console.log(info)
		ServiceMirosc.addMedFam(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				location.href="#/home/history/tab7";        
			}
		});
	}

	
});

app.controller('tab7', function ($scope, ServiceMirosc, serviceExtra) {
	//obs general
	window.scrollTo(0,0);
	var idDato = sessionStorage.idDato
	$scope.save = function(){
		var info = {
			cod_dato: idDato,
			txt_anomalias: $scope.txt_anomalias,
			txt_asimetrias: $scope.txt_asimetrias,
			txt_tx1: $scope.txt_tx1,
			txt_obsocular: $scope.txt_obsocular,
			txt_tx2: $scope.txt_tx2,
			txt_comportamiento: $scope.txt_comportamiento,
			txt_otro: $scope.txt_otro
		}
		ServiceMirosc.addObsGen(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				location.href="#/home/history/tab8";        
			}
		});
	}
});

app.controller('tab8', function ($scope, ServiceMirosc, serviceExtra) {
	//examen px y rxpx
	window.scrollTo(0,0);
	var idDato = sessionStorage.idDato

	
		loadValores();
		loadEje();
		//loadEsferico();
		loadCilindro();
	
	
	function loadValores(){
		ServiceMirosc.catvalores().then(function(respuesta) {
			console.log(respuesta)
			$scope.medidas = respuesta;
		});
		
	}

	function loadEje(){
		ServiceMirosc.cateje().then(function(respuesta) {
			console.log(respuesta)
			$scope.eje = respuesta;
		});
	}


	function loadEsferico(){
		ServiceMirosc.catesferico().then(function(respuesta) {
			console.log(respuesta)
			$scope.esferico = respuesta;
		});
	}


	function loadCilindro(){
		ServiceMirosc.catcilindro().then(function(respuesta) {
			console.log(respuesta)
			$scope.cilindro = respuesta;
		});
	}

	$scope.save = function(){
		//tabla 1
		var infoD1 = {
			cod_dato: idDato,
			des_ladoojo: 'd',
			cnu_avl: $scope.derAVL,
			cnu_ph: $scope.derPH,
			cnu_retinoscopia: $scope.derRetinos,
			cnu_rx: $scope.derOPT,
			cnu_bicromatica: $scope.derBicro,
			cnu_reloj: $scope.derReloj
		}
		var infoI1 = {
			cod_dato: idDato,
			des_ladoojo: 'i',
			cnu_avl: $scope.izqAVL,
			cnu_ph: $scope.izqPH,
			cnu_retinoscopia: $scope.izqRetinos,
			cnu_rx: $scope.izqOPT,
			cnu_bicromatica: $scope.izqBicro,
			cnu_reloj: $scope.izqReloj
		}
		//tabla 2
		var infoD2 = {
			cod_dato: idDato,
			des_ladoojo: 'd',
			cnu_esferico: $scope.derEsfe,
			cnu_cilindro: $scope.derCilin,
			cnu_eje: $scope.derEje,
			cnu_add: $scope.derADD,
			cnu_dnp: $scope.derDNP,
			cnu_dip: $scope.derDIP,
			cnu_altura: $scope.derAlt,
			cnu_prisma: $scope.derPris
		}
		var infoI2 = {
			cod_dato: idDato,
			des_ladoojo: 'i',
			cnu_esferico: $scope.izqEsfe,
			cnu_cilindro: $scope.izqCilin,
			cnu_eje: $scope.izqEje,
			cnu_add: $scope.izqADD,
			cnu_dnp: $scope.izqDNP,
			cnu_dip: $scope.izqDIP,
			cnu_altura: $scope.izqAlt,
			cnu_prisma: $scope.izqPris
		}
		ServiceMirosc.addExamenPX(infoD1).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				ServiceMirosc.addExamenPX(infoI1).then(function(respuesta) {
					console.log(respuesta)
					if(respuesta.status == "OK"){

					}
				});
			}
		});
		ServiceMirosc.addRXPaciente(infoD2).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				ServiceMirosc.addRXPaciente(infoI2).then(function(respuesta) {
					console.log(respuesta)
					if(respuesta.status == "OK"){
						location.href="#/home/history/tab9";    
					}
				});
			}
		});
	}
});

app.controller('tab9', function ($scope, ServiceMirosc, serviceExtra) {
	//laboratorio
	window.scrollTo(0,0);
	var idDato = sessionStorage.idDato
	$scope.save = function(){
		/*ServiceMirosc.addGeneral(info).then(function(respuesta) {
			console.log(respuesta)
			if(respuesta.status == "OK"){
				
			}
		});*/
	} 
});

app.controller('Contacto', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	$scope.paciente
	$scope.mostrarContacto = false;
	$scope.saveBotonContacto = false;
	$scope.updateBotonContacto = false;
	$scope.deleteBotonContacto = false;

	loadContacto();

	function loadContacto(){
	 ServiceMirosc.contactos().then(function(respuesta) {
		$scope.Miscontactos = respuesta;
	});
 }

 function controlViewSelect(){
	$scope.mostrarContacto = true;
	$scope.mostrarDeleteContacto = false;
	$scope.saveBotonContacto = true;
	$scope.updateBotonContacto = false;
	$scope.deleteBotonContacto = false;
	$scope.titulo = "Seleciona el mensaje";
}

$scope.selectMensaje = function(id){
	controlViewSelect();
	ServiceMirosc.mensajes().then(function(respuesta) {
			//console.log(id)
			$scope.paciente = id
			//console.log(respuesta)
			//console.log(respuesta[0])
			var array = respuesta == null ? [] : (respuesta instanceof Array ? respuesta : [respuesta]);
			$scope.MisMensajes = array;
			$scope.idmens = $scope.MisMensajes;
			$scope.idmens = respuesta[0].idmens;
		});
}

$scope.enviar = function(){
		//console.log($scope.idmens)
		var info = {
			id: $scope.paciente
		}
		//console.log(info)
		ServiceMirosc.searchContacto(info).then(function(respuesta){
			var url = respuesta[0].url;
			var info2 = {
				id: $scope.idmens
			}
			ServiceMirosc.searchMensajeURL(info2).then(function(respuesta){
				var urlC = url + respuesta[0].texto;
				//console.log(urlC)
				window.open(urlC, '_blank');
				serviceExtra.closeModal();
			});
			var infos = {
				paciente: $scope.paciente,
				mensaje: $scope.idmens
			}
			ServiceMirosc.addEnviado(infos).then(function(respuesta){
				console.log(respuesta)
			});
			//console.log(respuesta);
			//console.log(respuesta[0].nombreC);
			//console.log(respuesta[0].celular);
			//console.log(respuesta[0].url);
			//window.open('https://www.google.com', '_blank');
			//window.location.href="https://www.google.com"; 
		});
	}
	
});

app.controller('Marca', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarMarca = false;
	$scope.saveBotonMarca = false;
	$scope.updateBotonMarca = false;
	$scope.deleteBotonMarca = false;

	loadMarca();

	function loadMarca(){
		ServiceMirosc.marcas().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.Mismarcas = respuesta;
		});
	}

	function controlViewSaveMarca(){
		$scope.mostrarMarca = true;
		$scope.mostrarDeleteMarca = false;
		$scope.saveBotonMarca = true;
		$scope.updateBotonMarca = false;
		$scope.deleteBotonMarca = false;
		$scope.titulo = "Nueva Marca";
		//$('#modalOperacion').modal('show');
	}
	function controlViewUpdateMarca(){
		$scope.mostrarMarca = true;
		$scope.mostrarDeleteMarca = false;
		$scope.saveBotonMarca = false;
		$scope.updateBotonMarca = true;
		$scope.deleteBotonMarca = false;
		$scope.titulo = "Actualizar Marca";
		//$('#modalOperacion').modal('show');
	}
	function controlViewDeleteMarca(){
		$scope.mostrarMarca = false;
		$scope.mostrarDeleteMarca = true;
		$scope.saveBotonMarca = false;
		$scope.updateBotonMarca = false;
		$scope.deleteBotonMarca = true;
		$scope.titulo = "Eliminar Marca";
		//$('#modalOperacion').modal('show');
	}

	$scope.addMarca = function(){
		controlViewSaveMarca();
		$scope.Marca = "";
	}
	
	$scope.saveMarca = function(){
		var info = {
			marca: $scope.Marca
		}
		console.log(info);
		serviceExtra.closeModal();
		loadMarca();
		ServiceMirosc.addMarca(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMarca();
			}
			else{

			} 
		});
	}

	$scope.modificaMarca = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchMarca(info).then(function(respuesta){
			$scope.MarcaH = respuesta[0].id_marca
			$scope.Marca = respuesta[0].marca
		});
		controlViewUpdateMarca();
	}

	$scope.updateMarca = function(){
		serviceExtra.closeModal();
		loadMarca();
	}

	$scope.deleteAlertMarca = function(id){
		controlViewDeleteMarca();

	}
});


app.controller('Mensaje', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarMensaje = false;
	$scope.saveBotonMensaje = false;
	$scope.updateBotonMensaje = false;
	$scope.deleteBotonMensaje = false;

	loadMensaje();

	function loadMensaje(){
		ServiceMirosc.mensajes().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisMensajes = respuesta;
		});
	}

	function controlViewSaveMensaje(){
		$scope.mostrarMensaje = true;
		$scope.mostrarDeleteMensaje = false;
		$scope.saveBotonMensaje = true;
		$scope.updateBotonMensaje = false;
		$scope.deleteBotonMensaje = false;
		$scope.titulo = "Nuevo Mensaje";
		
	}
	function controlViewUpdateMensaje(){
		$scope.mostrarMensaje = true;
		$scope.mostrarDeleteMensaje = false;
		$scope.saveBotonMensaje = false;
		$scope.updateBotonMensaje = true;
		$scope.deleteBotonMensaje = false;
		$scope.titulo = "Actualizar Mensaje";

	}
	function controlViewDeleteMensaje(){
		$scope.mostrarMensaje = false;
		$scope.mostrarDeleteMensaje = true;
		$scope.saveBotonMensaje = false;
		$scope.updateBotonMensaje = false;
		$scope.deleteBotonMensaje = true;
		$scope.titulo = "Eliminar Mensaje";

	}

	$scope.addMensaje = function(){
		controlViewSaveMensaje();
		$scope.Mensaje = "";
	}
	
	$scope.saveMensaje = function(){
		var info = {
			mensaje: $scope.mensaje,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addMensaje(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMensaje();
			}
			else{

			}
		});
	}

	$scope.modificaMensaje = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchMensaje(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idMensajeH = respuesta[0].id_mensaje
			$scope.mensaje = respuesta[0].mensaje
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateMensaje();
	}

	$scope.updateMensaje = function(){
		var info = {
			id: $scope.idMensajeH,
			descripcion: $scope.descripcion,
			mensaje: $scope.mensaje
		}
		//console.log(info);
		ServiceMirosc.modifyMensaje($scope.idMensajeH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMensaje();
			}
		}); 
	}

	$scope.deleteAlertMensaje = function(id){
		controlViewDeleteMensaje();
		ServiceMirosc.deleteMensaje(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMensaje();
			}
		}); 
	}
});

app.controller('Material', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarMaterial = false;
	$scope.saveBotonMaterial = false;
	$scope.updateBotonMaterial = false;
	$scope.deleteBotonMaterial = false;

	loadMaterial();

	function loadMaterial(){
		ServiceMirosc.Material().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisMaterial = respuesta;
		});
	}

	function controlViewSaveMaterial(){
		$scope.mostrarMaterial = true;
		$scope.mostrarDeleteMaterial = false;
		$scope.saveBotonMaterial = true;
		$scope.updateBotonMaterial = false;
		$scope.deleteBotonMaterial = false;
		$scope.titulo = "Nuevo Material";
		
	}
	function controlViewUpdateMaterial(){
		$scope.mostrarMaterial = true;
		$scope.mostrarDeleteMaterial = false;
		$scope.saveBotonMaterial = false;
		$scope.updateBotonMaterial = true;
		$scope.deleteBotonMaterial = false;
		$scope.titulo = "Actualizar Material";

	}
	function controlViewDeleteMaterial(){
		$scope.mostrarMaterial = false;
		$scope.mostrarDeleteMaterial = true;
		$scope.saveBotonMaterial = false;
		$scope.updateBotonMaterial = false;
		$scope.deleteBotonMaterial = true;
		$scope.titulo = "Eliminar Material";

	}

	$scope.addMaterial = function(){
		controlViewSaveMaterial();
		$scope.Material = "";
	}
	
	$scope.saveMaterial = function(){
		var info = {
			Material: $scope.Material,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addMaterial(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMaterial();
			}
			else{

			}
		});
	}

	$scope.modificaMaterial = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchMaterial(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idMaterialH = respuesta[0].id_Material
			$scope.Material = respuesta[0].Material
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateMaterial();
	}

	$scope.updateMaterial = function(){
		var info = {
			id: $scope.idMaterialH,
			descripcion: $scope.descripcion,
			Material: $scope.Material
		}
		//console.log(info);
		ServiceMirosc.modifyMaterial($scope.idMaterialH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMaterial();
			}
		}); 
	}

	$scope.deleteAlertMaterial = function(id){
		controlViewDeleteMaterial();
		ServiceMirosc.deleteMaterial(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadMaterial();
			}
		}); 
	}
});


app.controller('Colores', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarColores = false;
	$scope.saveBotonColores = false;
	$scope.updateBotonColores = false;
	$scope.deleteBotonColores = false;

	loadColores();

	function loadColores(){
		ServiceMirosc.Colores().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisColores = respuesta;
		});
	}

	function controlViewSaveColores(){
		$scope.mostrarColores = true;
		$scope.mostrarDeleteColores = false;
		$scope.saveBotonColores = true;
		$scope.updateBotonColores = false;
		$scope.deleteBotonColores = false;
		$scope.titulo = "Nuevo Color";
		
	}
	function controlViewUpdateColores(){
		$scope.mostrarColores = true;
		$scope.mostrarDeleteColores = false;
		$scope.saveBotonColores = false;
		$scope.updateBotonColores = true;
		$scope.deleteBotonColores = false;
		$scope.titulo = "Actualizar Color";

	}
	function controlViewDeleteColores(){
		$scope.mostrarColores = false;
		$scope.mostrarDeleteColores = true;
		$scope.saveBotonColores = false;
		$scope.updateBotonColores = false;
		$scope.deleteBotonColores = true;
		$scope.titulo = "Eliminar Color";

	}

	$scope.addColores = function(){
		controlViewSaveColores();
		$scope.Colores = "";
	}
	
	$scope.saveColores = function(){
		var info = {
			Colores: $scope.Colores,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addColores(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColores();
			}
			else{

			}
		});
	}

	$scope.modificaColores = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchColores(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idColoresH = respuesta[0].id_Colores
			$scope.Colores = respuesta[0].Colores
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateColores();
	}

	$scope.updateColores = function(){
		var info = {
			id: $scope.idColoresH,
			descripcion: $scope.descripcion,
			Colores: $scope.Colores
		}
		//console.log(info);
		ServiceMirosc.modifyColores($scope.idColoresH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColores();
			}
		}); 
	}

	$scope.deleteAlertColores = function(id){
		controlViewDeleteColores();
		ServiceMirosc.deleteColores(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColores();
			}
		}); 
	}
});

app.controller('Armazon', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarArmazon = false;
	$scope.saveBotonArmazon = false;
	$scope.updateBotonArmazon = false;
	$scope.deleteBotonArmazon = false;

	loadArmazon();

	function loadArmazon(){
		ServiceMirosc.Armazon().then(function(respuesta) {
			$scope.MisArmazon = respuesta;
		});
	}

	function controlViewSaveArmazon(){
		$scope.mostrarArmazon = true;
		$scope.mostrarDeleteArmazon = false;
		$scope.saveBotonArmazon = true;
		$scope.updateBotonArmazon = false;
		$scope.deleteBotonArmazon = false;
		$scope.titulo = "Nuevo Armazon";
		
	}
	function controlViewUpdateArmazon(){
		$scope.mostrarArmazon = true;
		$scope.mostrarDeleteArmazon = false;
		$scope.saveBotonArmazon = false;
		$scope.updateBotonArmazon = true;
		$scope.deleteBotonArmazon = false;
		$scope.titulo = "Actualizar Armazon";

	}
	function controlViewDeleteArmazon(){
		$scope.mostrarArmazon = false;
		$scope.mostrarDeleteArmazon = true;
		$scope.saveBotonArmazon = false;
		$scope.updateBotonArmazon = false;
		$scope.deleteBotonArmazon = true;
		$scope.titulo = "Eliminar Armazon";

	}

	$scope.addArmazon = function(){
		controlViewSaveArmazon();
		$scope.Armazon = "";
	}
	
	$scope.saveArmazon = function(){
		var info = {
			Armazon: $scope.Armazon
		}
		console.log(info);
		ServiceMirosc.addArmazon(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadArmazon();
			}
			else{

			}
		});
	}

	$scope.modificaArmazon = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchArmazon(info).then(function(respuesta){
			$scope.idArmazonH = respuesta[0].idArmazon
			$scope.Armazon = respuesta[0].Armazon
		});        
		controlViewUpdateArmazon();
	}

	$scope.updateArmazon = function(){
		var info = {
			id: $scope.idArmazonH,
			Armazon: $scope.Armazon
		}
		ServiceMirosc.modifyArmazon($scope.idArmazonH,info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadArmazon();
			}
		}); 
	}

	$scope.deleteAlertArmazon = function(id){
		controlViewDeleteArmazon();
		ServiceMirosc.deleteArmazon(id).then(function(respuesta){
			console.log(respuesta)
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadArmazon();
			}
		}); 
	}
});

app.controller('TamForm', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarTamForm = false;
	$scope.saveBotonTamForm = false;
	$scope.updateBotonTamForm = false;
	$scope.deleteBotonTamForm = false;

	loadTamForm();

	function loadTamForm(){
		ServiceMirosc.TamForm().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisTamForm = respuesta;
		});
	}

	function controlViewSaveTamForm(){
		$scope.mostrarTamForm = true;
		$scope.mostrarDeleteTamForm = false;
		$scope.saveBotonTamForm = true;
		$scope.updateBotonTamForm = false;
		$scope.deleteBotonTamForm = false;
		$scope.titulo = "Nuevo Tama\u00F1o Frente";
		
	}
	function controlViewUpdateTamForm(){
		$scope.mostrarTamForm = true;
		$scope.mostrarDeleteTamForm = false;
		$scope.saveBotonTamForm = false;
		$scope.updateBotonTamForm = true;
		$scope.deleteBotonTamForm = false;
		$scope.titulo = "Actualizar Tama\u00F1o Frente";

	}
	function controlViewDeleteTamForm(){
		$scope.mostrarTamForm = false;
		$scope.mostrarDeleteTamForm = true;
		$scope.saveBotonTamForm = false;
		$scope.updateBotonTamForm = false;
		$scope.deleteBotonTamForm = true;
		$scope.titulo = "Eliminar Tama\u00F1o Frente";

	}

	$scope.addTamForm = function(){
		controlViewSaveTamForm();
		$scope.TamForm = "";
	}
	
	$scope.saveTamForm = function(){
		var info = {
			TamForm: $scope.TamForm,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addTamForm(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamForm();
			}
			else{

			}
		});
	}

	$scope.modificaTamForm = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchTamForm(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idTamFormH = respuesta[0].id_TamForm
			$scope.TamForm = respuesta[0].TamForm
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateTamForm();
	}

	$scope.updateTamForm = function(){
		var info = {
			id: $scope.idTamFormH,
			descripcion: $scope.descripcion,
			TamForm: $scope.TamForm
		}
		//console.log(info);
		ServiceMirosc.modifyTamForm($scope.idTamFormH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamForm();
			}
		}); 
	}

	$scope.deleteAlertTamForm = function(id){
		controlViewDeleteTamForm();
		ServiceMirosc.deleteTamForm(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamForm();
			}
		}); 
	}
});

app.controller('TamTerm', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarTamTerm = false;
	$scope.saveBotonTamTerm = false;
	$scope.updateBotonTamTerm = false;
	$scope.deleteBotonTamTerm = false;

	loadTamTerm();

	function loadTamTerm(){
		ServiceMirosc.TamTerm().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisTamTerm = respuesta;
		});
	}

	function controlViewSaveTamTerm(){
		$scope.mostrarTamTerm = true;
		$scope.mostrarDeleteTamTerm = false;
		$scope.saveBotonTamTerm = true;
		$scope.updateBotonTamTerm = false;
		$scope.deleteBotonTamTerm = false;
		$scope.titulo = "Nuevo Tama\u00F1o Terminal";
		
	}
	function controlViewUpdateTamTerm(){
		$scope.mostrarTamTerm = true;
		$scope.mostrarDeleteTamTerm = false;
		$scope.saveBotonTamTerm = false;
		$scope.updateBotonTamTerm = true;
		$scope.deleteBotonTamTerm = false;
		$scope.titulo = "Actualizar Tama\u00F1o Terminal";

	}
	function controlViewDeleteTamTerm(){
		$scope.mostrarTamTerm = false;
		$scope.mostrarDeleteTamTerm = true;
		$scope.saveBotonTamTerm = false;
		$scope.updateBotonTamTerm = false;
		$scope.deleteBotonTamTerm = true;
		$scope.titulo = "Eliminar Tama\u00F1o Terminal";

	}

	$scope.addTamTerm = function(){
		controlViewSaveTamTerm();
		$scope.TamTerm = "";
	}
	
	$scope.saveTamTerm = function(){
		var info = {
			TamTerm: $scope.TamTerm,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addTamTerm(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamTerm();
			}
			else{

			}
		});
	}

	$scope.modificaTamTerm = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchTamTerm(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idTamTermH = respuesta[0].id_TamTerm
			$scope.TamTerm = respuesta[0].TamTerm
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateTamTerm();
	}

	$scope.updateTamTerm = function(){
		var info = {
			id: $scope.idTamTermH,
			descripcion: $scope.descripcion,
			TamTerm: $scope.TamTerm
		}
		//console.log(info);
		ServiceMirosc.modifyTamTerm($scope.idTamTermH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamTerm();
			}
		}); 
	}

	$scope.deleteAlertTamTerm = function(id){
		controlViewDeleteTamTerm();
		ServiceMirosc.deleteTamTerm(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamTerm();
			}
		}); 
	}
});

app.controller('TamPuente', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarTamPuente = false;
	$scope.saveBotonTamPuente = false;
	$scope.updateBotonTamPuente = false;
	$scope.deleteBotonTamPuente = false;

	loadTamPuente();

	function loadTamPuente(){
		ServiceMirosc.TamPuente().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisTamPuente = respuesta;
		});
	}

	function controlViewSaveTamPuente(){
		$scope.mostrarTamPuente = true;
		$scope.mostrarDeleteTamPuente = false;
		$scope.saveBotonTamPuente = true;
		$scope.updateBotonTamPuente = false;
		$scope.deleteBotonTamPuente = false;
		$scope.titulo = "Nuevo Tama\u00F1o Puente";
		
	}
	function controlViewUpdateTamPuente(){
		$scope.mostrarTamPuente = true;
		$scope.mostrarDeleteTamPuente = false;
		$scope.saveBotonTamPuente = false;
		$scope.updateBotonTamPuente = true;
		$scope.deleteBotonTamPuente = false;
		$scope.titulo = "Actualizar Tama\u00F1o Puente";

	}
	function controlViewDeleteTamPuente(){
		$scope.mostrarTamPuente = false;
		$scope.mostrarDeleteTamPuente = true;
		$scope.saveBotonTamPuente = false;
		$scope.updateBotonTamPuente = false;
		$scope.deleteBotonTamPuente = true;
		$scope.titulo = "Eliminar Tama\u00F1o Puente";

	}

	$scope.addTamPuente = function(){
		controlViewSaveTamPuente();
		$scope.TamPuente = "";
	}
	
	$scope.saveTamPuente = function(){
		var info = {
			TamPuente: $scope.TamPuente,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addTamPuente(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamPuente();
			}
			else{

			}
		});
	}

	$scope.modificaTamPuente = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchTamPuente(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idTamPuenteH = respuesta[0].id_TamPuente
			$scope.TamPuente = respuesta[0].TamPuente
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateTamPuente();
	}

	$scope.updateTamPuente = function(){
		var info = {
			id: $scope.idTamPuenteH,
			descripcion: $scope.descripcion,
			TamPuente: $scope.TamPuente
		}
		//console.log(info);
		ServiceMirosc.modifyTamPuente($scope.idTamPuenteH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamPuente();
			}
		}); 
	}

	$scope.deleteAlertTamPuente = function(id){
		controlViewDeleteTamPuente();
		ServiceMirosc.deleteTamPuente(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadTamPuente();
			}
		}); 
	}
});



app.controller('ColFrente', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarColFrente = false;
	$scope.saveBotonColFrente = false;
	$scope.updateBotonColFrente = false;
	$scope.deleteBotonColFrente = false;

	loadColFrente();

	function loadColFrente(){
		ServiceMirosc.ColFrente().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisColFrente = respuesta;
		});
	}

	function controlViewSaveColFrente(){
		$scope.mostrarColFrente = true;
		$scope.mostrarDeleteColFrente = false;
		$scope.saveBotonColFrente = true;
		$scope.updateBotonColFrente = false;
		$scope.deleteBotonColFrente = false;
		$scope.titulo = "Nuevo Color Frente";
		
	}
	function controlViewUpdateColFrente(){
		$scope.mostrarColFrente = true;
		$scope.mostrarDeleteColFrente = false;
		$scope.saveBotonColFrente = false;
		$scope.updateBotonColFrente = true;
		$scope.deleteBotonColFrente = false;
		$scope.titulo = "Actualizar Color Frente";

	}
	function controlViewDeleteColFrente(){
		$scope.mostrarColFrente = false;
		$scope.mostrarDeleteColFrente = true;
		$scope.saveBotonColFrente = false;
		$scope.updateBotonColFrente = false;
		$scope.deleteBotonColFrente = true;
		$scope.titulo = "Eliminar Color Frente";

	}

	$scope.addColFrente = function(){
		controlViewSaveColFrente();
		$scope.ColFrente = "";
	}
	
	$scope.saveColFrente = function(){
		var info = {
			ColFrente: $scope.ColFrente,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addColFrente(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColFrente();
			}
			else{

			}
		});
	}

	$scope.modificaColFrente = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchColFrente(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idColFrenteH = respuesta[0].id_ColFrente
			$scope.ColFrente = respuesta[0].ColFrente
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateColFrente();
	}

	$scope.updateColFrente = function(){
		var info = {
			id: $scope.idColFrenteH,
			descripcion: $scope.descripcion,
			ColFrente: $scope.ColFrente
		}
		//console.log(info);
		ServiceMirosc.modifyColFrente($scope.idColFrenteH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColFrente();
			}
		}); 
	}

	$scope.deleteAlertColFrente = function(id){
		controlViewDeleteColFrente();
		ServiceMirosc.deleteColFrente(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColFrente();
			}
		}); 
	}
});

app.controller('ColTerm', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarColTerm = false;
	$scope.saveBotonColTerm = false;
	$scope.updateBotonColTerm = false;
	$scope.deleteBotonColTerm = false;

	loadColTerm();

	function loadColTerm(){
		ServiceMirosc.ColTerm().then(function(respuesta) {
			//console.log(respuesta)
			//console.log(respuesta[0])
			$scope.MisColTerm = respuesta;
		});
	}

	function controlViewSaveColTerm(){
		$scope.mostrarColTerm = true;
		$scope.mostrarDeleteColTerm = false;
		$scope.saveBotonColTerm = true;
		$scope.updateBotonColTerm = false;
		$scope.deleteBotonColTerm = false;
		$scope.titulo = "Nuevo Color Terminal";
		
	}
	function controlViewUpdateColTerm(){
		$scope.mostrarColTerm = true;
		$scope.mostrarDeleteColTerm = false;
		$scope.saveBotonColTerm = false;
		$scope.updateBotonColTerm = true;
		$scope.deleteBotonColTerm = false;
		$scope.titulo = "Actualizar Color Terminal";

	}
	function controlViewDeleteColTerm(){
		$scope.mostrarColTerm = false;
		$scope.mostrarDeleteColTerm = true;
		$scope.saveBotonColTerm = false;
		$scope.updateBotonColTerm = false;
		$scope.deleteBotonColTerm = true;
		$scope.titulo = "Eliminar Color Terminal";

	}

	$scope.addColTerm = function(){
		controlViewSaveColTerm();
		$scope.ColTerm = "";
	}
	
	$scope.saveColTerm = function(){
		var info = {
			ColTerm: $scope.ColTerm,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addColTerm(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColTerm();
			}
			else{

			}
		});
	}

	$scope.modificaColTerm = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchColTerm(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idColTermH = respuesta[0].id_ColTerm
			$scope.ColTerm = respuesta[0].ColTerm
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateColTerm();
	}

	$scope.updateColTerm = function(){
		var info = {
			id: $scope.idColTermH,
			descripcion: $scope.descripcion,
			ColTerm: $scope.ColTerm
		}
		//console.log(info);
		ServiceMirosc.modifyColTerm($scope.idColTermH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColTerm();
			}
		}); 
	}

	$scope.deleteAlertColTerm = function(id){
		controlViewDeleteColTerm();
		ServiceMirosc.deleteColTerm(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadColTerm();
			}
		}); 
	}
});

app.controller('Producto', function ($scope, ServiceMirosc, serviceExtra) {
	window.scrollTo(0,0);
	
	$scope.mostrarProducto = false;
	$scope.saveBotonProducto = false;
	$scope.updateBotonProducto = false;
	$scope.deleteBotonProducto = false;
	$scope.tablaProducto = true;

	loadDatos();

	function loadDatos(){
		ServiceMirosc.Producto().then(function(respuesta) {
			$scope.MisProducto = respuesta;
		});
		ServiceMirosc.marcas().then(function(respuesta) {
			$scope.Mismarcas = respuesta;
		});
		ServiceMirosc.Armazon().then(function(respuesta) {
			$scope.MisArmazon = respuesta;
		});
	}

	function controlViewSaveProducto(){
		$scope.tablaProducto = false;
		$scope.mostrarProducto = true;
		$scope.mostrarDeleteProducto = false;
		$scope.saveBotonProducto = true;
		$scope.updateBotonProducto = false;
		$scope.deleteBotonProducto = false;
		$scope.titulo = "Producto Nuevo";
		
	}
	function controlViewUpdateProducto(){
		$scope.tablaProducto = false;
		$scope.mostrarProducto = true;
		$scope.mostrarDeleteProducto = false;
		$scope.saveBotonProducto = false;
		$scope.updateBotonProducto = true;
		$scope.deleteBotonProducto = false;
		$scope.titulo = "Actualizar Producto";

	}
	function controlViewDeleteProducto(){
		$scope.mostrarProducto = false;
		$scope.mostrarDeleteProducto = true;
		$scope.saveBotonProducto = false;
		$scope.updateBotonProducto = false;
		$scope.deleteBotonProducto = true;
		$scope.titulo = "Elimimar Producto";

	}

	$scope.addProducto = function(){
		controlViewSaveProducto();
		$scope.Producto = "";
	}
	
	$scope.saveProducto = function(){
		var info = {
			Producto: $scope.Producto,
			descripcion: $scope.descripcion
		}
		//console.log(info);
		ServiceMirosc.addProducto(info).then(function(respuesta){
			console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadProducto();
				$scope.tablaProducto = true;
			}
			else{

			}
		});
	}

	$scope.modificaProducto = function(id){
		var info = {
			id: id
		}
		ServiceMirosc.searchProducto(info).then(function(respuesta){
			//console.log(respuesta)
			$scope.idProductoH = respuesta[0].id_Producto
			$scope.Producto = respuesta[0].Producto
			$scope.descripcion = respuesta[0].descripcion
		});        
		controlViewUpdateProducto();
	}

	$scope.updateProducto = function(){
		var info = {
			id: $scope.idProductoH,
			descripcion: $scope.descripcion,
			Producto: $scope.Producto
		}
		//console.log(info);
		ServiceMirosc.modifyProducto($scope.idProductoH,info).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadProducto();
			}
		}); 
	}

	$scope.deleteAlertProducto = function(id){
		controlViewDeleteProducto();
		ServiceMirosc.deleteProducto(id).then(function(respuesta){
			//console.log(respuesta);
			if (respuesta.status == "OK"){
				serviceExtra.closeModal();
				loadProducto();
			}
		}); 
	}
});