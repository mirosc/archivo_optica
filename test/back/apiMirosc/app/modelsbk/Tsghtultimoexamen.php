<?php

//namespace tsght;

class Tsghtultimoexamen extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_ultimoexamenid;

    /**
     *
     * @var integer
     */
    public $cod_quejaid_fk;

    /**
     *
     * @var string
     */
    public $des_fechaaprox;

    /**
     *
     * @var double
     */
    public $cnu_od;

    /**
     *
     * @var double
     */
    public $cnu_oi;

    /**
     *
     * @var double
     */
    public $cnu_add;

    /**
     *
     * @var string
     */
    public $txt_anteojos;

    /**
     *
     * @var string
     */
    public $bol_eficacia;

    /**
     *
     * @var string
     */
    public $bol_comodidad;

    /**
     *
     * @var string
     */
    public $bol_necesidad;

    /**
     *
     * @var string
     */
    public $opcional;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtultimoexamen");
        $this->belongsTo('cod_quejaid_fk', 'tsght\Tsghtqueja', 'cod_quejaid', ['alias' => 'Tsghtqueja']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtultimoexamen';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtultimoexamen[]|Tsghtultimoexamen|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtultimoexamen|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
