<?php

//namespace tsght;

class Tsghtpacientes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_pacienteid;

    /**
     *
     * @var string
     */
    public $des_nombre;

    /**
     *
     * @var string
     */
    public $des_paterno;

    /**
     *
     * @var string
     */
    public $des_materno;

    /**
     *
     * @var integer
     */
    public $cnu_pais;

    /**
     *
     * @var double
     */
    public $cnu_celular;

    /**
     *
     * @var double
     */
    public $cnu_telcasa;

    /**
     *
     * @var string
     */
    public $bol_whats;

    /**
     *
     * @var string
     */
    public $bol_telcasa;

    /**
     *
     * @var string
     */
    public $bol_activo;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtpacientes");
        $this->hasMany('cod_pacienteid', 'tsght\Tsghtdatosgeneral', 'cod_pacienteid_fk', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtpacientes';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtpacientes[]|Tsghtpacientes|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtpacientes|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
