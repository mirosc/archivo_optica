<?php

//namespace tsght;

class Tsghtantocularpx extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_antocularpxid;

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     *
     * @var string
     */
    public $bol_golpes;

    /**
     *
     * @var string
     */
    public $txt_ubicacion1;

    /**
     *
     * @var string
     */
    public $txt_tx1;

    /**
     *
     * @var string
     */
    public $txt_infecciones;

    /**
     *
     * @var string
     */
    public $txt_ubicacion2;

    /**
     *
     * @var string
     */
    public $txt_tx2;

    /**
     *
     * @var string
     */
    public $txt_desviacion_o;

    /**
     *
     * @var string
     */
    public $txt_tx3;

    /**
     *
     * @var string
     */
    public $opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtantocularpx");
        $this->belongsTo('cod_datogeneralid_fk', 'tsght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtantocularpx';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtantocularpx[]|Tsghtantocularpx|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtantocularpx|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
