<?php

//namespace tsght;

class Tsghtqueja extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_quejaid;

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     *
     * @var string
     */
    public $txt_causa;

    /**
     *
     * @var string
     */
    public $txt_problema;

    /**
     *
     * @var string
     */
    public $des_cuando;

    /**
     *
     * @var string
     */
    public $bol_vlejana;

    /**
     *
     * @var string
     */
    public $bol_vcercana;

    /**
     *
     * @var string
     */
    public $bol_vdoble;

    /**
     *
     * @var string
     */
    public $bol_ardor;

    /**
     *
     * @var string
     */
    public $bol_comezon;

    /**
     *
     * @var string
     */
    public $bol_secrecion;

    /**
     *
     * @var string
     */
    public $bol_fatiga;

    /**
     *
     * @var string
     */
    public $bol_epiforia;

    /**
     *
     * @var string
     */
    public $bol_ojoseco;

    /**
     *
     * @var string
     */
    public $bol_ojorojo;

    /**
     *
     * @var string
     */
    public $bol_fotofobia;

    /**
     *
     * @var string
     */
    public $bol_moscas;

    /**
     *
     * @var string
     */
    public $bol_flashes;

    /**
     *
     * @var string
     */
    public $bol_auras;

    /**
     *
     * @var integer
     */
    public $opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtqueja");
        $this->hasMany('cod_quejaid', 'tsght\Tsghtultimoexamen', 'cod_quejaid_fk', ['alias' => 'Tsghtultimoexamen']);
        $this->belongsTo('cod_datogeneralid_fk', 'tsght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtqueja';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtqueja[]|Tsghtqueja|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtqueja|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
