<?php

//namespace tsght;

class Tsghtantmedicopx extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     *
     * @var string
     */
    public $des_fecultexammed;

    /**
     *
     * @var string
     */
    public $bol_salud;

    /**
     *
     * @var string
     */
    public $bol_diabetes;

    /**
     *
     * @var string
     */
    public $bol_preionarter;

    /**
     *
     * @var string
     */
    public $bol_glaucoma;

    /**
     *
     * @var string
     */
    public $bol_cirugias;

    /**
     *
     * @var string
     */
    public $txt_tx1;

    /**
     *
     * @var string
     */
    public $bol_traumatismo;

    /**
     *
     * @var string
     */
    public $bol_dolorcabeza;

    /**
     *
     * @var string
     */
    public $bol_alergias;

    /**
     *
     * @var string
     */
    public $txt_tx2;

    /**
     *
     * @var string
     */
    public $bol_sermedicos;

    /**
     *
     * @var string
     */
    public $txt_cuales;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     *
     * @var integer
     */
    public $cod_antmedpxid;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtantmedicopx");
        $this->belongsTo('cod_datogeneralid_fk', 'tsght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtantmedicopx';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtantmedicopx[]|Tsghtantmedicopx|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtantmedicopx|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
