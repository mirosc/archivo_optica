<?php

//namespace tsght;

class Tsghtrxpaciente extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_rxpacienteid;

    /**
     *
     * @var string
     */
    public $des_ladoojo;

    /**
     *
     * @var double
     */
    public $cnu_esferico;

    /**
     *
     * @var double
     */
    public $cnu_cilindro;

    /**
     *
     * @var integer
     */
    public $cnu_eje;

    /**
     *
     * @var integer
     */
    public $cnu_add;

    /**
     *
     * @var integer
     */
    public $cnu_dnp;

    /**
     *
     * @var integer
     */
    public $cnu_dip;

    /**
     *
     * @var integer
     */
    public $cnu_altura;

    /**
     *
     * @var double
     */
    public $cnu_prisma;

    /**
     *
     * @var integer
     */
    public $opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtrxpaciente");
        $this->belongsTo('cod_datogeneralid_fk', 'tsght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtrxpaciente';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtrxpaciente[]|Tsghtrxpaciente|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtrxpaciente|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
