<?php

//namespace tsght;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Tsghtdatosgeneral extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid;

    /**
     *
     * @var integer
     */
    public $cod_pacienteid_fk;

    /**
     *
     * @var string
     */
    public $fec_registro;

    /**
     *
     * @var string
     */
    public $fec_nacimiento;

    /**
     *
     * @var string
     */
    public $txt_hobbies;

    /**
     *
     * @var string
     */
    public $des_calle;

    /**
     *
     * @var integer
     */
    public $cnu_numexterior;

    /**
     *
     * @var string
     */
    public $des_numinterior;

    /**
     *
     * @var string
     */
    public $des_colonia;

    /**
     *
     * @var integer
     */
    public $cnu_cp;

    /**
     *
     * @var string
     */
    public $des_municipio;

    /**
     *
     * @var string
     */
    public $des_estado;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $des_ocupacion;

    /**
     *
     * @var string
     */
    public $txt_factores;

    /**
     *
     * @var string
     */
    public $des_lugartrabajo;

    /**
     *
     * @var string
     */
    public $des_opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtdatosgeneral");
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtantmedicofam', 'cod_datogeneralid_fk', ['alias' => 'Tsghtantmedicofam']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtantmedicopx', 'cod_datogeneralid_fk', ['alias' => 'Tsghtantmedicopx']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtantocularpx', 'cod_datogeneralid_fk', ['alias' => 'Tsghtantocularpx']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtantoculfam', 'cod_datogeneralid_fk', ['alias' => 'Tsghtantoculfam']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtexamenpx', 'cod_datogeneralid_fk', ['alias' => 'Tsghtexamenpx']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtobservaciongen', 'cod_datogeneralid_fk', ['alias' => 'Tsghtobservaciongen']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtqueja', 'cod_datogeneralid_fk', ['alias' => 'Tsghtqueja']);
        $this->hasMany('cod_datogeneralid', 'tsght\Tsghtrxpaciente', 'cod_datogeneralid_fk', ['alias' => 'Tsghtrxpaciente']);
        $this->belongsTo('cod_pacienteid_fk', 'tsght\Tsghtpacientes', 'cod_pacienteid', ['alias' => 'Tsghtpacientes']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtdatosgeneral';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtdatosgeneral[]|Tsghtdatosgeneral|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtdatosgeneral|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
