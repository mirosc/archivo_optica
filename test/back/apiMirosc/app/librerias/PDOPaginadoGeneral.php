<?php

class PDOPaginadoGeneral {
    /**
     * Crear la matriz de salida de datos para las filas de DataTables
     *
     * @param array $columns Arreglo de informacion para columnas 
     * @param array $data    Datos para la consulta del GET
     * @param bool  $isJoin  Deetermina la consulta del JOIN para el metodo complex para una o mas consultas
     *
     * @return array Formatea los datos basados en ina fila
     */
    static function data_output ( $columns, $data, $isJoin = false )
    {
        $out = array();
        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();
            for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                $column = $columns[$j];
                // Is there a formatter?
                if ( isset( $column['formatter'] ) ) {
                    $row[ $column['dt'] ] = ($isJoin) ? $column['formatter']( $data[$i][ $column['field'] ], $data[$i] ) : $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
                }
                else {
                    $row[ $column['dt'] ] = ($isJoin) ? $data[$i][ $columns[$j]['field'] ] : $data[$i][ $columns[$j]['db'] ];
                }
            }
            $out[] = $row;
        }
        return $out;
    }
    /**
     * Paginado
     *
     * Construya la cláusula LIMIT para la consulta SQL de procesamiento del lado del servidor
     *
     *  @param  array $request Informacion enviada al servidor por el DataTables
     *  @param  array $columns Arreglo de columnas 
     *  @return string SQL clausula LIMIT
     */
    static function limit ( $request, $columns )
    {
        $limit = '';
        if ( isset($request['start']) && $request['length'] != -1 ) {
            $limit = "LIMIT ".intval($request['start']).", ".intval($request['length']);
        }
        return $limit;
    }
    /**
     * Ordenamiento
     *
     * Construya la cláusula LIMIT para la consulta SQL de procesamiento del lado del servidor
     *
     *  @param  array $request Informacion enviada al servidor por el DataTables
     *  @param  array $columns Arreglo de columnas 
     *  @param bool  $isJoin  Deetermina la consulta del JOIN para el metodo complex para una o mas consultas
     *
     *  @return string SQL clausula orde de ordenamiento
     */
    static function order ( $request, $columns, $isJoin = false )
    {
        $order = '';
        if ( isset($request['order']) && count($request['order']) ) {
            $orderBy = array();
            $dtColumns = PDOPaginadoGeneral::pluck( $columns, 'dt' );
            for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];
                if ( $requestColumn['orderable'] == 'true' ) {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';
                    $orderBy[] = ($isJoin) ? $column['db'].' '.$dir : '`'.$column['db'].'` '.$dir;
                }
            }
            $order = 'ORDER BY '.implode(', ', $orderBy);
        }
        return $order;
    }
    /**
     * Buscando y validando
     *
     * Construya la cláusula WHERE para la consulta SQL de procesamiento del lado del servidor.
     *
     * NOTA: esto no coincide con el filtrado integrado de DataTables que lo hace
     * palabra por palabra en cualquier campo. Es posible hacer aquí el rendimiento en grande
     * las bases de datos para que serían muy pobres
     *
     *  @param  array $request nformacion enviada al servidor por el DataTables
     *  @param  array $columns Arreglo de columnas 
     *  @param  array $bindings Matriz de valores para enlaces PDO, utilizada en la función sql_exec ()
     *  @param  bool  $isJoin  Determine la consulta JOIN / complex o simple
     *
     *  @return string SQL where clause
     */
    static function filter ( $request, $columns, &$bindings, $isJoin = false )
    {
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = PDOPaginadoGeneral::pluck( $columns, 'dt' );
        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $str = $request['search']['value'];
            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];
                if ( $requestColumn['searchable'] == 'true' ) {
                    $binding = PDOPaginadoGeneral::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
                    $globalSearch[] = ($isJoin) ? $column['db']." LIKE ".$binding : "`".$column['db']."` LIKE ".$binding;
                }
            }
        }
        // Individual column filtering
        for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
            $requestColumn = $request['columns'][$i];
            $columnIdx = array_search( $requestColumn['data'], $dtColumns );
            $column = $columns[ $columnIdx ];
            $str = $requestColumn['search']['value'];
            if ( $requestColumn['searchable'] == 'true' &&
                $str != '' ) {
                $binding = PDOPaginadoGeneral::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
                $columnSearch[] = ($isJoin) ? $column['db']." LIKE ".$binding : "`".$column['db']."` LIKE ".$binding;
            }
        }
        // Combina los filtros en una sola cadena
        $where = '';
        if ( count( $globalSearch ) ) {
            $where = '('.implode(' OR ', $globalSearch).')';
        }
        if ( count( $columnSearch ) ) {
            $where = $where === '' ?
                implode(' AND ', $columnSearch) :
                $where .' AND '. implode(' AND ', $columnSearch);
        }
        if ( $where !== '' ) {
            $where = 'WHERE '.$where;
        }
        return $where;
    }
    /**
     *
     * Realice las consultas SQL necesarias para un procesamiento del lado del servidor solicitado utilizando las funciones auxiliares de esta clase, limit (), order () y filter () entre otros. La matriz devuelta está lista para ser codificada como JSON en respuesta a una solicitud general de PDO, o puede modificarse si es necesario antes enviando de vuelta al cliente.
     *
     *  @param  array $request nformacion enviada al servidor por el DataTables
     *  @param  array $sql_details SQL detalles de conexión - ver sql_connect ()
     *  @param  string $table SQL tabla para consultar
     *  @param  string $primaryKey Clave principal de la tabla
     *  @param  array $columns Arreglo de columnas 
     *  @param  array $joinQuery Unirse a la cadena de consulta
     *  @param  string $extraWhere Where Donde cadena de string
     *  @param  string $groupBy groupBy por cualquier campo se aplicará
     *  @param  string $having HAVING bajo cualquier condición se aplicará
     *
     *  @return array  Matriz de respuesta de procesamiento del lado del servidor
     *
     */
    static function simple ( $request, $sql_details, $table, $primaryKey, $columns, $joinQuery = NULL, $extraWhere = '', $groupBy = '', $having = '')
    {
        $bindings = array();
        $db = PDOPaginadoGeneral::sql_connect( $sql_details );
        // Construyendo la cadena de consulta SQL a partir de la solicitud
        $limit = PDOPaginadoGeneral::limit( $request, $columns );
        $order = PDOPaginadoGeneral::order( $request, $columns, $joinQuery );
        $where = PDOPaginadoGeneral::filter( $request, $columns, $bindings, $joinQuery );
		// IF Extra donde se establece, luego establecer y preparar consulta
        if($extraWhere)
            $extraWhere = ($where) ? ' AND '.$extraWhere : ' WHERE '.$extraWhere;
        $groupBy = ($groupBy) ? ' GROUP BY '.$groupBy .' ' : '';
        $having = ($having) ? ' HAVING '.$having .' ' : '';
        // Consulta principal para obtener realmente los datos
        if($joinQuery){
            $col = PDOPaginadoGeneral::pluck($columns, 'db', $joinQuery);
            $query =  "SELECT SQL_CALC_FOUND_ROWS ".implode(", ", $col)."
			 $joinQuery
			 $where
			 $extraWhere
			 $groupBy
       $having
			 $order
			 $limit";
        }else{
            $query =  "SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", PDOPaginadoGeneral::pluck($columns, 'db'))."`
			 FROM `$table`
			 $where
			 $extraWhere
			 $groupBy
       $having
			 $order
			 $limit";
        }
        //echo $query;
        $data = PDOPaginadoGeneral::sql_exec( $db, $bindings,$query);
        // Longitud del conjunto de datos después de filtrar
        $resFilterLength = PDOPaginadoGeneral::sql_exec( $db,
            "SELECT FOUND_ROWS()"
        );
        $recordsFiltered = $resFilterLength[0][0];
        // Longitud total del conjunto de datos
        $resTotalLength = PDOPaginadoGeneral::sql_exec( $db,
            "SELECT COUNT(`{$primaryKey}`)
			 FROM   `$table`"
        );
        $recordsTotal = $resTotalLength[0][0];
        /*
         * Salida
         */
        return array(
            "draw"            => intval( $request['draw'] ),
            "recordsTotal"    => intval( $recordsTotal ),
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => PDOPaginadoGeneral::data_output( $columns, $data, $joinQuery )
        );
    }
    /**
     * Conexion a la base de datos
     *
     * @param  array $sql_details Matriz de detalles de conexión del servidor SQL, con
     *   propiedades:
     *     * host -  nombre del host
     *     * db   - dnombre de la base de datos
     *     * user - nombre del usuario
     *     * pass - contraseña del usuario
     * @return resource retorna la conexion de la base de datros
     */
    static function sql_connect ( $sql_details )
    {
        try {
            $db = @new PDO(
                "mysql:host={$sql_details['host']};dbname={$sql_details['db']}",
                $sql_details['user'],
                $sql_details['pass'],
                array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION )
            );
            $db->query("SET NAMES 'utf8'");
        }
        catch (PDOException $e) {
            PDOPaginadoGeneral::fatal(
                "An error occurred while connecting to the database. ".
                "The error reported by the server was: ".$e->getMessage()
            );
        }
        return $db;
    }
    /**
     * Ejecutar una consulta SQL en la base de datos
     *
     * @param  resource $db  Base de datos
     * @param  array    $bindings  Matriz de valores de enlace de PDO de bind () que se utilizarán para escapes seguros de cadenas. Tenga en cuenta que esto se puede dar como la cadena de consulta SQL si no se requieren enlaces.
     * @param  string   $sql SQL Ejecución de consultas
     * @return array         Resultado de la consulta (todas las filas)
     */
    static function sql_exec ( $db, $bindings, $sql=null )
    {
        // Argument shiftingArgumento cambiando
        if ( $sql === null ) {
            $sql = $bindings;
        }
        $stmt = $db->prepare( $sql );
        //echo $sql;
        // Bindeando los parametros
        if ( is_array( $bindings ) ) {
            for ( $i=0, $ien=count($bindings) ; $i<$ien ; $i++ ) {
                $binding = $bindings[$i];
                $stmt->bindValue( $binding['key'], $binding['val'], $binding['type'] );
            }
        }
        // Ejecutando la Query
        try {
            $stmt->execute();
        }
        catch (PDOException $e) {
            PDOPaginadoGeneral::fatal( "An SQL error occurred: ".$e->getMessage() );
        }
        // Retornando los datos
        return $stmt->fetchAll();
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Métodos internos
     */
    /**
     * Lanza un error fatal.
     *
     * Esto escribe un mensaje de error en una cadena JSON que DataTables verá y mostrará al usuario en el navegador.
     *
     * @param  string $msg Message to send to the client
     */
    static function fatal ( $msg )
    {
        echo json_encode( array(
            "error" => $msg
        ) );
        exit(0);
    }
    /**
     * Cree una clave de enlace PDO que se pueda usar para escanear variables de forma segura al ejecutar una consulta con sql_exec ()
     *
     * @param  array &$a    Matriz de enlaces
     * @param  *      $val  Valor para unir
     * @param  int    $type Tipo de campo PDO
     * @return string       La clave enlazada se usará en el SQL donde se usará este parámetro.
     */
    static function bind ( &$a, $val, $type )
    {
        $key = ':binding_'.count( $a );
        $a[] = array(
            'key' => $key,
            'val' => $val,
            'type' => $type
        );
        return $key;
    }
    /**
     * Tire de una propiedad particular de cada asociación. array en una matriz numérica, eturning y matriz de los valores de las propiedades de cada elemento.
     *
     *  @param  array  $a    Matriz para obtener datos de
     *  @param  string $prop Propiedad para leer
     *  @param  bool  $isJoin  Determine la consulta JOIN / complex o simple
     *  @return array        Matriz de valores de propiedad
     */
    static function pluck ( $a, $prop, $isJoin = false )
    {
        $out = array();
        for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
            $out[] = ($isJoin && isset($a[$i]['as'])) ? $a[$i][$prop]. ' AS '.$a[$i]['as'] : $a[$i][$prop];
        }
        return $out;
    }
}