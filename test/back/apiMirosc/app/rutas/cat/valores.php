<?php
//todos los 
$app->get('/api/cat/valores', function () use ($app) {

	$query = "SELECT *
	FROM cat.Tcatvalorprueba 
	ORDER BY 1";
	//$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
	$result = $app->modelsManager->executeQuery($query);

	$datos = array();
	foreach ($result as $fila) {
		$datos[] = array(
			'cod_valorpruebaid'   => $fila->cod_valorpruebaid,
			'cnu_valor'   => $fila->cnu_valor
			
		);
	}

	header('Access-Control-Allow-Origin: *'); 
	echo json_encode($datos);
});

$app->get('/api/cat/eje', function () use ($app) {

	$query = "SELECT *
	FROM cat.Tcateje
	ORDER BY 1";
	//$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
	$result = $app->modelsManager->executeQuery($query);

	$datos = array();
	foreach ($result as $fila) {
		$datos[] = array(
			'cod_ejeid'   => $fila->cod_ejeid,
			'cnu_valor'   => $fila->cnu_valor
			
		);
	}

	header('Access-Control-Allow-Origin: *'); 
	echo json_encode($datos);
});

$app->get('/api/cat/esferico', function () use ($app) {

	$query = "SELECT *
	FROM cat.Tcatesferico 
	ORDER BY 1";
	//$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
	$result = $app->modelsManager->executeQuery($query);

	$datos = array();
	foreach ($result as $fila) {
		$datos[] = array(
			'cod_esfericoid'   => $fila->cod_esfericoid,
			'cnu_valor'   => $fila->cnu_valor
			
		);
	}

	header('Access-Control-Allow-Origin: *'); 
	echo json_encode($datos);
});