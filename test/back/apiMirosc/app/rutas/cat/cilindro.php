<?php

$app->get('/api/cat/cilindro', function () use ($app) {

	$query = "SELECT *
	FROM cat.Tcatcilindro
	ORDER BY 1";
	//$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
	$result = $app->modelsManager->executeQuery($query);

	$datos = array();
	foreach ($result as $fila) {
		$datos[] = array(
			'cod_cilindroid'   => $fila->cod_cilindroid,
			'cnu_valor'   => $fila->cnu_valor
			
		);
	}

	header('Access-Control-Allow-Origin: *'); 
	echo json_encode($datos);
});