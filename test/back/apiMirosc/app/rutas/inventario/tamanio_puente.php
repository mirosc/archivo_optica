<?php
//todos los 
$app->get('/api/inventario/tamanio_puente', function () use ($app) {

    $query = "SELECT * FROM inventario.TamanioPuente";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_tamanio_puente'   => $fila->id_tamanio_puente,
            'tamanio_puente'   => $fila->tamanio_puente
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});


$app->post('/api/inventario/searchtamanio_puente', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT *
                FROM inventario.TamanioPuente
                WHERE id_tamanio_puente = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_tamanio_puente'   => $fila->id_tamanio_puente,
            'tamanio_puente'   => $fila->tamanio_puente
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/inventario/tamanio_puente/new', function() use ($app){
    //obtenemos el json que se ha enviado 
    $json = $app->request->getJsonRawBody();
    header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query
    $query = "INSERT INTO inventario.TamanioPuente(tamanio_puente, status) 
    VALUES (:tamanio_puente:, :status:)";

    $result = $app->modelsManager->executeQuery($query, array(        
        'tamanio_puente' => $json->tamanio_puente,
        'status' => 'true'
    ));

        //comprobamos si el insert se ha llevado a cabo
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
        $errors = array();
        foreach ($result->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }

    return $response;
});

/*
curl -i -X PUT -d '{"descripcion":"ASIMO"}' http://93.188.163.213/Test/apiMirosc/api/inventario/tamanio_puente/update/10
*/
$app->put('/api/inventario/tamanio_puente/update/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE inventario.TamanioPuente AS tamanio_puente SET tamanio_puente.tamanio_puente = :tamanio_puente:, tamanio_puente.status = :status: WHERE tamanio_puente.id_tamanio_puente = :id:";
    //$query = "UPDATE inventario.TamanioPuenteAS M SET M.tamanio_puente = :tamanio_puente: WHERE M.id_tamanio_puente = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'true',
        'tamanio_puente' => $json->tamanio_puente
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});

$app->put('/api/inventario/tamanio_puente/delete/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE inventario.TamanioPuente AS tamanio_puente SET tamanio_puente.status = :status: WHERE tamanio_puente.id_tamanio_puente = :id:";
    //$query = "UPDATE inventario.TamanioPuenteAS M SET M.descripcion = :descripcion: WHERE M.id_tamanio_puente = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'false'
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $id));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});