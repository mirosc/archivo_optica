<?php
//todos los 
$app->get('/api/inventario/material', function () use ($app) {

    $query = "SELECT * FROM inventario.Material WHERE status = 't'";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_material'   => $fila->id_material,
            'material'   => $fila->material
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});


$app->post('/api/inventario/searchMaterial', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT *
                FROM inventario.Material
                WHERE id_material = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_material'   => $fila->id_material,
            'material'   => $fila->material
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/inventario/Material/new', function() use ($app){
    //obtenemos el json que se ha enviado 
    $json = $app->request->getJsonRawBody();
    header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query
    $query = "INSERT INTO inventario.Material(material, status) 
    VALUES (:material:, :status:)";

    $result = $app->modelsManager->executeQuery($query, array(        
        'material' => $json->material,
        'status' => 'true'
    ));

        //comprobamos si el insert se ha llevado a cabo
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
        $errors = array();
        foreach ($result->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }

    return $response;
});

/*
curl -i -X PUT -d '{"descripcion":"ASIMO"}' http://93.188.163.213/Test/apiMirosc/api/inventario/Material/update/10
*/
$app->put('/api/inventario/Material/update/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE inventario.Material AS material SET material.material = :material:, material.status = :status: WHERE material.id_material = :id:";
    //$query = "UPDATE inventario.MaterialAS M SET M.material = :material: WHERE M.id_material = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'true',
        'material' => $json->material
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});

$app->put('/api/inventario/Material/delete/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE inventario.Material AS material SET material.status = :status: WHERE material.id_material = :id:";
    //$query = "UPDATE inventario.MaterialAS M SET M.descripcion = :descripcion: WHERE M.id_material = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'false'
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $id));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});