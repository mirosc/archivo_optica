<?php
//todos los 
$app->get('/api/inventario/marcas', function () use ($app) {

    $query = "SELECT marca.id_marca, marca.marca, COUNT(marca.marca) AS cantidad FROM inventario.Marcas AS marca WHERE status = 't' GROUP BY 1,2";
    //$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_marca'   => $fila->id_marca,
            'marca'   => $fila->marca,
            'cantidad'   => $fila->cantidad
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});


$app->post('/api/inventario/searchMarca', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT *
                FROM inventario.Marcas
                WHERE id_marca = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_marca'   => $fila->id_marca,
            'marca'   => $fila->marca
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/inventario/marca/new', function() use ($app){
    //obtenemos el json que se ha enviado 
    $json = $app->request->getJsonRawBody();
    header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query
    $query = "INSERT INTO inventario.Marcas (marca, status) 
    VALUES (:marca:, :status:)";

    $result = $app->modelsManager->executeQuery($query, array(        
        'marca' => $json->marca,
        'status' => 'true'
    ));

        //comprobamos si el insert se ha llevado a cabo
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
        $errors = array();
        foreach ($result->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }

    return $response;
});

/*
curl -i -X PUT -d '{"descripcion":"ASIMO"}' http://93.188.163.213/Test/apiMirosc/api/inventario/marca/update/10
*/
$app->put('/api/inventario/marca/update/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE inventario.Marcas as marcas SET marcas.marca = :marca:, marcas.fecha_mod = now(), marcas.status = :status: WHERE marcas.id_marca = :id:";
    //$query = "UPDATE inventario.Marca AS M SET M.marca = :marca: WHERE M.id_marca = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'true',
        'marca' => $json->marca
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});

$app->put('/api/inventario/marca/delete/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE inventario.Marcas as marcas SET marcas.status = :status: WHERE marcas.id_marca = :id:";
    //$query = "UPDATE inventario.Marca AS M SET M.descripcion = :descripcion: WHERE M.id_marca = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'f'
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $id));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});