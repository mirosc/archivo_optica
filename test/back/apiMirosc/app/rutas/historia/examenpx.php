<?php

$app->post('/api/historia/examenpx/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtexamenpx(
	cod_datogeneralid_fk, des_ladoojo, cnu_avl, cnu_ph, cnu_retinoscopia, cnu_rx, cnu_bicromatica, cnu_reloj, bol_status)
	VALUES (:cod_datogeneralid_fk:, :des_ladoojo:, :cnu_avl:, :cnu_ph:, :cnu_retinoscopia:, :cnu_rx:, :cnu_bicromatica:, :cnu_reloj:, true)";

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,
		'des_ladoojo' => $json->des_ladoojo,
		'cnu_avl' => $json->cnu_avl,
		'cnu_ph' => $json->cnu_ph,
		'cnu_retinoscopia' => $json->cnu_retinoscopia,
		'cnu_rx' => $json->cnu_rx,
		'cnu_bicromatica' => $json->cnu_bicromatica,
		'cnu_reloj' => $json->cnu_reloj
	));

        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true) 
	{
		$response->setJsonContent(array('status' => 'OK', 'data' => $json));
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
	}

	return $response;
});