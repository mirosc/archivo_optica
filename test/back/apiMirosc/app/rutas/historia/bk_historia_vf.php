    <?php
    //todos los 
    $app->get('/api/historia/pacientes', function () use ($app) {

        $query = "SELECT pacientes.id_paciente, CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC
        FROM historia.Pacientes AS pacientes, historia.Datosgeneral AS datos 
        WHERE pacientes.id_paciente = datos.id_paciente
        GROUP BY 1
        ORDER BY 1";
        //$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
        $result = $app->modelsManager->executeQuery($query);

        $datos = array();
        foreach ($result as $fila) {
            $datos[] = array(
                'id_paciente'   => $fila->id_paciente,
                'nomC'   => $fila->nomC
                
            );
        }

        header('Access-Control-Allow-Origin: *'); 
        echo json_encode($datos);
    });

    $app->get('/api/historia/Hpaciente/{id}', function ($id) use ($app) {

        $query = "SELECT datos.email, datos.f_nac, datos.f_reg,
        CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC, datos.id_datogeneral
        FROM historia.Pacientes AS pacientes, historia.Datosgeneral AS datos 
        WHERE pacientes.id_paciente = datos.id_paciente 
        AND pacientes.id_paciente = :id:";
        $result = $app->modelsManager->executeQuery($query,array('id' => $id));
        
        $datos = array();
        foreach ($result as $fila) {
            $datos[] = array(

                'email'   => $fila->email,
                'f_nac' => $fila->f_nac,
                'f_reg' => $fila->f_reg,
                'nomC' => $fila->nomC,
                'id_datogeneral' => $fila->id_datogeneral
            );
        }
        header('Access-Control-Allow-Origin: *');   

    });

    // PDF
    $app->get('/historia/vf/{id}', function ($id) use ($app) {

        require("/var/www/html/proyectos/test/back/apiMirosc/app/librerias/pdf/fpdf.php");
        //require("../../app/librerias/pdf/fpdf.php");
        /*
        $query = "SELECT * FROM historia.AntMedico AS medico, historia.AntOculFam AS fam, historia.AntOcular AS ocular, historia.Antecedentes AS ant, historia.Datosgeneral AS datos
        WHERE datos.id_datogeneral = :id:";
    */
        $query = "SELECT CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC, CONCAT(datos.calle,' ',datos.numero, ' ', datos.letra) AS direccion FROM historia.Pacientes AS pacientes, historia.AntMedico AS medico, historia.AntOculFam AS fam, historia.AntOcular AS ocular, historia.Antecedentes AS ant, historia.Datosgeneral AS datos
        WHERE pacientes.id_paciente = datos.id_paciente AND datos.id_datogeneral = :id:";

        $result = $app->modelsManager->executeQuery($query,array('id' => $id));
        //    echo json_encode($result);

        class PDF extends FPDF{
        // Cabecera de página
            function Header(){    //HISTORIA CLINICA    
                global $title;
                $this->Image('../public/img/logo.png',10,10,50);
                //$this->Image('../public/img/logo.jpg',10,10,30);
                $this->SetFont('Arial','B',12);
                $w = $this->GetStringWidth($title)+6;
                $this->SetX((210-$w)/2);
                $this->Cell(0,6,'HISTORIA CLINICA',1,0,'C');
                $this->Ln(15);
            }

            function T(){       //Texto cuerpo
                $this->SetFont('Arial','',9);
                $this->SetTextColor(0,0,0);
            }

            function TB(){       //Texto cuerpo
                $this->SetFont('Courier','',9);
                $this->SetTextColor(0,0,0);
            }
           
            function T1(){       //Titulo 1
                $this->SetFont('Arial','B',11);
            }

            function T2(){       //Titulo 2
                $this->SetFont('Arial','B',11);
            }

             function T2txtR(){       //Titulo 2, texto rojo
                $this->SetFont('Arial','',10);
                $this->SetTextColor(255,0,0);
                $this->Ln(2);
            }

            function CCel(){        //color celdas
                $this->SetDrawColor(0,80,180);
            }

           function datosGrales(){
                $nombre= utf8_decode('Nombre de prueba en función');
                //global $nombre;       //no pasa la variable
                $this->T2txtR();
                $this->CCel();
                $this->Cell(45,4,'DATOS GENERALES:',0,0,'');
                $this->Cell(80,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,'Fecha:',0,0,'');
                $this->SetFont('Courier','BI',11);
                $this->Cell(41,4,'01-01-2019',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(0,1,'',1,1,'C');
                $this->T();
                $this->Cell(20,4,'Nombre:',0,0,'');
                $this->T2();
                $this->Cell(90,4,$nombre,0,0,'C');    //DATOS EN VARIABLE
                $this->T();
                $this->Cell(18,4,'Edad:',0,0,'');
                $this->Cell(12,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,'Fecha de Nacimiento:',0,0,'');
                $this->Cell(36,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(25,4,utf8_decode('Dirección:'),0,0,'');
                $this->Cell(115,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'Tel:',0,0,'');
                $this->Cell(46,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(25,4,'Celular:',0,0,'');
                $this->Cell(45,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(25,4,'E-mail:',0,0,'');
                $this->Cell(45,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(25,4,'Hobbies:',0,0,'');
                $this->Cell(101,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(40,4,utf8_decode('Ocupación'),0,0,'');
                $this->Cell(30,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(40,4,'Factores de Riesgo:',0,0,'');
                $this->Cell(30,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(40,4,utf8_decode('Lugar de trabajo:'),0,0,'');
                $this->Cell(16,4,'',0,1,'C');    //DATOS EN VARIABLE
            }

            function quejaPpal(){
                $this->T2txtR();
                $this->Cell(45,4,'QUEJA PRINCIPAL:',0,1,'');
                $this->Cell(0,1,'',1,1,'C');
                $this->T();
                $this->Cell(80,4,utf8_decode('¿Cual es la causa de su visita?'),0,0,'');
                $this->Cell(116,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode('¿Problema principal que presentan sus ojos?'),0,0,'');
                $this->Cell(116,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(45,4,utf8_decode('¿Desde cuando?'),0,0,'');
                $this->Cell(151,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Visión Lejana'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Cercana'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Visión Doble'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Ardor'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Comezón'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Secreciones'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Fatiga Ocular'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Epiforia'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Ojo Seco'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Ojo Enrojecido'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Fotofobia'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(50,4,utf8_decode('Moscas volantes (Miodesopsias)'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(40,4,utf8_decode('Flashes de Luz o Estrellitas'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(50,4,utf8_decode('Auras o Aros de Diferentes Colores'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(60,4,utf8_decode('Fecha de último examén visual:'),0,0,'');
                $this->Cell(46,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(45,4,utf8_decode('Graduación anterior'),0,1,'');
                $this->Cell(15,4,'OD:',0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(30,4,'',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(15,4,'OI:',0,0,'');
                $this->Cell(30,4,'',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(15,4,'ADD:',0,0,'');
                $this->Cell(30,4,'',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(110,4,utf8_decode('¿Tipo y materiales lente de contacto o anteojos?'),0,0,'');
                $this->Cell(86,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Eficacia'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Comodidad'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Necesidad'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,1,'C');
                $this->AVL(); 
                $this->ODIO();   
                
            }

            function anteOcularPX(){
                $this->T2txtR();
                $this->Cell(80,4,'ANTECEDENTES OCULARES DEL PX:',0,1,'');
                $this->Cell(0,1,'',1,1,'C');
                $this->Ln(3);
                $this->T();
                $this->Cell(46,4,'Golpes ocular o craneal:',0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'Si',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(5,4,'No',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Ubicación:'),0,0,'');
                $this->Cell(35,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(16,4,'TX:',0,0,'');
                $this->Cell(180,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(55,4,utf8_decode('Infecciones o cirugias oculares:'),0,0,'');
                $this->Cell(26,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Ubicación:'),0,0,'');
                $this->Cell(35,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(16,4,'TX:',0,0,'');
                $this->Cell(180,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(55,4,utf8_decode('Desviaciones oculares:'),0,0,'');
                $this->Cell(46,4,'',0,1,'C');    //DATOS EN VARIABLE
            }

            function anteOcularFam(){
                $this->T2txtR();
                $this->Cell(80,4,'ANTECEDENTES OCULARES DE FAMILIARES:',0,1,'');
                $this->Cell(0,1,'',1,1,'C');
                $this->Ln(3);  
                $this->T();
                $this->Cell(30,4,utf8_decode('Cataratas'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Glaucoma'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Cirugías'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Estrabismo'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Ametropías elevadas'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Queratocono'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE  
                $this->Cell(10,4,'',0,0,'C');   
                $this->Cell(30,4,utf8_decode('Ceguera'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE       
            }

            function anteMedicoPX(){
                $this->T2txtR();
                $this->Cell(80,4,utf8_decode('ANTECEDENTES MÉDICOS DEL PX:'),0,1,'');
                $this->Cell(0,1,'',1,1,'C');
                $this->Ln(3);
                $this->T();
                $this->Cell(45,4,utf8_decode('Fecha último examén médico:'),0,0,'');
                $this->Cell(151,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Salud general'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Diabetes'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Presión arterial'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Glaucoma'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Cirugías'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('TX:'),0,0,'');
                $this->Cell(5,4,'',0,1,'C');
                $this->Cell(30,4,utf8_decode('Traumatismo'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Dolor de cabeza'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Alergias'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('TX:'),0,0,'');
                $this->Cell(5,4,'',0,1,'C');
                $this->Cell(80,4,utf8_decode('¿Cuenta con todos los servicios medicos?'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'Si',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(5,4,'No',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Mencione, ¿cual?:'),0,0,'');
                $this->Cell(35,4,'',0,1,'C');    //DATOS EN VARIABLE
            }

            function anteMedicoFam(){
                $this->T2txtR();
                $this->Cell(80,4,utf8_decode('ANTECEDENTES MÉDICO FAMILIARES:'),0,1,'');
                $this->Cell(0,1,'',1,1,'C');  
                $this->T();
                $this->Cell(30,4,utf8_decode('Diabetes'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Presión arterial'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Glaucoma'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Cirugías'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Traumatismo'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Dolor de cabeza'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE       
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Alergias'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(30,4,utf8_decode('Salud en general'),0,0,'');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'X',1,1,'C');    //DATOS EN VARIABLE        
            }

            function obsGral(){
                $this->T2txtR();
                $this->Cell(80,4,utf8_decode('OBSERVACIÓN GENERAL:'),0,1,'');
                $this->Cell(0,1,'',1,1,'C');  
                $this->T();
                $this->Cell(30,4,utf8_decode('Anomalías físicas:'),0,0,'');
                $this->Cell(45,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(30,4,utf8_decode('Asimetrías faciales:'),0,0,'');
                $this->Cell(45,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(16,4,'TX:',0,0,'');
                $this->Cell(30,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(40,4,utf8_decode('Observaciones oculares:'),0,0,'');
                $this->Cell(110,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(16,4,'TX:',0,0,'');
                $this->Cell(30,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(56,4,utf8_decode('Comportamiento personal:'),0,0,'');
                $this->Cell(140,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(25,4,utf8_decode('Otro:'),0,0,'');
                $this->Cell(140,4,'',0,1,'C');    //DATOS EN VARIABLE   
                $this->T2txtR();
            }

            function ODIO(){
                //examen OD OI 
                $this->T2txtR();
                $this->Cell(80,4,utf8_decode('RX DEL PACIENTE:'),0,1,'');
                $this->Cell(0,1,'',1,1,'C');              
                $this->T();
                $this->Ln(3);
                $this->TB();
                $this->Cell(15,4,'',0,0,'');
                $this->Cell(22,4,'Esferico',0,0,'C');
                $this->Cell(22,4,'Cilindro',0,0,'C');
                $this->Cell(22,4,'Eje',0,0,'C');
                $this->Cell(22,4,'ADD',0,0,'C');
                $this->Cell(22,4,'DNP',0,0,'C');
                $this->Cell(22,4,'DIP',0,0,'C');
                $this->Cell(22,4,'Altura',0,0,'C');
                $this->Cell(22,4,'Prisma',0,1,'C');
                $this->Cell(15,4,'OD:',0,0,'');
                $this->T();
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,1,'C');
                $this->TB();
                $this->Cell(15,4,'OI:',0,0,'');
                $this->T();
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,1,'C');  
            }
             
            function AVL(){
                //examen OD OI  
                $this->T2txtR();
                $this->Cell(80,4,utf8_decode('DESARROLLO Y PROGRESO EN EL EXAMEN VISUAL:'),0,1,'');
                $this->Cell(0,1,'',1,1,'C');  
                $this->T();                            
                $this->Ln(3);
                $this->TB();
                $this->Cell(15,4,'',0,0,'');
                $this->Cell(22,4,'A.V.L.',0,0,'C');
                $this->Cell(22,4,'P.H.',0,0,'C');
                $this->Cell(63,4,'Retinoscopia',0,0,'C');
                $this->Cell(33,4,'Bicromatica',0,0,'C');
                $this->Cell(33,4,'Reloj Astigmatico',0,1,'C');
                $this->Cell(15,4,'OD:',0,0,'');
                $this->T();
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(30,4,'',1,0,'C');
                $this->Cell(33,4,'',1,0,'C');
                $this->Cell(33,4,'',1,0,'C');
                $this->Cell(33,4,'',1,1,'C');
                $this->TB();
                $this->Cell(15,4,'OI:',0,0,'');
                $this->T();
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(22,4,'',1,0,'C');
                $this->Cell(30,4,'',1,0,'C');
                $this->Cell(33,4,'',1,0,'C');
                $this->Cell(33,4,'',1,0,'C');
                $this->Cell(33,4,'',1,1,'C');
                $this->Ln(2);
 
            }       
            function Footer(){      // Pie de página
                $this->SetY(-20);
                $this->SetFont(Courier,'BI',8);
                $this->SetTextColor(0,0,0);
                $this->Cell(0,5,utf8_decode('Calle Lira y Ortega 6A    Tlaxcala, Tlaxcala  Col. Centro  Tel. 246 46 1 00 00     Cel. 241 123 12 12'),0,1,'C');
                $this->Cell(0,4,utf8_decode('Pág. ').$this->PageNo().' de {nb}',0,1,'C');
            }

            function historia(){
                $this->datosGrales();        //funcion
                $this->quejaPpal();
                $this->anteOcularPX(); 
                $this->anteMedicoPX();     
                $this->anteOcularFam();
                $this->anteMedicoFam();
                $this->obsGral();
            }
        }

        $pdf = new PDF('P', 'mm', 'letter');  //mio
        $bold='B';
        $italic='I';
        $A='Arial';
        $C='Courier';
        $T1=9;  //tamaño de fuente
        $T2=11;
        $m=0;   //margen para pruebas de las celdas
        $mf=1;  //margen fijo, algunas celdas
        $sl=1;  //salto de linea predeterminado
        $ac=4;  //alto celdas, default = 5
        //letter: 216 mm - (10*2) = 196
        $pdf->T1();
        $title = 'HISTORIA CLINICA';
        $pdf->SetTitle($title);
        $nombre =utf8_decode('" Maria Magdalena Popocatl Ródriguez "');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->historia();
        $pdf->Output('Historia clinica.pdf','I');
    });
