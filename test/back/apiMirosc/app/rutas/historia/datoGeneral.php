<?php

$app->post('/api/historia/general/new', function() use ($app){
	$paciente = 0;
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtpacientes(
	des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (:des_nombre:, :des_paterno:, :des_materno:, :cnu_pais:, :cnu_celular:, :cnu_telcasa:, true, true, true)";

	$queryDato = "INSERT INTO sght.Tsghtdatosgeneral (cod_pacienteid_fk, des_calle, cnu_numexterior, des_numinterior, email, txt_hobbies, des_ocupacion, txt_factores, fec_nacimiento, fec_registro, bol_status, des_colonia, cnu_cp, des_municipio, des_estado) VALUES (:cod_pacienteid_fk:, :des_calle:, :cnu_numexterior:, :des_numinterior:, :email:, :txt_hobbies:, :des_ocupacion:, :txt_factores:, :fec_nacimiento:, now(), true, :des_colonia:, :cnu_cp:, :des_municipio:, :des_estado:)";

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'des_nombre' => $json->nombre,
		'des_paterno' => $json->paterno,
		'des_materno' => $json->materno,
		'cnu_celular' => $json->celular,
		'cnu_telcasa' => $json->casa,
		'cnu_pais' => '+521'

	));

	if($json->cod_paciente > 0){
		$paciente = $json->cod_paciente;
	}else{
		$paciente = $resultPaciente->getModel()->cod_pacienteid;
	}

	if (empty($json->nuInt)) {
		$json->nuInt = null;
	}

	$resultDato = $app->modelsManager->executeQuery($queryDato, array(        
		'cod_pacienteid_fk' => $paciente,
		'des_calle' => $json->calle,
		'cnu_numexterior' => $json->nuExt,
		'des_numinterior' => $json->nuInt,
		'cnu_cp' => $json->cp,
		'txt_hobbies' => $json->pasatiempo,
		'fec_nacimiento' => $json->nacimiento,
		'des_colonia' =>  $json->colonia,
		'des_municipio' =>  $json->municipio,
		'des_estado' =>  $json->estado,
		'email' =>  $json->email,
		'des_ocupacion' =>  $json->ocupacion,
		'txt_factores' =>  $json->facRies

	));
        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true || $paciente > 0) 
	{
		if($json->email != null){
			if ($resultDato->success() == true) 
			{
				$response->setJsonContent(
					array(
						'status' => 'OK', 
						'dataD' => $resultDato->getModel()->cod_datogeneralid, 
						'dataP' => $resultPaciente->getModel()->cod_pacienteid)
				);
			}
			else
			{
				$errors = array();
				foreach ($resultDato->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}

				$response->setJsonContent(array('status' => 'ERRORD', 'messages' => $errors));	
			}	
		}else{
			$response->setJsonContent(array('status' => 'ERRORD MAIL' ));
		}
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERRORP', 'messages' => $errors));
	}

	return $response;
});

$app->post('/api/historia/general/newProspecto', function() use ($app){
	
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtpacientes(
	des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (:des_nombre:, :des_paterno:, :des_materno:, :cnu_pais:, :cnu_celular:, :cnu_telcasa:, true, true, true)";

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'des_nombre' => $json->nombre,
		'des_paterno' => $json->paterno,
		'des_materno' => $json->materno,
		'cnu_celular' => $json->celular,
		'cnu_telcasa' => $json->casa,
		'cnu_pais' => '+521'

	));

	//comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true || $paciente > 0) 
	{
		$response->setJsonContent(
			array(
				'status' => 'OK', 
				'dataP' => $resultPaciente->getModel()->cod_pacienteid)
		);
		
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERRORP', 'messages' => $errors));
	}

	return $response;
});