<?php

$app->post('/api/historia/antocufam/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();
	$fk = 1;
        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtantoculfam(
		cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, bol_status, cod_txid_fk)
	VALUES (:cod_datogeneralid_fk:, :bol_cataratas:, :bol_glaucoma:, :bol_cirugias:, :bol_estrabismo:, :bol_ametropias:, :bol_queratocono:, :bol_ceguera:, true, :fk:)";


	$queryTX = "INSERT INTO sght.Tsghtx(
	txt_tx1, txt_tx2, txt_tx3, 
	txt_tx4, txt_tx5, txt_tx6, txt_tx7)
	VALUES (:txt1:, :txt2:, :txt3:, 
	:txt4:, :txt5:, :txt6:, :txt7:)";

	if (empty($json->txt1)){
		$json->txt1 = null;
	}
	if (empty($json->txt2)){
		$json->txt2 = null;
	}
	if (empty($json->txt3)){
		$json->txt3 = null;
	}
	if (empty($json->txt4)){
		$json->txt4 = null;
	}
	if (empty($json->txt5)){
		$json->txt5 = null;
	}
	if (empty($json->txt6)){
		$json->txt6 = null;
	}
	if (empty($json->txt7)){
		$json->txt7 = null;
	}

	$resultTX = $app->modelsManager->executeQuery($queryTX, array(
		'txt1' => $json->txt1,
		'txt2' => $json->txt2,
		'txt3' => $json->txt3,
		'txt4' => $json->txt4,
		'txt5' => $json->txt5,
		'txt6' => $json->txt6,
		'txt7' => $json->txt7
	));

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,
		'bol_cataratas' => $json->bol_cataratas,
		'bol_glaucoma' => $json->bol_glaucoma,
		'bol_cirugias' => $json->bol_cirugias,
		'bol_estrabismo' => $json->bol_estrabismo,
		'bol_ametropias' => $json->bol_ametropias,
		'bol_queratocono' => $json->bol_queratocono,
		'bol_ceguera' => $json->bol_ceguera,
		'fk' => $fk
	));


	if ($json->txt1 != null || $json->txt2 != null ||
		$json->txt3 != null || $json->txt4 != null ||
		$json->txt5 != null || $json->txt6 != null ||
		$json->txt7 != null) {
		if ($resultTX->success() == true) 
		{
			$fk = $resultTX->getModel()->cod_txid;
			$response->setJsonContent(array('status' => 'OK', 'data' => $json));
		} 
		else 
		{
	            //en otro caso cambiamos el estado http por un 500
	            //$response->setStatusCode(500, "Internal Error");

	            //enviamos los errores
			$errors = array();
			foreach ($resultTX->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
		}
	}else{

	

        //comprobamos si el insert se ha llevado a cabo
		if ($resultPaciente->success() == true) 
		{
			$response->setJsonContent(array('status' => 'OK', 
				'data' => $json,
				'data1' => $resultTX));
		} 
		else 
		{
	            //en otro caso cambiamos el estado http por un 500
	            //$response->setStatusCode(500, "Internal Error");

	            //enviamos los errores
			$errors = array();
			foreach ($resultPaciente->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
		}
	}

	return $response;
});