<?php
//todos los 
$app->get('/api/historia/pacientes', function () use ($app) {

	$query = "SELECT pacientes.cod_pacienteid, CONCAT(pacientes.des_nombre, ' ',pacientes.des_paterno, ' ', pacientes.des_materno) AS nomC
	FROM sght.Tsghtpacientes AS pacientes, sght.Tsghtdatosgeneral AS datos 
	WHERE pacientes.cod_pacienteid = datos.cod_pacienteid_fk
	GROUP BY 1
	ORDER BY 1";
	//$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
	$result = $app->modelsManager->executeQuery($query);

	$datos = array();
	foreach ($result as $fila) {
		$datos[] = array(
			'cod_pacienteid'   => $fila->cod_pacienteid,
			'nomC'   => $fila->nomC
			
		);
	}

	header('Access-Control-Allow-Origin: *'); 
	echo json_encode($datos);
});

$app->get('/api/historia/Hpaciente/{id}', function ($id) use ($app) {

	$query = "SELECT 
	datos.email, 
	datos.fec_nacimiento, 
	datos.fec_registro,
	CONCAT(pacientes.des_nombre, ' ',pacientes.des_paterno, ' ', pacientes.des_materno) AS nomC, datos.cod_datogeneralid
	FROM 
	sght.Tsghtpacientes AS pacientes, 
	sght.Tsghtdatosgeneral AS datos 
	WHERE pacientes.cod_pacienteid = datos.cod_pacienteid_fk 
	AND pacientes.cod_pacienteid = :id: 
	ORDER BY 3 DESC";
	$result = $app->modelsManager->executeQuery($query,array('id' => $id));
	
	$datos = array();
	foreach ($result as $fila) {
		$datos[] = array(
			'email'   => $fila->email,
			'fec_nacimiento' => $fila->fec_nacimiento,
			'f_reg' => $fila->fec_registro,
			'f_nac' => ((new DateTime())->diff(new DateTime($fila->fec_nacimiento)))->y,
			'nomC' => $fila->nomC,
			'cod_datogeneralid' => $fila->cod_datogeneralid
		);
	}
	
	header('Access-Control-Allow-Origin: *');   
	echo json_encode($datos);
});

// ticket
$app->get('/historia/ticket/{id}', function ($id) use ($app) {

	require("/var/www/html/proyectos/test/back/apiMirosc/app/librerias/fpdf181/fpdf.php");

	$pdf = new FPDF($orientation='P',$unit='mm', array(45,350));
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',8);    //Letra Arial, negrita (Bold), tam. 20
	$textypos = 5;
	$pdf->setY(2);
	$pdf->setX(2);
	$pdf->Cell(5,$textypos,"NOMBRE DE LA EMPRESA");
	$pdf->SetFont('Arial','',5);    //Letra Arial, negrita (Bold), tam. 20
	$textypos+=6;
	$pdf->setX(2);
	$pdf->Cell(5,$textypos,'-------------------------------------------------------------------');
	$textypos+=6;
	$pdf->setX(2);
	$pdf->Cell(5,$textypos,'CANT.  ARTICULO       PRECIO               TOTAL');
	$total =0;
	$off = $textypos+6;
	$producto = array(
		"q"=>1,
		"name"=>"Computadora Lenovo i5",
		"price"=>100
	);
	$productos = array($producto, $producto, $producto, $producto, $producto );
	foreach($productos as $pro){
		$pdf->setX(2);
		$pdf->Cell(5,$off,$pro["q"]);
		$pdf->setX(6);
		$pdf->Cell(35,$off,  strtoupper(substr($pro["name"], 0,12)) );
		$pdf->setX(20);
		$pdf->Cell(11,$off,  "$".number_format($pro["price"],2,".",",") ,0,0,"R");
		$pdf->setX(32);
		$pdf->Cell(11,$off,  "$ ".number_format($pro["q"]*$pro["price"],2,".",",") ,0,0,"R");
		$total += $pro["q"]*$pro["price"];
		$off+=6;
	}
	$textypos=$off+6;
	$pdf->setX(2);
	$pdf->Cell(5,$textypos,"TOTAL: " );
	$pdf->setX(38);
	$pdf->Cell(5,$textypos,"$ ".number_format($total,2,".",","),0,0,"R");
	$pdf->setX(2);
	$pdf->Cell(5,$textypos+6,'GRACIAS POR TU COMPRA ');
	$pdf->output();


});

// PDF
$app->get('/historia/pdf/{id}', function ($id) use ($app) {

	//require("/var/www/html/proyectos/test/back/apiMirosc/app/librerias/pdf/fpdf.php");
	//require("/var/www/html/proyectos/test/back/apiMirosc/app/librerias/fpdf181/ean13.php");
	//require("/var/www/html/proyectos/test/back/apiMirosc/app/librerias/fpdf181/code128.php");
   // require("/var/www/html/proyectos/test/back/apiMirosc/app/rutas/num2letra.php");
	//require("../../app/librerias/pdf/fpdf.php");
   // $query = "SELECT CONCAT(pacientes.des_nombre, ' ',pacientes.des_paterno, ' ', pacientes.des_materno) AS nomC, CONCAT(datos.calle,' ',datos.numero, ' ', datos.letra) AS direccion FROM sght.Tsghtpacientes AS pacientes, sght.AntMedico AS medico, sght.AntOculFam AS fam, sght.AntOcular AS ocular, sght.Antecedentes AS ant, sght.Tsghtdatosgeneral AS datos WHERE pacientes.cod_pacienteid = datos.cod_pacienteid_fk AND datos.cod_datogeneralid = :id:";

  /*  $query = "SELECT * FROM 
	sght.Tsghtpacientes AS pacientes, 
	sght.AntMedico AS medico, 
	sght.AntOculFam AS fam, 
	sght.AntOcular AS ocular, 
	sght.Antecedentes AS ant, 
	sght.Tsghtdatosgeneral AS datos,
	sght.Graduacion AS gra, 
	sght.ObsGrales AS obG,
	sght.Queja AS que
	WHERE 
	pacientes.cod_pacienteid = datos.cod_pacienteid_fk
	AND datos.cod_datogeneralid = :id:";*/

	$query = "SELECT * 
FROM 
	sght.Tsghtpacientes pacientes,  
	sght.Tsghtdatosgeneral datosG,
	sght.Tsghtqueja queja,
	
	sght.Tsghtantmedicofam mef,
	sght.Tsghtantmedicopx mep,
	sght.Tsghtantocularpx ocpx,
	sght.Tsghtantoculfam ocf,
	sght.Tsghtobservaciongen og
WHERE 
	pacientes.cod_pacienteid = datosG.cod_pacienteid_fk
	AND datosG.cod_datogeneralid = queja.cod_datogeneralid_fk
	
	AND datosG.cod_datogeneralid = mef.cod_datogeneralid_fk
	AND datosG.cod_datogeneralid = mep.cod_datogeneralid_fk
	AND datosG.cod_datogeneralid = ocpx.cod_datogeneralid_fk
	AND datosG.cod_datogeneralid = ocf.cod_datogeneralid_fk
	AND datosG.cod_datogeneralid = og.cod_datogeneralid_fk
	AND datosG.cod_datogeneralid = :id:";

	$result = $app->modelsManager->executeQuery($query,array('id' => $id));


	header('Access-Control-Allow-Origin: *');   
	//echo json_encode($result[0]->pacientes->des_nombre);
	echo json_encode($result);
/*
	class PDF extends FPDF{
		// Cabecera de página
		function Header(){        
			global $title;
			//$this->Image('../public/img/logo.png',10,10,50);
			$this->SetFont('Arial','B',14);
		// Calculamos ancho y posición del título.
			$w = $this->GetStringWidth($title)+6;
			$this->SetX((210-$w)/2);
			$this->Cell(0,6,'HISTORIA CLINICA',1,0,'C');

			$this->Ln(20);
		}

		// Pie de página
		function Footer(){
			$this->SetY(-20);
			$this->SetFont(Courier,'I',8);
			$this->Cell(0,5,utf8_decode('Calle Lira y Ortega 6A    Tlaxcala, Tlaxcala  Col. Centro  Tel. 246 46 1 00 00     Cel. 241 123 12 12'),0,1,'');
			$this->Cell(0,4,utf8_decode('Pág. ').$this->PageNo().' de {nb}',0,1,'C');
		}
	}

	$des_nombrePX .= $result[0]->tsghtpacientes->des_nombre.' '.$result[0]->tsghtpacientes->des_paterno.' '.$result[0]->tsghtpacientes->des_materno;

	$des_direccionPX .= $result[0]->tsghtdatosgeneral->des_calle.' '.$result[0]->tsghtdatosgeneral->cnu_numexterior.' '.$result[0]->tsghtdatosgeneral->des_numinterior.', '.$result[0]->tsghtdatosgeneral->des_municipio.', '.$result[0]->tsghtdatosgeneral->des_estado;
	
	// Creación del objeto de la clase heredada
	//$pdf = new PDF('P','mm','Letter');
	//$pdf = new PDF_EAN13('P','mm','Letter');
	$pdf = new PDF_Code128('P','mm','Letter');
	
	$bold='B';
	$italic='I';
	$A='Arial';
	$C='Courier';
	$T1=9;  //tamaño de fuente
	$T2=11;
	$m=0;   //margen para pruebas de las celdas
	$mf=1;  //margen fijo, algunas celdas
	$sl=1;  //salto de linea predeterminado
	$ac=4;  //alto celdas, default = 5
	//letter: 216 mm - (10*2) = 196
	$pdf->SetFont($A,$bold,$T1);
	$title = 'HISTORIA CLINICA';
	$pdf->SetTitle($title);
	$nombre =utf8_decode($des_nombrePX);  //nombre de prueba, para la longitud de la celda, y dato recuperado de la BD       DATOS EN VARIABLE
	$pdf->AliasNbPages();
	$pdf->AddPage();
	//$pdf->Cell(196,8,$title,0,1,'C');
	$pdf->SetFont($A,$bold,$T2);       
	$pdf->SetDrawColor(0,80,180);
	$pdf->SetTextColor(220,50,50);

////////////////////////////////////////////////////
	$pdf->Cell(45,$ac,'DATOS GENERALES:',$m,0,'');
	$pdf->Cell(80,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,'Fecha:',$m,0,'');
	$pdf->SetFont($C,'BI',$T2);
	$pdf->Cell(41,$ac,$result[0]->tsghtdatosgeneral->fec_registro,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(0,1,'',1,$sl,'C');
	$pdf->Ln(3);
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(20,$ac,'Nombre:',$m,0,'');
	$pdf->SetFont($A,$bold,$T2);
	$pdf->Cell(90,$ac,$nombre,$m,0,'C');    //DATOS EN VARIABLE
	$pdf->SetFont($A,'',$T1);
	$pdf->Cell(18,$ac,'Edad:',$m,0,'');
	$pdf->Cell(12,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(20,$ac,'Hobbies:',$m,0,'');
	$pdf->Cell(36,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(25,$ac,utf8_decode('Dirección:'),$m,0,'');
	$pdf->Cell(115,$ac,$des_direccionPX,$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'Tel:',$m,0,'');
	$pdf->Cell(46,$ac,$result[0]->tsghtpacientes->cnu_telcasa,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(25,$ac,'Celular:',$m,0,'');
	$pdf->Cell(45,$ac,$result[0]->tsghtpacientes->cnu_celular,$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(25,$ac,'E-mail:',$m,0,'');
	$pdf->Cell(101,$ac,$result[0]->tsghtdatosgeneral->email,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(40,$ac,utf8_decode('Ocupación'),$m,0,'');
	$pdf->Cell(40,$ac,$result[0]->tsghtdatosgeneral->des_ocupacion,$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(40,$ac,'Factores de Riesgo:',$m,0,'');
	$pdf->Cell(76,$ac,$result[0]->tsghtdatosgeneral->txt_factores,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Ln(5);
	$pdf->SetFont($A,$bold,$T2);       
	$pdf->SetTextColor(220,50,50);


	$pdf->Cell(45,$ac,'QUEJA PRINCIPAL:',$m,$sl,'');
	$pdf->Cell(0,1,'',1,$sl,'C');
	$pdf->Ln(3);
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(80,$ac,utf8_decode('¿Cual es la causa de su visita?'),$m,0,'');
	$pdf->Cell(116,$ac,$result[0]->tsghtqueja->txt_causa,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(80,$ac,utf8_decode('¿Problema principal que presentan sus ojos?'),$m,0,'');
	$pdf->Cell(116,$ac,$result[0]->tsghtqueja->txt_problema,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(45,$ac,utf8_decode('¿Desde cuando?'),$m,0,'');
	$pdf->Cell(151,$ac,$result[0]->tsghtqueja->des_cuando,$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Ln(5);
	$pdf->Cell(30,$ac,utf8_decode('Visión Lejana'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	if($result[0]->tsghtqueja->bol_vlejana){
		$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	}else{
		$pdf->Cell(5,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
	}
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Cercana'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_vcercana,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Visión Doble'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_vdoble,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Ardor'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_ardor,$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Comezón'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_comezon,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Secreciones'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_secrecion,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Fatiga Ocular'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_fatiga,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Epiforia'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_epiforia,$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Ojo Seco'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_ojoseco,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Ojo Enrojecido'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_ojorojo,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Fotofobia'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_fotofobia,$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Ln(5);
	$pdf->Cell(196,$ac,utf8_decode('¿Ha experimentado alguno de los siguientes sintomas?'),$m,$sl,'');
	$pdf->Ln(5);
	$pdf->Cell(80,$ac,utf8_decode('Moscas (Mioplesapsias)'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_moscas,$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(60,$ac,utf8_decode('Flashes de Luz o Estrellitas'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_flashes,$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(80,$ac,utf8_decode('Auras o Aros de Diferentes Colores'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,$result[0]->tsghtqueja->bol_auras,$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Ln(5);
	$pdf->Cell(16,$ac,'Otros:',$m,0,'');
	$pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(60,$ac,utf8_decode('Fecha de último examén visual:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(45,$ac,utf8_decode('Graduación anterior'),$m,$sl,'');
	$pdf->Cell(15,$ac,'OD:',$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(15,$ac,'OI:',$m,0,'');
	$pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(15,$ac,'ADD:',$m,0,'');
	$pdf->Cell(30,$ac,'',$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Ln(5);
	$pdf->Cell(110,$ac,utf8_decode('¿Tipo y materiales lente de contacto o anteojos?'),$m,0,'');
	$pdf->Cell(86,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Eficacia'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Comodidad'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Necesidad'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Retinoscopia'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Bicromatica'),$m,$sl,'');
	$pdf->Cell(15,$ac,'OD:',$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(15,$ac,'OI:',$m,0,'');
	$pdf->Cell(30,$ac,'',$mf,1,'C');    //DATOS EN VARIABLE      
	$pdf->Ln(5);
	$pdf->SetFont($A,$bold,$T2);       
	$pdf->SetTextColor(220,50,50);


	$pdf->Cell(80,$ac,'ANTECEDENTES OCULARES DEL PX:',$m,$sl,'');
	$pdf->Cell(0,1,'',1,$sl,'C');
	$pdf->Ln(3);
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(55,$ac,'Golpes ocular o craneal:',$m,0,'');
	$pdf->Cell(46,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Ubicación:'),$m,0,'');
	$pdf->Cell(65,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(16,$ac,'TX:',$m,0,'');
	$pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(55,$ac,utf8_decode('Infecciones o cirugias oculares:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(16,$ac,'TX:',$m,0,'');
	$pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(55,$ac,utf8_decode('Desviaciones oculares:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Reloj astigmatico'),$m,$sl,'');
	$pdf->Cell(15,$ac,'OD:',$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(15,$ac,'OI:',$m,0,'');
	$pdf->Cell(30,$ac,'',$mf,1,'C');    //DATOS EN VARIABLE  
	$pdf->SetFont($A,$bold,$T2);       
	$pdf->SetTextColor(220,50,50);
	$pdf->Ln(5);


	$pdf->Cell(80,$ac,utf8_decode('ANTECEDENTES MÉDICOS DEL PX:'),$m,$sl,'');
	$pdf->Cell(0,1,'',1,$sl,'C');
	$pdf->Ln(3);        
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(45,$ac,utf8_decode('Fecha último examén médico:'),$m,0,'');
	$pdf->Cell(151,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Ln(5);
	$pdf->Cell(30,$ac,utf8_decode('Salud general'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Diabetes'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Presión arterial'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Glaucoma'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Cirugías'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('TX:'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,$sl,'C');
	$pdf->Cell(30,$ac,utf8_decode('Traumatismo'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Dolor de cabeza'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Alergias'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('TX:'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,$sl,'C');
	$pdf->Cell(80,$ac,utf8_decode('¿Cuenta con todos los servicios?'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,$sl,'C');    //DATOS EN VARIABLE


////////////////////////////////////////////////////
	$pdf->AddPage();    //2da pagina
	$pdf->SetFont($A,$bold,$T2);    //color y fuente header
	$pdf->SetTextColor(0,0,0);
////////////////////////////////////////////////////        
	$pdf->SetTextColor(250,50,50);


	$pdf->Cell(80,$ac,'ANTECEDENTES OCULARES DE FAMILIARES:',$m,$sl,'');
	$pdf->Cell(0,1,'',1,$sl,'C');
	$pdf->Ln(3);        
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(30,$ac,utf8_decode('Cataratas'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Glaucoma'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Cirugías'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Estrabismo'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Ametropías elevadas'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Queratocono'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,$sl,'C');    //DATOS EN VARIABLE
	$pdf->SetFont($A,$bold,$T2);          
	$pdf->SetTextColor(220,50,50);
	$pdf->Ln(5);


	$pdf->Cell(80,$ac,utf8_decode('ANTECEDENTES MÉDICO FAMILIARES:'),$m,$sl,'');
	$pdf->Cell(0,1,'',1,$sl,'C');  
	$pdf->Ln(5);     
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(30,$ac,utf8_decode('Diabetes'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Presión arterial'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Glaucoma'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Cirugías'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Traumatismo'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Dolor de cabeza'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE       
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Alergias'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(10,$ac,'',$m,0,'C');
	$pdf->Cell(30,$ac,utf8_decode('Salud en general'),$m,0,'');
	$pdf->Cell(5,$ac,'',$m,0,'C');
	$pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
	$pdf->SetFont($A,$bold,$T2);          
	$pdf->SetTextColor(220,50,50);
	$pdf->Ln(5);


	$pdf->Cell(80,$ac,utf8_decode('OBSERVACIÓN GENERAL:'),$m,$sl,'');
	$pdf->Cell(0,1,'',1,$sl,'C');
	$pdf->Ln(3);        
	$pdf->SetFont($A,'',$T1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(55,$ac,utf8_decode('Anomalías físicas:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
	$pdf->Cell(30,$ac,utf8_decode('Asimetrías faciales:'),$m,0,'');
	$pdf->Cell(65,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(16,$ac,'TX:',$m,0,'');
	$pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(55,$ac,utf8_decode('Observaciones oculares:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(16,$ac,'TX:',$m,0,'');
	$pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(55,$ac,utf8_decode('Comportamiento personal:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->Cell(55,$ac,utf8_decode('Otro:'),$m,0,'');
	$pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
	$pdf->SetFont($A,$bold,$T2);       
	$pdf->SetTextColor(220,50,50);
	$pdf->Ln(5);
	
	//$pdf->AddPage();
	//$pdf->EAN13(80,40,'123456789012');

	$code='CODE 128x';
	$pdf->Code128(50,100,$code,80,20);
	//$pdf->SetXY(50,45);
	//$pdf->Write(5,'A set: "'.$code.'"');
	
	/*$pdf->AddPage();
	$variable = codigoAleatorio;

	$img = HgetImage($code_base64);
	try {
		if ($img !== false)  $pdf->cell(100,5, $pdf->Image($img[0], 85,200,50,50, $img[1]),'C');
		
	} catch (Exception $e) {
		$pdf->cell(100,5, '','C');
	}*/
	
	//$pdf->Image('http://chart.apis.google.com/chart?cht=qr&chs=230x230&chl=http%3A%2F%2F127.0.0. 1%2Fproyecto%2Fphp%2Flog.php%3Fbusca%'.$variable.'%26submit%3Dbuscar&ecc=L&matrix=4',95,170,25,25,'P NG'); 

	//$pdf->Output();
	/*$des_nombredoc .= $result[0]->pacientes->des_nombre.$result[0]->pacientes->des_paterno.'.pdf';
	//$pdf->Output($des_nombredoc,'D');
	//$pdf->Output('cnsjc.pdf','D');
	$pdf->Output();*/
});

function HgetImage($dataURI){
	$img = explode(',',$dataURI,2);
	$pic = 'data://text/plain;base64,'.$img[1];
	$type = explode("/", explode(':', substr($dataURI, 0, strpos($dataURI, ';')))[1])[1]; // get the image type
	if ($type=="png"||$type=="jpeg"||$type=="gif") return array($pic, $type);
	return false;
}

