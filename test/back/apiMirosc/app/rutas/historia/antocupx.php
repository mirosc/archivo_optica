<?php

$app->post('/api/historia/antocupx/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtantocularpx(
		cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, bol_infecciones, txt_ubicacion2, txt_tx2, bol_desviacion_o, txt_tx3, bol_status)
	VALUES (:cod_datogeneralid_fk:, :bol_golpes:, :txt_ubicacion1:, :txt_tx1:, :bol_infecciones:, :txt_ubicacion2:, :txt_tx2:, :bol_desviacion_o:, :txt_tx3:, true)";

	if (empty($json->bol_golpes)){
		$json->bol_golpes = false;
	}
	if (empty($json->txt_ubicacion1)){
		$json->txt_ubicacion1 = '';
	}
	if (empty($json->txt_tx1)){
		$json->txt_tx1 = '';
	}
	if (empty($json->bol_infecciones)){
		$json->bol_infecciones = false;
	}
	if (empty($json->txt_ubicacion2)){
		$json->txt_ubicacion2 = '';
	}
	if (empty($json->txt_tx2)){
		$json->txt_tx2 = '';
	}
	if (empty($json->bol_desviacion_o)){
		$json->bol_desviacion_o = false;
	}
	if (empty($json->txt_tx3)){
		$json->txt_tx3 = '';
	}

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,

		'bol_golpes' => $json->bol_golpes,
		'txt_ubicacion1' => $json->txt_ubicacion1,
		'txt_tx1' => $json->txt_tx1,

		'bol_infecciones' => $json->bol_infecciones,
		'txt_ubicacion2' => $json->txt_ubicacion2,
		'txt_tx2' => $json->txt_tx2,

		'bol_desviacion_o' => $json->bol_desviacion_o,
		'txt_tx3' => $json->txt_tx3
	));

        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true) 
	{
		$response->setJsonContent(array('status' => 'OK', 'data' => $json));
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
	}

	return $response;
});