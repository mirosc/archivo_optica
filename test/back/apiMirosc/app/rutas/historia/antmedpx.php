<?php

$app->post('/api/historia/antmedpx/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtantmedicopx(
	cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status)
	VALUES (:cod_datogeneralid_fk:, :des_fecultexammed:, :bol_salud:, :bol_diabetes:, :bol_preionarter:, :bol_glaucoma:, :bol_cirugias:, :txt_tx1:, :bol_traumatismo:, :bol_dolorcabeza:, :bol_alergias:, :txt_tx2:, :bol_sermedicos:, :txt_cuales:, true)";

    if (empty($json->des_fecultexammed)){
        $json->des_fecultexammed = null;
    }
    if (empty($json->txt_tx1)){
        $json->txt_tx1 = null;
    }
    if (empty($json->txt_tx2)){
        $json->txt_tx2 = null;
    }
    if (empty($json->txt_cuales)){
        $json->txt_cuales = null;
    }
    if (empty($json->bol_salud)){
        $json->bol_salud = false;
    }
    if (empty($json->bol_diabetes)){
        $json->bol_diabetes = false;
    }
    if (empty($json->bol_preionarter)){
        $json->bol_preionarter = false;
    }
    if (empty($json->bol_glaucoma)){
        $json->bol_glaucoma = false;
    }
    if (empty($json->bol_cirugias)){
        $json->bol_cirugias = false;
    }
    if (empty($json->bol_traumatismo)){
        $json->bol_traumatismo = false;
    }
    if (empty($json->bol_dolorcabeza)){
        $json->bol_dolorcabeza = false;
    }
    if (empty($json->bol_alergias)){
        $json->bol_alergias = false;
    }
    if (empty($json->bol_sermedicos)){
        $json->bol_sermedicos = false;
    }

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,
		'des_fecultexammed' => $json->des_fecultexammed,
		'bol_salud' => $json->bol_salud,
		'bol_diabetes' => $json->bol_diabetes,
		'bol_preionarter' => $json->bol_preionarter,
		'bol_glaucoma' => $json->bol_glaucoma,
		'bol_cirugias' => $json->bol_cirugias,
		'txt_tx1' => $json->txt_tx1,
		'bol_traumatismo' => $json->bol_traumatismo,
		'bol_dolorcabeza' => $json->bol_dolorcabeza,
		'bol_alergias' => $json->bol_alergias,
		'txt_tx2' => $json->txt_tx2,
		'bol_sermedicos' => $json->bol_sermedicos,
		'txt_cuales' => $json->txt_cuales

	));

        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true) 
	{
		$response->setJsonContent(array('status' => 'OK', 'data' => $json));
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
	}

	return $response;
});