<?php

$app->post('/api/historia/queja/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtqueja(
	cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional)
	VALUES (:cod_datogeneralid_fk:, :txt_causa:, :txt_problema:, :des_cuando:, :bol_vlejana:, :bol_vcercana:, :bol_vdoble:, :bol_ardor:, :bol_comezon:, :bol_secrecion:, :bol_fatiga:, :bol_epiforia:, :bol_ojoseco:, :bol_ojorojo:, :bol_fotofobia:, :bol_moscas:, :bol_flashes:, :bol_auras:, :opcional:)";

    if (empty($json->causa)) {
        $json->causa = '';
    }
    if (empty($json->bol_vlejana)) {
        $json->bol_vlejana = false;
    }
    if (empty($json->bol_vcercana)) {
        $json->bol_vcercana = false;
    }
    if (empty($json->bol_vdoble)) {
        $json->bol_vdoble = false;
    }
    if (empty($json->bol_ardor)) {
        $json->bol_ardor = false;
    }
    if (empty($json->bol_comezon)) {
        $json->bol_comezon = false;
    }
    if (empty($json->bol_secrecion)) {
        $json->bol_secrecion = false;
    }
    if (empty($json->bol_fatiga)) {
        $json->bol_fatiga = false;
    }
    if (empty($json->bol_epiforia)) {
        $json->bol_epiforia = false;
    }
    if (empty($json->bol_ojoseco)) {
        $json->bol_ojoseco = false;
    }
    if (empty($json->bol_ojorojo)) {
        $json->bol_ojorojo = false;
    }
    if (empty($json->bol_moscas)) {
        $json->bol_moscas = false;
    }
    if (empty($json->bol_flashes)) {
        $json->bol_flashes = false;
    }
    if (empty($json->bol_auras)) {
        $json->bol_auras = false;
    }
    if (empty($json->bol_fotofobia)) {
        $json->bol_fotofobia = false;
    }
    if (empty($json->otro)) {
        $json->otro = '';
    }

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,
		'txt_causa' => $json->causa,
		'txt_problema' => $json->problema,
		'des_cuando' => $json->cuando,
		'bol_vlejana' => $json->bol_vlejana,
		'bol_vcercana' => $json->bol_vcercana,
		'bol_vdoble' => $json->bol_vdoble,
		'bol_ardor' => $json->bol_ardor,
		'bol_comezon' => $json->bol_comezon,
		'bol_secrecion' => $json->bol_secrecion,
		'bol_fatiga' => $json->bol_fatiga,
		'bol_epiforia' => $json->bol_epiforia,
		'bol_ojoseco' => $json->bol_ojoseco,
		'bol_ojorojo' => $json->bol_ojorojo,
		'bol_moscas' => $json->bol_moscas,
		'bol_flashes' => $json->bol_flashes,
		'bol_auras' => $json->bol_auras,
		'bol_fotofobia' => $json->bol_fotofobia,
        'opcional' => $json->otro

	));

        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true) 
	{
		$response->setJsonContent(array('status' => 'OK', 'data' => $json));
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERROR', 
                                        'messages' => $errors));
	}

	return $response;
});