<?php

$app->post('/api/historia/obsgen/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtobservaciongen(
	cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, bol_status)
	VALUES (:cod_datogeneralid_fk:, :txt_anomalias:, :txt_asimetrias:, :txt_tx1:, :txt_obsocular:, :txt_tx2:, :txt_comportamiento:, :txt_otro:, true)";

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,
		'txt_anomalias' => $json->txt_anomalias,
		'txt_asimetrias' => $json->txt_asimetrias,
		'txt_tx1' => $json->txt_tx1,
		'txt_obsocular' => $json->txt_obsocular,
		'txt_tx2' => $json->txt_tx2,
		'txt_comportamiento' => $json->txt_comportamiento,
		'txt_otro' => $json->txt_otro
	));

        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true) 
	{
		$response->setJsonContent(array('status' => 'OK', 'data' => $json));
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
	}

	return $response;
});