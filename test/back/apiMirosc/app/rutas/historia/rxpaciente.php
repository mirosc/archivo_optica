<?php

$app->post('/api/historia/rxpaciente/new', function() use ($app){
    //obtenemos el json que se ha enviado 
	$json = $app->request->getJsonRawBody();
	header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
	$response = new Phalcon\Http\Response();

        //creamos la consulta con query
	$queryPaciente = "INSERT INTO sght.Tsghtrxpaciente(
	cod_datogeneralid_fk, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, bol_status)
	VALUES (:cod_datogeneralid_fk:, :des_ladoojo:, :cnu_esferico:, :cnu_cilindro:, :cnu_eje:, :cnu_add:, :cnu_dnp:, :cnu_dip:, :cnu_altura:, :cnu_prisma:, true)";

	$resultPaciente = $app->modelsManager->executeQuery($queryPaciente, array(        
		'cod_datogeneralid_fk' => $json->cod_dato,
		'des_ladoojo' => $json->des_ladoojo,
		'cnu_esferico' => $json->cnu_esferico,
		'cnu_cilindro' => $json->cnu_cilindro,
		'cnu_eje' => $json->cnu_eje,
		'cnu_add' => $json->cnu_add,
		'cnu_dnp' => $json->cnu_dnp,
		'cnu_dip' => $json->cnu_dip,
		'cnu_altura' => $json->cnu_altura,
		'cnu_prisma' => $json->cnu_prisma
	));

        //comprobamos si el insert se ha llevado a cabo
	if ($resultPaciente->success() == true) 
	{
		$response->setJsonContent(array('status' => 'OK', 'data' => $json));
	} 
	else 
	{
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
		$errors = array();
		foreach ($resultPaciente->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
	}

	return $response;
});