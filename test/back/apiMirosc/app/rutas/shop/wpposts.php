<?php
//todos los 
$app->get('/api/shop/wpposts', function () use ($app) {

    $query = "SELECT * FROM WpPosts";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'post_author'   => $fila->post_author,
            'post_date'   => $fila->post_date
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});