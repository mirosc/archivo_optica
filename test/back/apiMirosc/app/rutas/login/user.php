<?php
//todos los 
$app->get('/api/login/user', function () use ($app) {

    $query = "SELECT * FROM login.User";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_user'   => $fila->id_user,
            'user'   => $fila->user
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});


$app->post('/api/login/searchUser', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT user
                FROM login.User
                WHERE id_user = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'iduser'   => $info->id,
            'user'   => $fila->user
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/login/user/new', function() use ($app){
    //obtenemos el json que se ha enviado 
    $json = $app->request->getJsonRawBody();
    header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query
    $query = "INSERT INTO login.User 
        (nombre, apellido, email, pass, token, status, id_tipousuario)
        VALUES 
        (':nombre:', ':apellido:',':email:',
        crypt(':pass:', gen_salt('bf')),
        'null',TRUE, :id_tipousuario:)";

    $result = $app->modelsManager->executeQuery($query, array(        
        'nombre' => $json->nombre,
        'apellido' => $json->apellido,
        'email' => $json->email,
        'pass' => $json->pass,
        'id_tipousuario' => $json->id_tipousuario
    ));

        //comprobamos si el insert se ha llevado a cabo
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
        $errors = array();
        foreach ($result->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }

    return $response;
});

/*
curl -i -X PUT -d '{"descripcion":"ASIMO"}' http://93.188.163.213/Test/apiMirosc/api/login/user/update/10
*/
$app->put('/api/login/user/update/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE login.User AS user SET user.user = :user: WHERE user.id_user = :id:";
    //$query = "UPDATE login.UserAS M SET M.user = :user: WHERE M.id_user = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'user' => $json->user
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});

$app->put('/api/login/user/delete/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE login.User AS user SET user.status = :status: WHERE user.id_user = :id:";
    //$query = "UPDATE login.UserAS M SET M.descripcion = :descripcion: WHERE M.id_user = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'false'
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $id));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});
//////////////////////////////////////////////////////////////////////////////////////////77


//Sistema de Login
$app->post('/api/user/login', function () use ($app) {
    header('Access-Control-Allow-Origin: *');
    //obtenemos el json que se ha enviado
    $info = $app->request->getJsonRawBody();
    //$pass = sha1($info->password); //codificadomos el password
    $pass = $info->password; //codificadomos el password
    $token = sha1($info->email.time());

    //Desclaramos el arreglo de datos
    $datos = array();

    // CONTROL DE ERRORES
    $error = 0;
    $errors = array();
    $messages = null;

    //configuracion inicial
    $idUser = null;

    $queryPass = "SELECT encode(sha256(':pass:'),'hex') x, tipousuario FROM login.TipoUsuario";

    $queryLogin = "SELECT encode(sha256(':pass:'),'hex') nombre, encode(sha256('r351d3nc145'),'hex') apellido, user.email, user.id_tipousuario, roll.tipousuario
                    FROM login.User AS user LEFT JOIN login.TipoUsuario AS roll ON user.id_tipousuario = roll.id_tipousuario
                    WHERE email = :email: AND pass = :pass:";

    $querySession = "SELECT * FROM UserLogin WHERE idUser = :idUser: AND idDevices = :idDevices:";

    $insertSession = "INSERT INTO UserLogin (idUser, idDevices, idPlataform,token,browser,startLogin) 
    VALUES (:idUser:, :idDevices:, :idPlataform:, :token:, :browser:, now())";

    $updateSession = "UPDATE UserLogin 
    SET token = :token:, browser = :browser:, idPlataform = :idPlataform:, startLogin = now(), finishLogin = NULL 
    WHERE idUser = :idUser: AND idDevices = :idDevices:";

    $resultPass = $app->modelsManager->executeQuery($queryPass,array('pass' => $pass));

    $resultLogin = $app->modelsManager->executeQuery($queryLogin,array('email' => $info->email,'pass' => $pass));

    // Usuario de tipo 4 Custom no se podra autentica $resultLogin[0]->idRoll != 4
    if ($resultLogin->count() == 1) {
        //$resultSession = $app->modelsManager->executeQuery($querySession,array('idUser' => $resultLogin[0]->idUser,'idDevices' => $idDevices));

        /*if ($resultLogin[0]->idPlant != NULL)
            $initConf = true;
        else
            $initConf = false;*/
            $datos[] = array(
                'testpass'   => $resultPass[0]->x,
                'idRoll' => $resultLogin[0]->id_tipousuario,
                'firstName' => $resultLogin[0]->nombre,
                'lastName'   => $resultLogin[0]->apellido,
                'email' => $resultLogin[0]->email,
                'nameRoll' => $resultLogin[0]->tipousuario,
                "token" => $token
        );
/*
        if ($resultSession->count() == 0) {

            $addSession = $app->modelsManager->executeQuery($insertSession,array('idUser' => $resultLogin[0]->idUser, 'token' => $token, 'idDevices' => $idDevices, 'idPlataform' => $plataform['idPlataform'], 'browser' => $browser));
            if ($addSession->success() == false) {
                $error++;
                $messages = 'Exist problem when session start';
            }
        } elseif ($resultSession->count() == 1 && $resultSession[0]->token ==  NULL) {
            $addSession = $app->modelsManager->executeQuery($updateSession,array('idUser' => $resultSession[0]->idUser, 'token' => $token, 'idDevices' => $idDevices, 'idPlataform' => $plataform['idPlataform'], 'browser' => $browser));
            if ($addSession->success() == false) {
                $error++;
                $messages = 'Exist problem when session start';
            }
        } else {
            if ($info->idUser != null || $info->idUser != '') {
                $addSession = $app->modelsManager->executeQuery($updateSession,array('idUser' => $info->idUser, 'token' => $token, 'idDevices' => $idDevices, 'idPlataform' => $plataform['idPlataform'], 'browser' => $browser));
                if ($addSession->success() == false) {
                    $error++;
                    $messages = 'Exist problem when session start';
                }
            } else {
                $idUser = $resultLogin[0]->idUser;
                $error++;
                $messages = 'there is another session on another device, close the corresponding session';
            }
        }
        */
    } else {
        $error++;
        $messages = 'User or Password are incorrects.';
    }

    //creamos una respuesta
    $response = new Phalcon\Http\Response();
    //comprobamos si el insert se ha llevado a cabo
    if ($error == 0) {
        $response->setJsonContent(array('status' => 'OK', 'data' => $datos));
    } else {
        if ($idUser == null) $response->setJsonContent(array('status' => 'ERROR', 'messages' => $messages));
        else $response->setJsonContent(array('status' => 'ERROR', 'messages' => $messages, 'idUser' => $idUser));
    }
   
    return $response;
});