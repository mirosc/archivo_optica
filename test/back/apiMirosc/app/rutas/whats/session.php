<?php
//todos los 
/*
$app->get('/api/login/user', function () use ($app) {

    $query = "SELECT * FROM login.User";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_user'   => $fila->id_user,
            'user'   => $fila->user
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});


$app->post('/api/login/searchUser', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT user
                FROM login.User
                WHERE id_user = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'iduser'   => $info->id,
            'user'   => $fila->user
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});
*/
$app->post('/api/login/session/new', function() use ($app){
    //obtenemos el json que se ha enviado 
    $json = $app->request->getJsonRawBody();
    header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query
    $query = "INSERT INTO login.Session 
        (id_user, token)
        VALUES 
        (':id_user:', ':token:'";

    $result = $app->modelsManager->executeQuery($query, array(        
        'id_user' => $json->id_user,
        'token' => $json->token
    ));

        //comprobamos si el insert se ha llevado a cabo
    if ($result->success() == true) {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
        $queryToken = "UPDATE login.User AS user SET user.token = :token: WHERE user.id_user = :id:";

        $resultToken = $app->modelsManager->executeQuery($queryToken, array(        
            'id_user' => $json->id_user,
            'token' => $json->token
        ));
        if ($result->success() == true) {
            $response->setJsonContent(array('status' => 'OK', 'data' => $json));
        }
        
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
        $errors = array();
        foreach ($result->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }

    return $response;
});

/*
curl -i -X PUT -d '{"descripcion":"ASIMO"}' http://93.188.163.213/Test/apiMirosc/api/login/session/update/10
*/
$app->put('/api/login/session/update/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE login.Session AS session SET session.session = :session: WHERE session.id_session = :id:";
    //$query = "UPDATE login.SessionAS M SET M.session = :session: WHERE M.id_session = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'session' => $json->session
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});
/*
$app->put('/api/login/session/delete/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE login.User AS user SET user.status = :status: WHERE user.id_user = :id:";
    //$query = "UPDATE login.UserAS M SET M.descripcion = :descripcion: WHERE M.id_user = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'false'
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $id));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});