<?php
//todos los 
$app->get('/api/whats/mensajes', function () use ($app) {

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT id_mensaje, descripcion, mensaje
    FROM whats.Mensajes
    WHERE status = true 
    ORDER BY 1";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_mensaje'   => $fila->id_mensaje,
            'descripcion'   => $fila->descripcion,
            'mensaje'   => $fila->mensaje
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/whats/searchMensaje', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT descripcion, mensaje
    FROM whats.Mensajes
    WHERE id_mensaje = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_mensaje'   => $info->id,
            'descripcion'   => $fila->descripcion,
            'mensaje'   => $fila->mensaje
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/whats/searchMensajeURL', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT CONCAT('.%20', REPLACE(mensaje, ' ', '%20')) AS texto
    FROM whats.Mensajes
    WHERE id_mensaje = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'texto'   => $fila->texto
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/whats/mensajes/new', function() use ($app){
    //obtenemos el json que se ha enviado 
    $json = $app->request->getJsonRawBody();
    header('Access-Control-Allow-Origin: *'); 

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query
    $query = "INSERT INTO whats.Mensajes (descripcion, status, mensaje) 
    VALUES (:descripcion:, :status:, :mensaje:)";

    $result = $app->modelsManager->executeQuery($query, array(        
        'descripcion' => $json->descripcion,
        'mensaje' => $json->mensaje,
        'status' => 'true'
    ));

        //comprobamos si el insert se ha llevado a cabo
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

            //enviamos los errores
        $errors = array();
        foreach ($result->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }

    return $response;
});

/*
curl -i -X PUT -d '{"descripcion":"ASIMO"}' http://93.188.163.213/Test/apiMirosc/api/whats/mensajes/update/10
*/
$app->put('/api/whats/mensajes/update/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE whats.Mensajes as mensajes SET mensajes.descripcion = :descripcion:, mensajes.mensaje = :mensaje:, mensajes.status = :status: WHERE mensajes.id_mensaje = :id:";
    //$query = "UPDATE whats.Mensajes AS M SET M.descripcion = :descripcion: WHERE M.id_mensaje = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'true',
        'descripcion' => $json->descripcion,
        'mensaje' => $json->mensaje
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $json));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});

$app->put('/api/whats/mensajes/delete/{id:[0-9]+}', function($id) use($app) 
{
    //obtenemos el json que se ha enviado 
    header('Access-Control-Allow-Origin: *'); 
    $json = $app->request->getJsonRawBody();

    //creamos una respuesta
    $response = new Phalcon\Http\Response();

        //creamos la consulta con query

    $query = "UPDATE whats.Mensajes as mensajes SET mensajes.status = :status: WHERE mensajes.id_mensaje = :id:";
    //$query = "UPDATE whats.Mensajes AS M SET M.descripcion = :descripcion: WHERE M.id_mensaje = :id:";
    $result = $app->modelsManager->executeQuery($query, array(
        'id' => $id,
        'status' => 'false'
    ));

        //comprobamos si la actualización se ha llevado a cabo correctamente
    if ($result->success() == true) 
    {
        $response->setJsonContent(array('status' => 'OK', 'data' => $id));
    } 
    else 
    {
            //en otro caso cambiamos el estado http por un 500
            //$response->setStatusCode(500, "Internal Error");

        $errors = array();
        foreach ($result->getMessages() as $message) 
        {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
    }
    
    return $response;
});
