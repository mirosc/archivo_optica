<?php
//todos los 
$app->get('/api/whats/contactos', function () use ($app) {

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT pacientes.cod_pacienteid, pacientes.des_nombre, pacientes.des_paterno, pacientes.des_materno, pacientes.cnu_celular
                FROM sght.Tsghtpacientes AS pacientes
                WHERE pacientes.bol_whats = true 
                AND pacientes.bol_activo = true 
                AND pacientes.des_nombre <> '.'
                ORDER BY 1";
    $result = $app->modelsManager->executeQuery($query);

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'id_paciente'   => $fila->cod_pacienteid,
            'nombre'   => $fila->des_nombre,
            'paterno'   => $fila->des_paterno,
            'materno'   => $fila->des_materno,
            'celular'   => $fila->cnu_celular
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/whats/searchCliente', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT CONCAT('https://api.whatsapp.com/send?phone=', cod_pais, celular, '&text=Hola,%20',
                REPLACE(REPLACE(CONCAT(nombre, '%20', paterno, '%20', materno), ' ', '%20'), '.', '')) AS url
                FROM historia.Pacientes
                WHERE id_paciente = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'url'   => $fila->url
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});

$app->post('/api/whats/searchClienteTest', function () use ($app) {

    header('Access-Control-Allow-Origin: *'); 
    //obtenemos el json que se ha enviado 
    $info = $app->request->getJsonRawBody();

    //$query = "SELECT * FROM historia.Pacientes";
    $query = "SELECT CONCAT('https://api.whatsapp.com/send?phone=', cod_pais, celular, '&text=Hola,%20',
                REPLACE(REPLACE(CONCAT(nombre, '%20', paterno, '%20', materno), ' ', '%20'), '.', '')) AS url
                FROM historia.Pacientes
                WHERE id_paciente = :id:";
    $result = $app->modelsManager->executeQuery($query,array('id' => $info->id));

    $datos = array();
    foreach ($result as $fila) {
        $datos[] = array(
            'url'   => $fila->url
        );
    }

    header('Access-Control-Allow-Origin: *'); 
    echo json_encode($datos);
});
