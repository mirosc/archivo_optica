    <?php
    //todos los 
    $app->get('/api/historia/pacientes', function () use ($app) {

        $query = "SELECT pacientes.id_paciente, CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC
        FROM historia.Pacientes AS pacientes, historia.Datosgeneral AS datos 
        WHERE pacientes.id_paciente = datos.id_paciente
        GROUP BY 1
        ORDER BY 1";
        //$query = "SELECT marca.id_marca, marca.marca, COUNT(producto.id_marcas) AS cantidad FROM inventario.Marcas AS marca, inventario.Producto AS producto GROUP BY 1,2";
        $result = $app->modelsManager->executeQuery($query);

        $datos = array();
        foreach ($result as $fila) {
            $datos[] = array(
                'id_paciente'   => $fila->id_paciente,
                'nomC'   => $fila->nomC
                
            );
        }

        header('Access-Control-Allow-Origin: *'); 
        echo json_encode($datos);
    });

    $app->get('/api/historia/Hpaciente/{id}', function ($id) use ($app) {

        $query = "SELECT datos.email, datos.f_nac, datos.f_reg,
        CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC, datos.id_datogeneral
        FROM historia.Pacientes AS pacientes, historia.Datosgeneral AS datos 
        WHERE pacientes.id_paciente = datos.id_paciente 
        AND pacientes.id_paciente = :id:";
        $result = $app->modelsManager->executeQuery($query,array('id' => $id));
        
        $datos = array();
        foreach ($result as $fila) {
            $datos[] = array(

                'email'   => $fila->email,
                'f_nac' => $fila->f_nac,
                'f_reg' => $fila->f_reg,
                'nomC' => $fila->nomC,
                'id_datogeneral' => $fila->id_datogeneral
            );
        }
        header('Access-Control-Allow-Origin: *');   

    });

    // PDF
    $app->get('/historia/v1/{id}', function ($id) use ($app) {

        require("/var/www/html/proyectos/test/back/apiMirosc/app/librerias/pdf/fpdf.php");
        //require("../../app/librerias/pdf/fpdf.php");
        /*
        $query = "SELECT * FROM historia.AntMedico AS medico, historia.AntOculFam AS fam, historia.AntOcular AS ocular, historia.Antecedentes AS ant, historia.Datosgeneral AS datos
        WHERE datos.id_datogeneral = :id:";
    */
        $query = "SELECT CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC, CONCAT(datos.calle,' ',datos.numero, ' ', datos.letra) AS direccion FROM historia.Pacientes AS pacientes, historia.AntMedico AS medico, historia.AntOculFam AS fam, historia.AntOcular AS ocular, historia.Antecedentes AS ant, historia.Datosgeneral AS datos
        WHERE pacientes.id_paciente = datos.id_paciente AND datos.id_datogeneral = :id:";

        //$result = $app->modelsManager->executeQuery($query,array('id' => $id));
        //    echo json_encode($result);

        class PDF extends FPDF{
        // Cabecera de página
            function Header(){        
                global $title;
                $this->Image('../public/img/logo.png',10,10,50);
                $this->SetFont('Arial','B',14);
        // Calculamos ancho y posición del título.
                $w = $this->GetStringWidth($title)+6;
                $this->SetX((210-$w)/2);
                $this->Cell(0,6,'HISTORIA CLINICA',1,0,'C');
                $this->Ln(20);
            }

        // Pie de página
            function Footer(){
                $this->SetY(-20);
                $this->SetFont(Courier,'I',8);
                $this->Cell(0,5,utf8_decode('Calle Lira y Ortega 6A    Tlaxcala, Tlaxcala  Col. Centro  Tel. 246 46 1 00 00     Cel. 241 123 12 12'),0,1,'');
                $this->Cell(0,4,utf8_decode('Pág. ').$this->PageNo().' de {nb}',0,1,'C');
            }
        }

        // Creación del objeto de la clase heredada
        //  http://localhost/proyectos/test/back/apiMirosc/historia/pdf/1#page=1&zoom=90
        $pdf = new PDF('P','mm','Letter');
        $bold='B';
        $italic='I';
        $A='Arial';
        $C='Courier';
        $T1=9;  //tamaño de fuente
        $T2=11;
        $m=0;   //margen para pruebas de las celdas
        $mf=1;  //margen fijo, algunas celdas
        $sl=1;  //salto de linea predeterminado
        $ac=4;  //alto celdas, default = 5
        //letter: 216 mm - (10*2) = 196
        $pdf->SetFont($A,$bold,$T1);
        $title = 'HISTORIA CLINICA';
        $pdf->SetTitle($title);
        $nombre =utf8_decode('" Maria Magdalena Popocatl Ródriguez "');  //nombre de prueba, para la longitud de la celda, y dato recuperado de la BD       DATOS EN VARIABLE
        $pdf->AliasNbPages();
        $pdf->AddPage();
        //$pdf->Cell(196,8,$title,0,1,'C');
        $pdf->SetFont($A,$bold,$T2);       
        $pdf->SetDrawColor(0,80,180);
        $pdf->SetTextColor(220,50,50);

////////////////////////////////////////////////////
        $pdf->Cell(45,$ac,'DATOS GENERALES:',$m,0,'');
        $pdf->Cell(80,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,'Fecha:',$m,0,'');
        $pdf->SetFont($C,'BI',$T2);
        $pdf->Cell(41,$ac,'01-01-2019',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(0,1,'',1,$sl,'C');
        $pdf->Ln(3);
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(20,$ac,'Nombre:',$m,0,'');
        $pdf->SetFont($A,$bold,$T2);
        $pdf->Cell(90,$ac,$nombre,$m,0,'C');    //DATOS EN VARIABLE
        $pdf->SetFont($A,'',$T1);
        $pdf->Cell(18,$ac,'Edad:',$m,0,'');
        $pdf->Cell(12,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(20,$ac,'Hobbies:',$m,0,'');
        $pdf->Cell(36,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(25,$ac,utf8_decode('Dirección:'),$m,0,'');
        $pdf->Cell(115,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'Tel:',$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(25,$ac,'Celular:',$m,0,'');
        $pdf->Cell(45,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(25,$ac,'E-mail:',$m,0,'');
        $pdf->Cell(101,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(40,$ac,utf8_decode('Ocupación'),$m,0,'');
        $pdf->Cell(40,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(40,$ac,'Factores de Riesgo:',$m,0,'');
        $pdf->Cell(76,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Ln(5);
        $pdf->SetFont($A,$bold,$T2);       
        $pdf->SetTextColor(220,50,50);


        $pdf->Cell(45,$ac,'QUEJA PRINCIPAL:',$m,$sl,'');
        $pdf->Cell(0,1,'',1,$sl,'C');
        $pdf->Ln(3);
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(80,$ac,utf8_decode('¿Cual es la causa de su visita?'),$m,0,'');
        $pdf->Cell(116,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(80,$ac,utf8_decode('¿Problema principal que presentan sus ojos?'),$m,0,'');
        $pdf->Cell(116,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(45,$ac,utf8_decode('¿Desde cuando?'),$m,0,'');
        $pdf->Cell(151,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Ln(5);
        $pdf->Cell(30,$ac,utf8_decode('Visión Lejana'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Cercana'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Visión Doble'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Ardor'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Comezón'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Secreciones'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Fatiga Ocular'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Epiforia'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Ojo Seco'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Ojo Enrojecido'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Fotofobia'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Ln(5);
        $pdf->Cell(196,$ac,utf8_decode('¿Ha experimentado alguno de los siguientes sintomas?'),$m,$sl,'');
        $pdf->Ln(5);
        $pdf->Cell(80,$ac,utf8_decode('Moscas (Mioplesapsias)'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(60,$ac,utf8_decode('Flashes de Luz o Estrellitas'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(80,$ac,utf8_decode('Auras o Aros de Diferentes Colores'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Ln(5);
        $pdf->Cell(16,$ac,'Otros:',$m,0,'');
        $pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(60,$ac,utf8_decode('Fecha de último examén visual:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(45,$ac,utf8_decode('Graduación anterior'),$m,$sl,'');
        $pdf->Cell(15,$ac,'OD:',$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(15,$ac,'OI:',$m,0,'');
        $pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(15,$ac,'ADD:',$m,0,'');
        $pdf->Cell(30,$ac,'',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Ln(5);
        $pdf->Cell(110,$ac,utf8_decode('¿Tipo y materiales lente de contacto o anteojos?'),$m,0,'');
        $pdf->Cell(86,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Eficacia'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Comodidad'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Necesidad'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Retinoscopia'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Bicromatica'),$m,$sl,'');
        $pdf->Cell(15,$ac,'OD:',$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(15,$ac,'OI:',$m,0,'');
        $pdf->Cell(30,$ac,'',$mf,1,'C');    //DATOS EN VARIABLE      
        $pdf->Ln(5);
        $pdf->SetFont($A,$bold,$T2);       
        $pdf->SetTextColor(220,50,50);


        $pdf->Cell(80,$ac,'ANTECEDENTES OCULARES DEL PX:',$m,$sl,'');
        $pdf->Cell(0,1,'',1,$sl,'C');
        $pdf->Ln(3);
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(55,$ac,'Golpes ocular o craneal:',$m,0,'');
        $pdf->Cell(46,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Ubicación:'),$m,0,'');
        $pdf->Cell(65,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(16,$ac,'TX:',$m,0,'');
        $pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(55,$ac,utf8_decode('Infecciones o cirugias oculares:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(16,$ac,'TX:',$m,0,'');
        $pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(55,$ac,utf8_decode('Desviaciones oculares:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Reloj astigmatico'),$m,$sl,'');
        $pdf->Cell(15,$ac,'OD:',$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,'',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(15,$ac,'OI:',$m,0,'');
        $pdf->Cell(30,$ac,'',$mf,1,'C');    //DATOS EN VARIABLE  
        $pdf->SetFont($A,$bold,$T2);       
        $pdf->SetTextColor(220,50,50);
        $pdf->Ln(5);


        $pdf->Cell(80,$ac,utf8_decode('ANTECEDENTES MÉDICOS DEL PX:'),$m,$sl,'');
        $pdf->Cell(0,1,'',1,$sl,'C');
        $pdf->Ln(3);        
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(45,$ac,utf8_decode('Fecha último examén médico:'),$m,0,'');
        $pdf->Cell(151,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Ln(5);
        $pdf->Cell(30,$ac,utf8_decode('Salud general'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Diabetes'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Presión arterial'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Glaucoma'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Cirugías'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('TX:'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,$sl,'C');
        $pdf->Cell(30,$ac,utf8_decode('Traumatismo'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Dolor de cabeza'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Alergias'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('TX:'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,$sl,'C');
        $pdf->Cell(80,$ac,utf8_decode('¿Cuenta con todos los servicios?'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,$sl,'C');    //DATOS EN VARIABLE


////////////////////////////////////////////////////
        $pdf->AddPage();    //2da pagina
        $pdf->SetFont($A,$bold,$T2);    //color y fuente header
        $pdf->SetTextColor(0,0,0);
////////////////////////////////////////////////////        
        $pdf->SetTextColor(250,50,50);


        $pdf->Cell(80,$ac,'ANTECEDENTES OCULARES DE FAMILIARES:',$m,$sl,'');
        $pdf->Cell(0,1,'',1,$sl,'C');
        $pdf->Ln(3);        
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(30,$ac,utf8_decode('Cataratas'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Glaucoma'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Cirugías'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Estrabismo'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Ametropías elevadas'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Queratocono'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,$sl,'C');    //DATOS EN VARIABLE
        $pdf->SetFont($A,$bold,$T2);          
        $pdf->SetTextColor(220,50,50);
        $pdf->Ln(5);


        $pdf->Cell(80,$ac,utf8_decode('ANTECEDENTES MÉDICO FAMILIARES:'),$m,$sl,'');
        $pdf->Cell(0,1,'',1,$sl,'C');  
        $pdf->Ln(5);     
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(30,$ac,utf8_decode('Diabetes'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Presión arterial'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Glaucoma'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Cirugías'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Traumatismo'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Dolor de cabeza'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE       
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Alergias'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(10,$ac,'',$m,0,'C');
        $pdf->Cell(30,$ac,utf8_decode('Salud en general'),$m,0,'');
        $pdf->Cell(5,$ac,'',$m,0,'C');
        $pdf->Cell(5,$ac,'X',$mf,1,'C');    //DATOS EN VARIABLE
        $pdf->SetFont($A,$bold,$T2);          
        $pdf->SetTextColor(220,50,50);
        $pdf->Ln(5);


        $pdf->Cell(80,$ac,utf8_decode('OBSERVACIÓN GENERAL:'),$m,$sl,'');
        $pdf->Cell(0,1,'',1,$sl,'C');
        $pdf->Ln(3);        
        $pdf->SetFont($A,'',$T1);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(55,$ac,utf8_decode('Anomalías físicas:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,0,'C');    //DATOS EN VARIABLE
        $pdf->Cell(30,$ac,utf8_decode('Asimetrías faciales:'),$m,0,'');
        $pdf->Cell(65,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(16,$ac,'TX:',$m,0,'');
        $pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(55,$ac,utf8_decode('Observaciones oculares:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(16,$ac,'TX:',$m,0,'');
        $pdf->Cell(180,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(55,$ac,utf8_decode('Comportamiento personal:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->Cell(55,$ac,utf8_decode('Otro:'),$m,0,'');
        $pdf->Cell(46,$ac,'',$m,$sl,'C');    //DATOS EN VARIABLE
        $pdf->SetFont($A,$bold,$T2);       
        $pdf->SetTextColor(220,50,50);
        $pdf->Ln(5);



        $pdf->Output();
    });
