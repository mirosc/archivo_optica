    <?php
    // PDF
    $app->get('/historia/recibo1/{id}', function ($id) use ($app) {

        require("../../apiMirosc/app/librerias/pdf/fpdf.php");
        
        /*
        $query = "SELECT * FROM historia.AntMedico AS medico, historia.AntOculFam AS fam, historia.AntOcular AS ocular, historia.Antecedentes AS ant, historia.Datosgeneral AS datos
        WHERE datos.id_datogeneral = :id:";
    */
        $query = "SELECT CONCAT(pacientes.nombre, ' ',pacientes.paterno, ' ', pacientes.materno) AS nomC, CONCAT(datos.calle,' ',datos.numero, ' ', datos.letra) AS direccion FROM historia.Pacientes AS pacientes, historia.AntMedico AS medico, historia.AntOculFam AS fam, historia.AntOcular AS ocular, historia.Antecedentes AS ant, historia.Datosgeneral AS datos
        WHERE pacientes.id_paciente = datos.id_paciente AND datos.id_datogeneral = :id:";

        $result = $app->modelsManager->executeQuery($query,array('id' => $id));
        //    echo json_encode($result);

        class PDF extends FPDF{
        // Cabecera de página
            function Header(){    //RECIBO  
                global $title; 
                $this->encIzq();
                $this->encDer();
                $this->Ln(3);
                $this->linea();
            }

            function encDer(){
                $this->Image('../public/img/logo.png',140,10,50);
                $this->T2txtR();
                $this->T1();
                $this->SetXY(203,10);
                $this->Cell(15,4,'Folio:',0,0,'');
                $this->T();
                $this->Cell(25,4,'',0,1,'C');   //DATOS EN VARIABLE
                $this->SetXY(200,15);
                $this->Cell(45,4,utf8_decode('ÓSCAR LÓPEZ CHAVEZ'),0,1,'C');
                $this->SetXY(200,20);
                $this->Cell(46,4,'R.F.C. LOCO7603147G8',0,1,'C');
                $this->SetXY(146,28);
                $this->Cell(25,4,'246 46 2 65 61',0,0,'C');
                $this->Image('../public/img/phone.png',142,28,5);
                $this->Cell(8,4,'',0,0,'');
                $this->Image('../public/img/whatsapp.png',174,28,5);
                $this->Cell(25,4,'241 119 55 18',0,0,'C');
                $this->Cell(8,4,'',0,0,'');
                $this->Image('../public/img/mail.png',205,26,8);
                $this->Cell(46,4,'opticamirosc76@hotmail.com',0,1,'C');
                $this->Image('../public/img/oj01.png',140,40,35);
                $this->Image('../public/img/oj02.png',170,43,35);
                $this->Image('../public/img/oj03.png',200,44,35);
                $this->Image('../public/img/oj04.png',230,44,35);
            }

            function encIzq(){
                $this->Image('../public/img/logo.png',10,10,50);
                $this->T2txtR();
                $this->T1();
                $this->SetXY(73,10);
                $this->Cell(15,4,'Folio:',0,0,'');
                $this->T();
                $this->Cell(25,4,$Folio,0,1,'C');   //DATOS EN VARIABLE
                $this->SetXY(70,15);
                $this->Cell(45,4,utf8_decode('ÓSCAR LÓPEZ CHAVEZ'),0,1,'C');
                $this->SetXY(70,20);
                $this->Cell(46,4,'R.F.C. LOCO7603147G8',0,1,'C');
                $this->SetXY(16,28);
                $this->Cell(25,4,'246 46 2 65 61',0,0,'C');
                $this->Image('../public/img/phone.png',12,28,5);
                $this->Cell(8,4,'',0,0,'');
                $this->Image('../public/img/whatsapp.png',44,28,5);
                $this->Cell(25,4,'241 119 55 18',0,0,'C');
                $this->Cell(8,4,'',0,0,'');
                $this->Image('../public/img/mail.png',75,26,8);
                $this->Cell(46,4,'opticamirosc76@hotmail.com',0,1,'C');
                $this->Image('../public/img/oj01.png',10,40,35);
                $this->Image('../public/img/oj02.png',40,43,35);
                $this->Image('../public/img/oj03.png',70,44,35);
                $this->Image('../public/img/oj04.png',100,44,35);
            }

            function T(){       //Texto cuerpo
                $this->SetFont('Arial','',9);
                $this->SetTextColor(0,0,0);
            }

            function TB(){       //Texto cuerpo
                $this->SetFont('Courier','B',7);
                $this->SetTextColor(0,0,0);
            }

            function T1(){       //Titulo 1
                $this->SetFont('Arial','B',11);
            }

            function T2(){       //Titulo 2
                $this->SetFont('Arial','B',11);
            }

            function T2txtR(){       //Titulo 2, texto rojo
                $this->SetFont('Arial','',10);
                $this->SetTextColor(255,0,0);
                $this->Ln(2);
            }

            function CCel(){        //color celdas
                $this->SetDrawColor(0,80,180);
            }

            function ODIO_Rec(){            
                $this->T();
                $this->TB();
                //IZQ
                $this->Cell(8,4,'',0,0,'');
                $this->Cell(14,4,'Esferico',0,0,'C');
                $this->Cell(14,4,'Cilindro',0,0,'C');
                $this->Cell(14,4,'Eje',0,0,'C');
                $this->Cell(14,4,'ADD',0,0,'C');
                $this->Cell(14,4,'DNP',0,0,'C');
                $this->Cell(14,4,'DIP',0,0,'C');
                $this->Cell(14,4,'Altura',0,0,'C');
                $this->Cell(14,4,'Prisma',0,0,'C');
                $this->Cell(15,4,'',0,0,'C');
                //DER
                $this->Cell(8,4,'',0,0,'');
                $this->Cell(14,4,'Esferico',0,0,'C');
                $this->Cell(14,4,'Cilindro',0,0,'C');
                $this->Cell(14,4,'Eje',0,0,'C');
                $this->Cell(14,4,'ADD',0,0,'C');
                $this->Cell(14,4,'DNP',0,0,'C');
                $this->Cell(14,4,'DIP',0,0,'C');
                $this->Cell(14,4,'Altura',0,0,'C');
                $this->Cell(14,4,'Prisma',0,1,'C');
                //IZQ
                $this->Cell(8,4,'OD:',0,0,'');
                $this->T();
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Esferico
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Cilindro
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Eje
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD ADD
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD DNP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD DIP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Altura
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Prisma
                $this->Cell(15,4,'',0,0,'C');
                //DER
                $this->TB();
                $this->Cell(8,4,'OD:',0,0,'');
                $this->T();
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Esferico
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Cilindro
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Eje
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD ADD
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD DNP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD DIP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Altura
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OD Prisma                
                $this->Cell(15,4,'',0,1,'C');
                //IZQ
                $this->TB();
                $this->Cell(8,4,'OI:',0,0,'');
                $this->T();
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Esferico
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Cilindro
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Eje
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI ADD
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI DNP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI DIP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Altura
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Prisma
                $this->Cell(15,4,'',0,0,'C'); 
                //DER
                $this->TB();
                $this->Cell(8,4,'OI:',0,0,'');
                $this->T();
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Esferico
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Cilindro
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Eje
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI ADD
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI DNP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI DIP
                $this->Cell(14,4,'',1,0,'C');   //DATOS EN VARIABLE     OI Altura
                $this->Cell(14,4,'',1,1,'C');   //DATOS EN VARIABLE     OI Prisma
            }

            function logo(){
                $this->TB();
                $this->SetXY(40,150);
                $this->Cell(40,4,'Original',0,0,'');
                $this->T2txtR();
                $this->T1();
                $this->SetXY(73,158);
                $this->Cell(15,4,'Folio:',0,0,'');
                $this->T();
                $this->Cell(25,4,'',0,1,'C');   //DATOS EN VARIABLE
                $this->TB();
                $this->SetXY(170,150);
                $this->Cell(40,4,'Copia Paciente',0,0,'');
                $this->T2txtR();
                $this->T1();
                $this->SetXY(208,158);
                $this->Cell(15,4,'Folio:',0,0,'');
                $this->T();
                $this->Cell(25,4,'',0,1,'C');   //DATOS EN VARIABLE
                $this->Image('../public/img/logo.png',10,150,50);
                $this->Image('../public/img/logo.png',140,150,50);
                $this->SetXY(10,165);
                $this->ODIO_Rec();
                $this->Ln();
                $this->TB();
                $this->Cell(30,4,'Marca',0,0,'C');
                $this->Cell(30,4,'Modelo',0,0,'C');
                $this->Cell(30,4,utf8_decode('Tipo Armazón'),0,0,'C');
                $this->Cell(30,4,'Color',0,0,'C');
                $this->Cell(15,4,'',0,0,'C');
                $this->Cell(30,4,'Marca',0,0,'C');
                $this->Cell(30,4,'Modelo',0,0,'C');
                $this->Cell(30,4,utf8_decode('Tipo Armazón'),0,0,'C');
                $this->Cell(30,4,'Color',0,1,'C');

                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE marca
                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE modelo
                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE tipo armazon
                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE color
                $this->Cell(15,4,'',0,0,'C');
                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE marca
                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE modelo
                $this->Cell(30,4,'',1,0,'C');   //DATOS EN VARIABLE tipo armazon
                $this->Cell(30,4,'',1,1,'C');   //DATOS EN VARIABLE color
                $this->Ln(3);

                $this->Cell(20,4,'Lente',0,0,'C');
                $this->Cell(20,4,'Material',0,0,'C');
                $this->Cell(20,4,'Color',0,0,'C');
                $this->Cell(20,4,'Tratamiento',0,0,'C');
                $this->Cell(20,4,'Filtro',0,0,'C');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'',1,0,'C');
                $this->Cell(12,4,'Claro',0,0,'C');
                $this->Cell(13,4,'',0,0,'C');
                $this->Cell(20,4,'Lente',0,0,'C');
                $this->Cell(20,4,'Material',0,0,'C');
                $this->Cell(20,4,'Color',0,0,'C');
                $this->Cell(20,4,'Tratamiento',0,0,'C');
                $this->Cell(20,4,'Filtro',0,0,'C');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'',1,0,'C');
                $this->Cell(12,4,'Claro',0,1,'C');

                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'',1,0,'C');
                $this->Cell(12,4,'Medio',0,0,'C');

                $this->Cell(13,4,'',0,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(20,4,'',1,0,'C');
                $this->Cell(5,4,'',0,0,'C');
                $this->Cell(5,4,'',1,0,'C');
                $this->Cell(12,4,'Medio',0,1,'C');

                $this->Cell(105,4,'',0,0,'C');
                $this->Cell(5,4,'',1,0,'C');
                $this->Cell(14,4,'Obscuro',0,0,'C');
                $this->Cell(116,4,'',0,0,'C');
                $this->Cell(5,4,'',1,0,'C');
                $this->Cell(14,4,'Obscuro',0,1,'C');

                $this->Cell(3,4,'',0,0,'C');
                $this->Cell(14,4,'Observaciones',0,0,'C');
                $this->Cell(110,4,'',0,0,'C');
                $this->Cell(12,4,'',0,0,'C');
                $this->Cell(14,4,'Observaciones',0,1,'C');
                $this->Cell(110,4,'',0,1,'C');  
            }

            function linea(){
                $this->CCel();
                $this->Cell(125,1,'',1,0,'');
                $this->Cell(135,1,'',1,1,'');
            }

            function Footer(){      // Pie de página
                $this->SetY(-70);
                $this->linea();
                $this->logo();
            }

            function datosRecibo(){
                $this->SetXY(10,55);
                $nombre= utf8_decode('Maria Magdalena Popocatl Ródriguez');
                $this->T2txtR();
                $this->CCel();
                $this->Cell(40,4,'Recibo de Pago:',0,0,'');
                $this->Cell(70,4,'',0,0,'C');
                $this->Cell(25,4,'',0,0,'C'); 
                $this->Cell(40,4,'Recibo de Pago:',0,0,'');
                $this->Cell(70,4,'Copia Paciente',0,0,'C');
                $this->Cell(15,4,'',0,1,'C');    //DATOS EN VARIABLE
                $this->Cell(0,1,'',0,1,'C');
                $this->linea();
                $this->Ln(2);
                $this->T();
                $this->Cell(15,4,'Nombre:',0,0,'');
                $this->T2();
                $this->Cell(70,4,$nombre,0,0,'C');  //DATOS EN VARIABLE
                $this->T();
                $this->Cell(12,4,'Fecha:',0,0,'');
                $this->T2();
                $this->Cell(20,4,'01-01-2019',0,0,'C'); //DATOS EN VARIABLE
                $this->T();
                $this->Cell(18,4,'',0,0,'');
                $this->Cell(15,4,'Nombre:',0,0,'');
                $this->T2();
                $this->Cell(70,4,$nombre,0,0,'C');  //DATOS EN VARIABLE
                $this->T();
                $this->Cell(12,4,'Fecha:',0,0,'');
                $this->T2();
                $this->Cell(20,4,'01-01-2019',0,1,'C');    //DATOS EN VARIABLE

                $this->T();                              
                $this->Cell(25,4,utf8_decode('Dirección:'),0,0,'');
                $this->Cell(100,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(10,4,'',0,0,'C');
                $this->Cell(25,4,utf8_decode('Dirección:'),0,0,'');
                $this->Cell(100,4,'',0,1,'C');    //DATOS EN VARIABLE
                
                $this->Cell(15,4,utf8_decode('Teléfono:'),0,0,'');
                $this->Cell(45,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(15,4,'Celular:',0,0,'');
                $this->Cell(45,4,'',0,0,'C');   //DATOS EN VARIABLE
                $this->Cell(15,4,'',0,0,'C');
                $this->Cell(15,4,utf8_decode('Teléfono:'),0,0,'');
                $this->Cell(45,4,'',0,0,'C');    //DATOS EN VARIABLE
                $this->Cell(15,4,'Celular:',0,0,'');
                $this->Cell(45,4,'',0,1,'C');    //DATOS EN VARIABLE

                //Titulo IZQ
                $this->Ln(2);
                $this->Cell(15,4,utf8_decode('Cantidad'),1,0,'');
                $this->Cell(80,4,utf8_decode('Descripción'),1,0,'');
                $this->Cell(20,4,utf8_decode('Importe'),1,0,'');
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode('Cantidad'),1,0,'');
                $this->Cell(80,4,utf8_decode('Descripción'),1,0,'');
                $this->Cell(20,4,utf8_decode('Importe'),1,1,'');
                $this->Ln(2);

               //Información
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,1,'C');    //DATOS EN VARIABLE

                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,1,'C');    //DATOS EN VARIABLE

                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,1,'C');    //DATOS EN VARIABLE

                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,1,'C');    //DATOS EN VARIABLE

                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,1,'C');    //DATOS EN VARIABLE

                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,0,'C');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(15,4,utf8_decode(''),1,0,'C');   //DATOS EN VARIABLE
                $this->Cell(80,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),1,1,'C');    //DATOS EN VARIABLE
                $this->Ln(3);

                $this->Cell(80,4,utf8_decode(''),0,0,''); 
                $this->Cell(15,4,utf8_decode('Subtotal'),1,0,'');
                $this->Cell(20,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(80,4,utf8_decode(''),0,0,'');    
                $this->Cell(15,4,utf8_decode('Subtotal'),1,0,'');   
                $this->Cell(20,4,utf8_decode(''),1,1,'');    //DATOS EN VARIABLE
  
                $this->Cell(80,4,utf8_decode('Importe con letra'),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(15,4,utf8_decode('IVA'),1,0,'');
                $this->Cell(20,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                $this->Cell(80,4,utf8_decode('Importe con letra'),0,0,'');    
                $this->Cell(15,4,utf8_decode('IVA'),1,0,'');   
                $this->Cell(20,4,utf8_decode(''),1,1,'');    //DATOS EN VARIABLE

                $impLet='cuatro mil novecientos noventa y cinco pesos';
                $this->Cell(75,4,utf8_decode($impLet),1,0,'C');  
                $this->Cell(5,4,utf8_decode(''),0,0,'C');  
                $this->Cell(15,4,utf8_decode('TOTAL'),1,0,'');
                $this->Cell(20,4,utf8_decode(''),1,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');
                
                $this->Cell(75,4,utf8_decode($impLet),1,0,'C');  
                $this->Cell(5,4,utf8_decode(''),0,0,'C');     
                $this->Cell(15,4,utf8_decode('TOTAL'),1,0,'');   
                $this->Cell(20,4,utf8_decode(''),1,1,'');    //DATOS EN VARIABLE
                $this->Ln(2);

                $this->TB(); 
                $this->Cell(42,4,utf8_decode(''),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(73,4,utf8_decode('Nuestros precios no incluyen el impuesto del IVA.'),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(42,4,utf8_decode(''),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(73,4,utf8_decode('Nuestros precios no incluyen el impuesto del IVA.'),0,1,'');    //DATOS EN VARIABLE

                $this->Cell(42,4,utf8_decode(''),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(73,4,utf8_decode('En caso de cancelación. No se aplicará devolución.'),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(20,4,utf8_decode(''),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(42,4,utf8_decode(''),0,0,'');    //DATOS EN VARIABLE
                $this->Cell(73,4,utf8_decode('En caso de cancelación. No se aplicará devolución.'),0,1,'');    //DATOS EN VARIABLE
                       
            }

        }

        //  http://localhost/proyectos/test/back/apiMirosc/historia/pdf/1#page=1&zoom=90

        
        /*$mpdf = new mPDF('',    // mode - default ''
         '',    // format - A4, for example, default ''
        0,     // font size - default 0
        '',    // default font family
        15,    // margin_left
        15,    // margin right
        16,     // margin top
        16,    // margin bottom
        9,     // margin header
        9,     // margin footer
        'L');  // L - landscape, P - portrait*/

//$pdf->UPC_A(10,20,'012345678901', 5, 0.35, 9);

        $title = 'Recibo';
        $nombre =utf8_decode('" Maria Magdalena Popocatl Ródriguez "'); 
        $folio=123;     //pasar el valor para que asi se guarde el pdf

        $tipo='letter';
        $orien='l';
        $pdf = new PDF('P', 'mm', $tipo);  //mio

        $pdf->T1();
        $pdf->SetTitle($title);

        $pdf->AliasNbPages();
        $pdf->AddPage($orien);
        $pdf->Image('../public/img/cancelado.png',15,76,110);
        $pdf->Image('../public/img/cancelado.png',150,76,110);

        $pdf->datosRecibo();

##################################################################################

        $pdf->Output($title._.$folio.'.pdf','I'); //I D F S

    });
