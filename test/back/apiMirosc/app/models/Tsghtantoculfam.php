<?php

//namespace sght;

class Tsghtantoculfam extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_antoculfamid;

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     *
     * @var string
     */
    public $bol_cataratas;

    /**
     *
     * @var string
     */
    public $bol_glaucoma;

    /**
     *
     * @var string
     */
    public $bol_cirugias;

    /**
     *
     * @var string
     */
    public $bol_estrabismo;

    /**
     *
     * @var string
     */
    public $bol_ametropias;

    /**
     *
     * @var string
     */
    public $bol_queratocono;

    /**
     *
     * @var string
     */
    public $bol_ceguera;

    /**
     *
     * @var string
     */
    public $opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     *
     * @var integer
     */
    public $cod_txid_fk;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtantoculfam");
        $this->belongsTo('cod_datogeneralid_fk', 'sght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
        $this->belongsTo('cod_txid_fk', 'sght\Tsghtx', 'cod_txid', ['alias' => 'Tsghtx']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtantoculfam';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtantoculfam[]|Tsghtantoculfam|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtantoculfam|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
