<?php

//namespace sght;

class Tsghtexamenpx extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_examenpxid;

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     *
     * @var string
     */
    public $des_ladoojo;

    /**
     *
     * @var double
     */
    public $cnu_avl;

    /**
     *
     * @var double
     */
    public $cnu_ph;

    /**
     *
     * @var double
     */
    public $cnu_retinoscopia;

    /**
     *
     * @var double
     */
    public $cnu_rx;

    /**
     *
     * @var double
     */
    public $cnu_bicromatica;

    /**
     *
     * @var double
     */
    public $cnu_reloj;

    /**
     *
     * @var integer
     */
    public $opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtexamenpx");
        $this->belongsTo('cod_datogeneralid_fk', 'sght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtexamenpx';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtexamenpx[]|Tsghtexamenpx|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtexamenpx|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
