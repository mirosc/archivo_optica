<?php

//namespace cat;

class Tcatcilindro extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_cilindroid;

    /**
     *
     * @var string
     */
    public $cnu_valor;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cat");
        $this->setSource("tcatcilindro");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tcatcilindro';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcatcilindro[]|Tcatcilindro|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcatcilindro|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
