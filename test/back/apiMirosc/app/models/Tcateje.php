<?php

//namespace cat;

class Tcateje extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_ejeid;

    /**
     *
     * @var integer
     */
    public $cnu_valor;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cat");
        $this->setSource("tcateje");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tcateje';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcateje[]|Tcateje|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcateje|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
