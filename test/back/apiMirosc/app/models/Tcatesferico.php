<?php

//namespace cat;

class Tcatesferico extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_esfericoid;

    /**
     *
     * @var string
     */
    public $cnu_valor;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cat");
        $this->setSource("tcatesferico");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tcatesferico';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcatesferico[]|Tcatesferico|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcatesferico|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
