<?php

//namespace cat;

class Tcatvalorprueba extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_valorpruebaid;

    /**
     *
     * @var string
     */
    public $cnu_valor;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cat");
        $this->setSource("tcatvalorprueba");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tcatvalorprueba';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcatvalorprueba[]|Tcatvalorprueba|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tcatvalorprueba|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
