<?php

//namespace sght;

class Tsghtobservaciongen extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_observaciongenid;

    /**
     *
     * @var integer
     */
    public $cod_datogeneralid_fk;

    /**
     *
     * @var string
     */
    public $txt_anomalias;

    /**
     *
     * @var string
     */
    public $txt_asimetrias;

    /**
     *
     * @var string
     */
    public $txt_tx1;

    /**
     *
     * @var string
     */
    public $txt_obsocular;

    /**
     *
     * @var string
     */
    public $txt_tx2;

    /**
     *
     * @var string
     */
    public $txt_comportamiento;

    /**
     *
     * @var string
     */
    public $txt_otro;

    /**
     *
     * @var string
     */
    public $opcional;

    /**
     *
     * @var string
     */
    public $bol_status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtobservaciongen");
        $this->belongsTo('cod_datogeneralid_fk', 'sght\Tsghtdatosgeneral', 'cod_datogeneralid', ['alias' => 'Tsghtdatosgeneral']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtobservaciongen';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtobservaciongen[]|Tsghtobservaciongen|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtobservaciongen|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
