<?php

//namespace sght;

class Tsghtx extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cod_txid;

    /**
     *
     * @var string
     */
    public $txt_tx1;

    /**
     *
     * @var string
     */
    public $txt_tx2;

    /**
     *
     * @var string
     */
    public $txt_tx3;

    /**
     *
     * @var string
     */
    public $txt_tx4;

    /**
     *
     * @var string
     */
    public $txt_tx5;

    /**
     *
     * @var string
     */
    public $txt_tx6;

    /**
     *
     * @var string
     */
    public $txt_tx7;

    /**
     *
     * @var string
     */
    public $txt_tx8;

    /**
     *
     * @var string
     */
    public $txt_tx9;

    /**
     *
     * @var string
     */
    public $txt_tx10;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("sght");
        $this->setSource("tsghtx");
        $this->hasMany('cod_txid', 'sght\Tsghtantmedicofam', 'cod_txid_fk', ['alias' => 'Tsghtantmedicofam']);
        $this->hasMany('cod_txid', 'sght\Tsghtantoculfam', 'cod_txid_fk', ['alias' => 'Tsghtantoculfam']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tsghtx';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtx[]|Tsghtx|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tsghtx|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
