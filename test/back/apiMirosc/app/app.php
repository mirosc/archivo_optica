<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Add your routes here
 */
$app->get('/', function () {
    echo $this['view']->render('index');
});


include('rutas/login/user.php');

//include('rutas/num2letra.php');

include('rutas/historia/antmedfam.php');
include('rutas/historia/antmedpx.php');
include('rutas/historia/antocufam.php');
include('rutas/historia/antocupx.php');
include('rutas/historia/datoGeneral.php');
include('rutas/historia/examenpx.php');
include('rutas/historia/historia.php');
include('rutas/historia2/bk_historia_vf.php');
include('rutas/historia/observaciongen.php');
include('rutas/historia/queja.php');
include('rutas/historia/rxpaciente.php');

include('rutas/cat/valores.php');
include('rutas/cat/cilindro.php');

include('rutas/whats/contactos.php');
include('rutas/whats/mensajes.php');
include('rutas/whats/enviados.php');

include('rutas/inventario/marcas.php');
include('rutas/inventario/armazon.php');
include('rutas/inventario/material.php');
include('rutas/inventario/color.php');
include('rutas/inventario/tamanio_forma.php');
include('rutas/inventario/tamanio_puente.php');
include('rutas/inventario/tamanio_terminal.php');
include('rutas/inventario/color_forma.php');
include('rutas/inventario/color_terminal.php');



include('rutas/shop/wpposts.php');

/**
 * Not found handler
 */
$app->notFound(function () use($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
