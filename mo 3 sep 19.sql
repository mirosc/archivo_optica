--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Debian 11.5-1.pgdg90+1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-1.pgdg90+1)

-- Started on 2019-09-03 23:29:17 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--DROP DATABASE mirosc;
--
-- TOC entry 3017 (class 1262 OID 16384)
-- Name: mirosc; Type: DATABASE; Schema: -; Owner: postgres
--

--CREATE DATABASE mirosc WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE = 'es_ES.UTF-8';


ALTER DATABASE mirosc OWNER TO mirosc;

--\connect mirosc

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 20627)
-- Name: sght; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sght;


ALTER SCHEMA sght OWNER TO mirosc;

--
-- TOC entry 219 (class 1255 OID 48303)
-- Name: fu_obtener_edad(date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
pn_edad := FLOOR(((DATE_PART('YEAR',current_date)-DATE_PART('YEAR',pd_fecha_ini))* 372 + (DATE_PART('MONTH',current_date) - DATE_PART('MONTH',pd_fecha_ini))*31 + (DATE_PART('DAY',current_date)-DATE_PART('DAY',pd_fecha_ini)))/372);
END;
$$;


ALTER FUNCTION public.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) OWNER TO mirosc;

--
-- TOC entry 220 (class 1255 OID 48304)
-- Name: fu_obtener_edad(date); Type: FUNCTION; Schema: sght; Owner: postgres
--

CREATE FUNCTION sght.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
pn_edad := FLOOR(((DATE_PART('YEAR',current_date)-DATE_PART('YEAR',pd_fecha_ini))* 372 + (DATE_PART('MONTH',current_date) - DATE_PART('MONTH',pd_fecha_ini))*31 + (DATE_PART('DAY',current_date)-DATE_PART('DAY',pd_fecha_ini)))/372);
END;
$$;


ALTER FUNCTION sght.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) OWNER TO mirosc;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 215 (class 1259 OID 20776)
-- Name: tsghtantmedicofam; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtantmedicofam (
    cod_antmedfam integer NOT NULL,
    cod_datogeneralid_fk integer,
    bol_diabetes boolean,
    bol_preionarter boolean,
    bol_glaucoma boolean,
    bol_cirugias boolean,
    bol_traumatismo boolean,
    bol_dolorcabeza boolean,
    bol_alergias boolean,
    bol_saludgeneral boolean,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtantmedicofam OWNER TO mirosc;

--
-- TOC entry 214 (class 1259 OID 20774)
-- Name: tsghtantmedicofam_cod_antmedfam_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtantmedicofam_cod_antmedfam_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtantmedicofam_cod_antmedfam_seq OWNER TO mirosc;

--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 214
-- Name: tsghtantmedicofam_cod_antmedfam_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtantmedicofam_cod_antmedfam_seq OWNED BY sght.tsghtantmedicofam.cod_antmedfam;


--
-- TOC entry 218 (class 1259 OID 20869)
-- Name: tsghtantmedicopx_cod_antmedpxid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtantmedicopx_cod_antmedpxid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE sght.tsghtantmedicopx_cod_antmedpxid_seq OWNER TO mirosc;

--
-- TOC entry 211 (class 1259 OID 20752)
-- Name: tsghtantmedicopx; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtantmedicopx (
    cod_datogeneralid_fk integer,
    des_fecultexammed character varying(50),
    bol_salud boolean,
    bol_diabetes boolean,
    bol_preionarter boolean,
    bol_glaucoma boolean,
    bol_cirugias boolean,
    txt_tx1 text,
    bol_traumatismo boolean,
    bol_dolorcabeza boolean,
    bol_alergias boolean,
    txt_tx2 text,
    bol_sermedicos boolean,
    txt_cuales text,
    bol_status boolean DEFAULT true,
    cod_antmedpxid integer DEFAULT nextval('sght.tsghtantmedicopx_cod_antmedpxid_seq'::regclass) NOT NULL
);


ALTER TABLE sght.tsghtantmedicopx OWNER TO mirosc;

--
-- TOC entry 210 (class 1259 OID 20740)
-- Name: tsghtantocularpx; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtantocularpx (
    cod_antocularpxid integer NOT NULL,
    cod_datogeneralid_fk integer,
    bol_golpes boolean,
    txt_ubicacion1 text,
    txt_tx1 text,
    txt_infecciones text,
    txt_ubicacion2 text,
    txt_tx2 text,
    txt_desviacion_o text,
    txt_tx3 text,
    opcional text,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtantocularpx OWNER TO mirosc;

--
-- TOC entry 209 (class 1259 OID 20738)
-- Name: tsghtantocularpx_cod_antocularpxid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtantocularpx_cod_antocularpxid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtantocularpx_cod_antocularpxid_seq OWNER TO mirosc;

--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 209
-- Name: tsghtantocularpx_cod_antocularpxid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtantocularpx_cod_antocularpxid_seq OWNED BY sght.tsghtantocularpx.cod_antocularpxid;


--
-- TOC entry 213 (class 1259 OID 20764)
-- Name: tsghtantoculfam; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtantoculfam (
    cod_antoculfamid integer NOT NULL,
    cod_datogeneralid_fk integer,
    bol_cataratas boolean,
    bol_glaucoma boolean,
    bol_cirugias boolean,
    bol_estrabismo boolean,
    bol_ametropias boolean,
    bol_queratocono boolean,
    bol_ceguera boolean,
    opcional text,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtantoculfam OWNER TO mirosc;

--
-- TOC entry 212 (class 1259 OID 20762)
-- Name: tsghtantoculfam_cod_antoculfamid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtantoculfam_cod_antoculfamid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtantoculfam_cod_antoculfamid_seq OWNER TO mirosc;

--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 212
-- Name: tsghtantoculfam_cod_antoculfamid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtantoculfam_cod_antoculfamid_seq OWNED BY sght.tsghtantoculfam.cod_antoculfamid;


--
-- TOC entry 200 (class 1259 OID 20685)
-- Name: tsghtdatosgeneral; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtdatosgeneral (
    cod_datogeneralid integer NOT NULL,
    cod_pacienteid_fk integer,
    fec_registro date,
    fec_nacimiento date,
    txt_hobbies text,
    des_calle character varying(50),
    cnu_numexterior integer,
    des_numinterior character varying(8),
    des_colonia character varying(20),
    cnu_cp integer,
    des_municipio character varying(40),
    des_estado character varying(20),
    email character varying(50),
    des_ocupacion character varying(50),
    txt_factores text,
    des_lugartrabajo character varying(30),
    des_opcional character varying(50),
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtdatosgeneral OWNER TO mirosc;

--
-- TOC entry 199 (class 1259 OID 20683)
-- Name: tsghtdatosgeneral_cod_datogeneralid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtdatosgeneral_cod_datogeneralid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtdatosgeneral_cod_datogeneralid_seq OWNER TO mirosc;

--
-- TOC entry 3021 (class 0 OID 0)
-- Dependencies: 199
-- Name: tsghtdatosgeneral_cod_datogeneralid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtdatosgeneral_cod_datogeneralid_seq OWNED BY sght.tsghtdatosgeneral.cod_datogeneralid;


--
-- TOC entry 206 (class 1259 OID 20722)
-- Name: tsghtexamenpx; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtexamenpx (
    cod_examenpxid integer NOT NULL,
    cod_datogeneralid_fk integer,
    des_ladoojo character(1),
    cnu_avl numeric(4,2),
    cnu_ph numeric(4,2),
    cnu_retinoscopia numeric(4,2),
    cnu_rx numeric(4,2),
    cnu_bicromatica numeric(4,2),
    cnu_reloj numeric(4,2),
    opcional integer,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtexamenpx OWNER TO mirosc;

--
-- TOC entry 205 (class 1259 OID 20720)
-- Name: tsghtexamenpx_cod_examenpxid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtexamenpx_cod_examenpxid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtexamenpx_cod_examenpxid_seq OWNER TO mirosc;

--
-- TOC entry 3022 (class 0 OID 0)
-- Dependencies: 205
-- Name: tsghtexamenpx_cod_examenpxid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtexamenpx_cod_examenpxid_seq OWNED BY sght.tsghtexamenpx.cod_examenpxid;


--
-- TOC entry 217 (class 1259 OID 20785)
-- Name: tsghtobservaciongen; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtobservaciongen (
    cod_observaciongenid integer NOT NULL,
    cod_datogeneralid_fk integer,
    txt_anomalias text,
    txt_asimetrias text,
    txt_tx1 text,
    txt_obsocular text,
    txt_tx2 text,
    txt_comportamiento text,
    txt_otro text,
    opcional text,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtobservaciongen OWNER TO mirosc;

--
-- TOC entry 216 (class 1259 OID 20783)
-- Name: tsghtobservaciongen_cod_observaciongenid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtobservaciongen_cod_observaciongenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtobservaciongen_cod_observaciongenid_seq OWNER TO mirosc;

--
-- TOC entry 3023 (class 0 OID 0)
-- Dependencies: 216
-- Name: tsghtobservaciongen_cod_observaciongenid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtobservaciongen_cod_observaciongenid_seq OWNED BY sght.tsghtobservaciongen.cod_observaciongenid;


--
-- TOC entry 198 (class 1259 OID 20672)
-- Name: tsghtpacientes; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtpacientes (
    cod_pacienteid integer NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_paterno character varying(50),
    des_materno character varying(50),
    cnu_pais smallint DEFAULT 521,
    cnu_celular numeric,
    cnu_telcasa numeric,
    bol_whats boolean,
    bol_telcasa boolean,
    bol_activo boolean DEFAULT true
);


ALTER TABLE sght.tsghtpacientes OWNER TO mirosc;

--
-- TOC entry 197 (class 1259 OID 20670)
-- Name: tsghtpacientes_cod_pacienteid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtpacientes_cod_pacienteid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtpacientes_cod_pacienteid_seq OWNER TO mirosc;

--
-- TOC entry 3024 (class 0 OID 0)
-- Dependencies: 197
-- Name: tsghtpacientes_cod_pacienteid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtpacientes_cod_pacienteid_seq OWNED BY sght.tsghtpacientes.cod_pacienteid;


--
-- TOC entry 202 (class 1259 OID 20699)
-- Name: tsghtqueja; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtqueja (
    cod_quejaid integer NOT NULL,
    cod_datogeneralid_fk integer,
    txt_causa text,
    txt_problema text,
    des_cuando character varying(50),
    bol_vlejana boolean,
    bol_vcercana boolean,
    bol_vdoble boolean,
    bol_ardor boolean,
    bol_comezon boolean,
    bol_secrecion boolean,
    bol_fatiga boolean,
    bol_epiforia boolean,
    bol_ojoseco boolean,
    bol_ojorojo boolean,
    bol_fotofobia boolean,
    bol_moscas boolean,
    bol_flashes boolean,
    bol_auras boolean,
    opcional integer,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtqueja OWNER TO mirosc;

--
-- TOC entry 201 (class 1259 OID 20697)
-- Name: tsghtqueja_cod_quejaid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtqueja_cod_quejaid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtqueja_cod_quejaid_seq OWNER TO mirosc;

--
-- TOC entry 3025 (class 0 OID 0)
-- Dependencies: 201
-- Name: tsghtqueja_cod_quejaid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtqueja_cod_quejaid_seq OWNED BY sght.tsghtqueja.cod_quejaid;


--
-- TOC entry 208 (class 1259 OID 20731)
-- Name: tsghtrxpaciente; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtrxpaciente (
    cod_rxpaciente integer NOT NULL,
    des_ladoojo character(1),
    cnu_esferico numeric(4,2),
    cnu_cilindro numeric(4,2),
    cnu_eje integer,
    cnu_add integer,
    cnu_dnp integer,
    cnu_dip integer,
    cnu_altura integer,
    cnu_prisma numeric(4,2),
    opcional integer,
    bol_status boolean DEFAULT true,
    cod_datogeneralid_fk integer
);


ALTER TABLE sght.tsghtrxpaciente OWNER TO mirosc;

--
-- TOC entry 207 (class 1259 OID 20729)
-- Name: tsghtrxpaciente_cod_rxpaciente_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtrxpaciente_cod_rxpaciente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtrxpaciente_cod_rxpaciente_seq OWNER TO mirosc;

--
-- TOC entry 3026 (class 0 OID 0)
-- Dependencies: 207
-- Name: tsghtrxpaciente_cod_rxpaciente_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtrxpaciente_cod_rxpaciente_seq OWNED BY sght.tsghtrxpaciente.cod_rxpaciente;


--
-- TOC entry 204 (class 1259 OID 20711)
-- Name: tsghtultimoexamen; Type: TABLE; Schema: sght; Owner: postgres
--

CREATE TABLE sght.tsghtultimoexamen (
    cod_ultimoexamenid integer NOT NULL,
    cod_quejaid_fk integer,
    des_fechaaprox character varying(30),
    cnu_od numeric(4,2),
    cnu_oi numeric(4,2),
    cnu_add numeric(4,2),
    txt_anteojos text,
    bol_eficacia boolean,
    bol_comodidad boolean,
    bol_necesidad boolean,
    opcional character varying(50)
);


ALTER TABLE sght.tsghtultimoexamen OWNER TO mirosc;

--
-- TOC entry 203 (class 1259 OID 20709)
-- Name: tsghtultimoexamen_cod_ultimoexamenid_seq; Type: SEQUENCE; Schema: sght; Owner: postgres
--

CREATE SEQUENCE sght.tsghtultimoexamen_cod_ultimoexamenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtultimoexamen_cod_ultimoexamenid_seq OWNER TO mirosc;

--
-- TOC entry 3027 (class 0 OID 0)
-- Dependencies: 203
-- Name: tsghtultimoexamen_cod_ultimoexamenid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: postgres
--

ALTER SEQUENCE sght.tsghtultimoexamen_cod_ultimoexamenid_seq OWNED BY sght.tsghtultimoexamen.cod_ultimoexamenid;


--
-- TOC entry 2831 (class 2604 OID 20779)
-- Name: tsghtantmedicofam cod_antmedfam; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantmedicofam ALTER COLUMN cod_antmedfam SET DEFAULT nextval('sght.tsghtantmedicofam_cod_antmedfam_seq'::regclass);


--
-- TOC entry 2825 (class 2604 OID 20743)
-- Name: tsghtantocularpx cod_antocularpxid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantocularpx ALTER COLUMN cod_antocularpxid SET DEFAULT nextval('sght.tsghtantocularpx_cod_antocularpxid_seq'::regclass);


--
-- TOC entry 2829 (class 2604 OID 20767)
-- Name: tsghtantoculfam cod_antoculfamid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantoculfam ALTER COLUMN cod_antoculfamid SET DEFAULT nextval('sght.tsghtantoculfam_cod_antoculfamid_seq'::regclass);


--
-- TOC entry 2816 (class 2604 OID 20688)
-- Name: tsghtdatosgeneral cod_datogeneralid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtdatosgeneral ALTER COLUMN cod_datogeneralid SET DEFAULT nextval('sght.tsghtdatosgeneral_cod_datogeneralid_seq'::regclass);


--
-- TOC entry 2821 (class 2604 OID 20725)
-- Name: tsghtexamenpx cod_examenpxid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtexamenpx ALTER COLUMN cod_examenpxid SET DEFAULT nextval('sght.tsghtexamenpx_cod_examenpxid_seq'::regclass);


--
-- TOC entry 2833 (class 2604 OID 20788)
-- Name: tsghtobservaciongen cod_observaciongenid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtobservaciongen ALTER COLUMN cod_observaciongenid SET DEFAULT nextval('sght.tsghtobservaciongen_cod_observaciongenid_seq'::regclass);


--
-- TOC entry 2813 (class 2604 OID 20675)
-- Name: tsghtpacientes cod_pacienteid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtpacientes ALTER COLUMN cod_pacienteid SET DEFAULT nextval('sght.tsghtpacientes_cod_pacienteid_seq'::regclass);


--
-- TOC entry 2818 (class 2604 OID 20702)
-- Name: tsghtqueja cod_quejaid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtqueja ALTER COLUMN cod_quejaid SET DEFAULT nextval('sght.tsghtqueja_cod_quejaid_seq'::regclass);


--
-- TOC entry 2823 (class 2604 OID 20734)
-- Name: tsghtrxpaciente cod_rxpaciente; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtrxpaciente ALTER COLUMN cod_rxpaciente SET DEFAULT nextval('sght.tsghtrxpaciente_cod_rxpaciente_seq'::regclass);


--
-- TOC entry 2820 (class 2604 OID 20714)
-- Name: tsghtultimoexamen cod_ultimoexamenid; Type: DEFAULT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtultimoexamen ALTER COLUMN cod_ultimoexamenid SET DEFAULT nextval('sght.tsghtultimoexamen_cod_ultimoexamenid_seq'::regclass);


--
-- TOC entry 3008 (class 0 OID 20776)
-- Dependencies: 215
-- Data for Name: tsghtantmedicofam; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtantmedicofam (cod_antmedfam, cod_datogeneralid_fk, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, bol_traumatismo, bol_dolorcabeza, bol_alergias, bol_saludgeneral, bol_status) VALUES (1, 23, NULL, true, true, NULL, NULL, NULL, true, NULL, true);
INSERT INTO sght.tsghtantmedicofam (cod_antmedfam, cod_datogeneralid_fk, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, bol_traumatismo, bol_dolorcabeza, bol_alergias, bol_saludgeneral, bol_status) VALUES (2, 24, NULL, true, true, NULL, NULL, true, true, NULL, true);
INSERT INTO sght.tsghtantmedicofam (cod_antmedfam, cod_datogeneralid_fk, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, bol_traumatismo, bol_dolorcabeza, bol_alergias, bol_saludgeneral, bol_status) VALUES (3, 27, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3004 (class 0 OID 20752)
-- Dependencies: 211
-- Data for Name: tsghtantmedicopx; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtantmedicopx (cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status, cod_antmedpxid) VALUES (23, '2019-07-10T05:00:00.000Z', NULL, true, true, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, true, 1);
INSERT INTO sght.tsghtantmedicopx (cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status, cod_antmedpxid) VALUES (24, '2019-07-01T05:00:00.000Z', NULL, NULL, true, NULL, true, 'sdfs', NULL, NULL, true, 'fsdf', NULL, NULL, true, 2);
INSERT INTO sght.tsghtantmedicopx (cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status, cod_antmedpxid) VALUES (27, '2019-04-04T06:00:00.000Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, 3);


--
-- TOC entry 3003 (class 0 OID 20740)
-- Dependencies: 210
-- Data for Name: tsghtantocularpx; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_infecciones, txt_ubicacion2, txt_tx2, txt_desviacion_o, txt_tx3, opcional, bol_status) VALUES (1, 22, true, 'erv', 'erv', NULL, NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_infecciones, txt_ubicacion2, txt_tx2, txt_desviacion_o, txt_tx3, opcional, bol_status) VALUES (2, 22, true, 'erv', 'erv', NULL, NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_infecciones, txt_ubicacion2, txt_tx2, txt_desviacion_o, txt_tx3, opcional, bol_status) VALUES (3, 23, true, 'crec', 'crecc', NULL, NULL, NULL, 'sdcd', 'recc', NULL, true);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_infecciones, txt_ubicacion2, txt_tx2, txt_desviacion_o, txt_tx3, opcional, bol_status) VALUES (4, 24, true, 'sdfsd', 'sdfs', 'sdfs', NULL, NULL, 'sdfs', NULL, NULL, true);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_infecciones, txt_ubicacion2, txt_tx2, txt_desviacion_o, txt_tx3, opcional, bol_status) VALUES (5, 27, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3006 (class 0 OID 20764)
-- Dependencies: 213
-- Data for Name: tsghtantoculfam; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtantoculfam (cod_antoculfamid, cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, opcional, bol_status) VALUES (1, 23, NULL, true, NULL, NULL, true, true, NULL, NULL, true);
INSERT INTO sght.tsghtantoculfam (cod_antoculfamid, cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, opcional, bol_status) VALUES (2, 24, true, NULL, NULL, NULL, true, true, true, NULL, true);
INSERT INTO sght.tsghtantoculfam (cod_antoculfamid, cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, opcional, bol_status) VALUES (3, 27, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 2993 (class 0 OID 20685)
-- Dependencies: 200
-- Data for Name: tsghtdatosgeneral; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (22, 3, '2019-07-24', '2019-07-03', 'efre', 'fsfd', 3443, 'efr', NULL, 34323, 'frdfd', 'rf', 'jfn@fdj.cd', 'crcr', 'crcre', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (23, 4, '2019-07-24', '2019-07-10', 'efefs', 'ewf', 34, 'wef', NULL, 43343, 'refrefer', 'efrefre', 'fre@jn.comd', 'crecre', 'crecre', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (24, 5, '2019-07-24', '2019-07-02', 'ddr', 'frf', 643534, '4r', NULL, 34545, 'frefe', 'erfe', 'mirosc@cb.comd', 'fc', 'rffdr', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (25, 2, '2019-08-19', '2019-08-01', 'dcccd', 'scdc', 4, 'sd', NULL, 33414, 'dwed', 'edwwe', 'cre@crc.com', 'csd', 'cds', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (26, 5, '2019-08-19', '2019-08-05', 'sds', 'sdc', 34, '4', NULL, 44544, 'dvcdf', 'dfc', 'dcfd@gd.com', 'dfc', 'dfc', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (21, 2, '2019-07-24', '2007-12-04', 'rfrfr', 'erf', 34, '43', NULL, 34453, 'frrrrrrrrrrr', 'ferf', 'mfr@jfkmf.com', 'rjnv', 'jenj', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (27, 6, '2019-08-24', '1990-02-07', 'jugar', 'av reforma', 7, NULL, NULL, 90000, 'tlaxcala', 'tlaxcala', 'micorreo@hotmail.com', 'trabajador domestico', 'diabetes', NULL, NULL, true);


--
-- TOC entry 2999 (class 0 OID 20722)
-- Dependencies: 206
-- Data for Name: tsghtexamenpx; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtexamenpx (cod_examenpxid, cod_datogeneralid_fk, des_ladoojo, cnu_avl, cnu_ph, cnu_retinoscopia, cnu_rx, cnu_bicromatica, cnu_reloj, opcional, bol_status) VALUES (1, 24, 'd', 53.00, 43.00, 34.00, 43.00, 43.00, 34.00, NULL, true);
INSERT INTO sght.tsghtexamenpx (cod_examenpxid, cod_datogeneralid_fk, des_ladoojo, cnu_avl, cnu_ph, cnu_retinoscopia, cnu_rx, cnu_bicromatica, cnu_reloj, opcional, bol_status) VALUES (2, 24, 'i', 53.00, 53.00, 34.00, 34.00, 34.00, 34.00, NULL, true);
INSERT INTO sght.tsghtexamenpx (cod_examenpxid, cod_datogeneralid_fk, des_ladoojo, cnu_avl, cnu_ph, cnu_retinoscopia, cnu_rx, cnu_bicromatica, cnu_reloj, opcional, bol_status) VALUES (3, 27, 'd', 3.00, 4.00, 5.00, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtexamenpx (cod_examenpxid, cod_datogeneralid_fk, des_ladoojo, cnu_avl, cnu_ph, cnu_retinoscopia, cnu_rx, cnu_bicromatica, cnu_reloj, opcional, bol_status) VALUES (4, 27, 'i', 3.00, 4.00, 6.00, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3010 (class 0 OID 20785)
-- Dependencies: 217
-- Data for Name: tsghtobservaciongen; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtobservaciongen (cod_observaciongenid, cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, opcional, bol_status) VALUES (1, 23, 'vf', 'dfvd', NULL, 'dfv', NULL, 'dfv', 'dfv', NULL, true);
INSERT INTO sght.tsghtobservaciongen (cod_observaciongenid, cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, opcional, bol_status) VALUES (2, 24, 'sdfsdf', 'fdsf', NULL, 'sdfs', NULL, 'sdf', 'sdfss', NULL, true);
INSERT INTO sght.tsghtobservaciongen (cod_observaciongenid, cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, opcional, bol_status) VALUES (3, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 2991 (class 0 OID 20672)
-- Dependencies: 198
-- Data for Name: tsghtpacientes; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (2, 'brenda', 'test', 'test2', 521, 324234324, 3454358593, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (3, 'bren', 'dew', 'ewd', 521, 3454353454, 3435345435, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (4, 'csc', 'dfv', 'sdvsd', 521, 3443434334, 43343434, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (5, 'mi', 'pe', 'lalo', 521, 3243243432, 4245235352, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (6, 'oscar', 'chavez', 'lopez', 521, 1234567890, 1234567890, true, true, true);


--
-- TOC entry 2995 (class 0 OID 20699)
-- Dependencies: 202
-- Data for Name: tsghtqueja; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (2, 21, NULL, 'rfe', 'tgtr', true, NULL, NULL, NULL, NULL, NULL, true, NULL, NULL, true, NULL, NULL, NULL, true, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (3, 22, NULL, 'revre', 'revre', NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (4, 23, NULL, 'cerc', 'cerc', NULL, NULL, true, NULL, NULL, true, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (5, 24, NULL, 'df', 'sdgs', NULL, NULL, true, NULL, NULL, true, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (6, 27, NULL, 'ardor', '2 meses', true, NULL, NULL, true, true, NULL, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3001 (class 0 OID 20731)
-- Dependencies: 208
-- Data for Name: tsghtrxpaciente; Type: TABLE DATA; Schema: sght; Owner: postgres
--

INSERT INTO sght.tsghtrxpaciente (cod_rxpaciente, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, opcional, bol_status, cod_datogeneralid_fk) VALUES (1, 'd', 13.00, 15.00, 17, 19, 21, 23, 25, 27.00, NULL, true, 23);
INSERT INTO sght.tsghtrxpaciente (cod_rxpaciente, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, opcional, bol_status, cod_datogeneralid_fk) VALUES (2, 'i', 14.00, 16.00, 18, 20, 22, 24, 26, 28.00, NULL, true, 23);
INSERT INTO sght.tsghtrxpaciente (cod_rxpaciente, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, opcional, bol_status, cod_datogeneralid_fk) VALUES (3, 'd', 34.00, 54.00, 53, 43, 43, 43, 34, 34.00, NULL, true, 24);
INSERT INTO sght.tsghtrxpaciente (cod_rxpaciente, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, opcional, bol_status, cod_datogeneralid_fk) VALUES (4, 'i', 34.00, 53.00, 34, 43, 54, 53, 34, 53.00, NULL, true, 24);
INSERT INTO sght.tsghtrxpaciente (cod_rxpaciente, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, opcional, bol_status, cod_datogeneralid_fk) VALUES (5, 'd', 20.00, 8.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, 27);
INSERT INTO sght.tsghtrxpaciente (cod_rxpaciente, des_ladoojo, cnu_esferico, cnu_cilindro, cnu_eje, cnu_add, cnu_dnp, cnu_dip, cnu_altura, cnu_prisma, opcional, bol_status, cod_datogeneralid_fk) VALUES (6, 'i', -20.00, -8.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, 27);


--
-- TOC entry 2997 (class 0 OID 20711)
-- Dependencies: 204
-- Data for Name: tsghtultimoexamen; Type: TABLE DATA; Schema: sght; Owner: postgres
--



--
-- TOC entry 3028 (class 0 OID 0)
-- Dependencies: 214
-- Name: tsghtantmedicofam_cod_antmedfam_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtantmedicofam_cod_antmedfam_seq', 3, true);


--
-- TOC entry 3029 (class 0 OID 0)
-- Dependencies: 218
-- Name: tsghtantmedicopx_cod_antmedpxid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtantmedicopx_cod_antmedpxid_seq', 3, true);


--
-- TOC entry 3030 (class 0 OID 0)
-- Dependencies: 209
-- Name: tsghtantocularpx_cod_antocularpxid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtantocularpx_cod_antocularpxid_seq', 5, true);


--
-- TOC entry 3031 (class 0 OID 0)
-- Dependencies: 212
-- Name: tsghtantoculfam_cod_antoculfamid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtantoculfam_cod_antoculfamid_seq', 3, true);


--
-- TOC entry 3032 (class 0 OID 0)
-- Dependencies: 199
-- Name: tsghtdatosgeneral_cod_datogeneralid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtdatosgeneral_cod_datogeneralid_seq', 27, true);


--
-- TOC entry 3033 (class 0 OID 0)
-- Dependencies: 205
-- Name: tsghtexamenpx_cod_examenpxid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtexamenpx_cod_examenpxid_seq', 4, true);


--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 216
-- Name: tsghtobservaciongen_cod_observaciongenid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtobservaciongen_cod_observaciongenid_seq', 3, true);


--
-- TOC entry 3035 (class 0 OID 0)
-- Dependencies: 197
-- Name: tsghtpacientes_cod_pacienteid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtpacientes_cod_pacienteid_seq', 6, true);


--
-- TOC entry 3036 (class 0 OID 0)
-- Dependencies: 201
-- Name: tsghtqueja_cod_quejaid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtqueja_cod_quejaid_seq', 6, true);


--
-- TOC entry 3037 (class 0 OID 0)
-- Dependencies: 207
-- Name: tsghtrxpaciente_cod_rxpaciente_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtrxpaciente_cod_rxpaciente_seq', 6, true);


--
-- TOC entry 3038 (class 0 OID 0)
-- Dependencies: 203
-- Name: tsghtultimoexamen_cod_ultimoexamenid_seq; Type: SEQUENCE SET; Schema: sght; Owner: postgres
--

SELECT pg_catalog.setval('sght.tsghtultimoexamen_cod_ultimoexamenid_seq', 1, false);


--
-- TOC entry 2856 (class 2606 OID 20782)
-- Name: tsghtantmedicofam tsghtantmedicofam_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantmedicofam
    ADD CONSTRAINT tsghtantmedicofam_pkey PRIMARY KEY (cod_antmedfam);


--
-- TOC entry 2852 (class 2606 OID 20863)
-- Name: tsghtantmedicopx tsghtantmedicopx_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantmedicopx
    ADD CONSTRAINT tsghtantmedicopx_pkey PRIMARY KEY (cod_antmedpxid);


--
-- TOC entry 2850 (class 2606 OID 20749)
-- Name: tsghtantocularpx tsghtantocularpx_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantocularpx
    ADD CONSTRAINT tsghtantocularpx_pkey PRIMARY KEY (cod_antocularpxid);


--
-- TOC entry 2854 (class 2606 OID 20773)
-- Name: tsghtantoculfam tsghtantoculfam_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantoculfam
    ADD CONSTRAINT tsghtantoculfam_pkey PRIMARY KEY (cod_antoculfamid);


--
-- TOC entry 2838 (class 2606 OID 20696)
-- Name: tsghtdatosgeneral tsghtdatosgeneral_email_key; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtdatosgeneral
    ADD CONSTRAINT tsghtdatosgeneral_email_key UNIQUE (email);


--
-- TOC entry 2840 (class 2606 OID 20694)
-- Name: tsghtdatosgeneral tsghtdatosgeneral_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtdatosgeneral
    ADD CONSTRAINT tsghtdatosgeneral_pkey PRIMARY KEY (cod_datogeneralid);


--
-- TOC entry 2846 (class 2606 OID 20728)
-- Name: tsghtexamenpx tsghtexamenpx_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtexamenpx
    ADD CONSTRAINT tsghtexamenpx_pkey PRIMARY KEY (cod_examenpxid);


--
-- TOC entry 2858 (class 2606 OID 20794)
-- Name: tsghtobservaciongen tsghtobservaciongen_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtobservaciongen
    ADD CONSTRAINT tsghtobservaciongen_pkey PRIMARY KEY (cod_observaciongenid);


--
-- TOC entry 2836 (class 2606 OID 20682)
-- Name: tsghtpacientes tsghtpacientes_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtpacientes
    ADD CONSTRAINT tsghtpacientes_pkey PRIMARY KEY (cod_pacienteid);


--
-- TOC entry 2842 (class 2606 OID 20708)
-- Name: tsghtqueja tsghtqueja_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtqueja
    ADD CONSTRAINT tsghtqueja_pkey PRIMARY KEY (cod_quejaid);


--
-- TOC entry 2848 (class 2606 OID 20737)
-- Name: tsghtrxpaciente tsghtrxpaciente_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtrxpaciente
    ADD CONSTRAINT tsghtrxpaciente_pkey PRIMARY KEY (cod_rxpaciente);


--
-- TOC entry 2844 (class 2606 OID 20719)
-- Name: tsghtultimoexamen tsghtultimoexamen_pkey; Type: CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtultimoexamen
    ADD CONSTRAINT tsghtultimoexamen_pkey PRIMARY KEY (cod_ultimoexamenid);


--
-- TOC entry 2867 (class 2606 OID 20804)
-- Name: tsghtantmedicofam fk_tsghtantmedicofam_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantmedicofam
    ADD CONSTRAINT fk_tsghtantmedicofam_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2865 (class 2606 OID 20864)
-- Name: tsghtantmedicopx fk_tsghtantmedicopx_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantmedicopx
    ADD CONSTRAINT fk_tsghtantmedicopx_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid) ON UPDATE CASCADE;


--
-- TOC entry 2864 (class 2606 OID 20814)
-- Name: tsghtantocularpx fk_tsghtantocularpx_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantocularpx
    ADD CONSTRAINT fk_tsghtantocularpx_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2866 (class 2606 OID 20819)
-- Name: tsghtantoculfam fk_tsghtantoculfam_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtantoculfam
    ADD CONSTRAINT fk_tsghtantoculfam_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2859 (class 2606 OID 20824)
-- Name: tsghtdatosgeneral fk_tsghtdatosgeneral_tsghtpacientes_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtdatosgeneral
    ADD CONSTRAINT fk_tsghtdatosgeneral_tsghtpacientes_1 FOREIGN KEY (cod_pacienteid_fk) REFERENCES sght.tsghtpacientes(cod_pacienteid);


--
-- TOC entry 2862 (class 2606 OID 20829)
-- Name: tsghtexamenpx fk_tsghtexamenpx_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtexamenpx
    ADD CONSTRAINT fk_tsghtexamenpx_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2868 (class 2606 OID 20834)
-- Name: tsghtobservaciongen fk_tsghtobservaciongen_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtobservaciongen
    ADD CONSTRAINT fk_tsghtobservaciongen_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2860 (class 2606 OID 20839)
-- Name: tsghtqueja fk_tsghtqueja_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtqueja
    ADD CONSTRAINT fk_tsghtqueja_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2863 (class 2606 OID 20844)
-- Name: tsghtrxpaciente fk_tsghtrxpaciente_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtrxpaciente
    ADD CONSTRAINT fk_tsghtrxpaciente_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2861 (class 2606 OID 20849)
-- Name: tsghtultimoexamen fk_tsghtultimoexamen_tsghtqueja_1; Type: FK CONSTRAINT; Schema: sght; Owner: postgres
--

ALTER TABLE ONLY sght.tsghtultimoexamen
    ADD CONSTRAINT fk_tsghtultimoexamen_tsghtqueja_1 FOREIGN KEY (cod_quejaid_fk) REFERENCES sght.tsghtqueja(cod_quejaid);


-- Completed on 2019-09-03 23:29:17 CDT

--
-- PostgreSQL database dump complete
--

